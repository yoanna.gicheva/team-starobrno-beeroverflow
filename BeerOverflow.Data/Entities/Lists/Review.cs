﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Data.Entities.Lists
{
    public class Review
    {
        public Review(string text, int beerId, int userId)
        {
            Text = text;
            BeerId = beerId;
            UserId = userId;
            Votes = new List<Vote>();
            CreatedOn = DateTime.Now;
            Flagged = false;
            IsDeleted = false;
        }
        public int ReviewId { get; set; }
        public User User { get; set; }
        public int UserId { get; set; }
        public string Text { get; set; }
        public Beer Beer { get; set; }
        public int BeerId { get; set; }
        public ICollection<Vote> Votes { get; set; }
        public int PositiveVotes { get; set; }
        public int NegativeVotes { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool Flagged { get; set; }
        public bool IsDeleted { get; set; }

    }
}
