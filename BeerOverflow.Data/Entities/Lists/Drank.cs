﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Data.Entities.Lists
{
    public class Drank
    {
        public Drank(int beerId, int userId)
        {
            UserId = userId;
            BeerId = beerId;
        }
        public int DrankId { get; set; }
        public User User { get; set; }
        public int UserId { get; set; }
        public Beer Beer { get; set; }
        public int BeerId { get; set; }
    }
}
