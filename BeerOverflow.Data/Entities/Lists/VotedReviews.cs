﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Data.Entities.Lists
{
    public class VotedReviews
    {
        public int Id { get; set; }
        public User User { get; set; }
        public int UserId { get; set; }
        public Vote Vote { get; set; }
        public int VoteId { get; set; }
    }
}
