﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Data.Entities.Lists
{
    public class UserModification
    {
        public UserModification(DateTime time, string message)
        {
            Time = time;
            Message = message;
        }
        public int Id { get; set; }
        public DateTime Time { get; set; }
        public string Message { get; set; }
        public User User { get; set; }
        public int UserId { get; set; }

    }
}
