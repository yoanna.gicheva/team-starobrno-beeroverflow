﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Data.Entities
{
   public class Country
    {
        public int CountryId { get; set; }
        public string CountryName{ get; set; }
        public bool IsDeleted { get; set; }
        public List<Brewery> Breweries { get; set; }
    }
}
