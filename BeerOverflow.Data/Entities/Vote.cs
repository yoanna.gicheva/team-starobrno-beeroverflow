﻿using BeerOverflow.Data.Entities.Lists;

namespace BeerOverflow.Data.Entities
{
    public class Vote
    {

        public Vote(bool thumbsUp, int reviewId, int userId)
        {
            ThumbsUp = thumbsUp;
            ReviewId = reviewId;
            UserId = userId;
        }
        public int VoteId { get; set; }
        public bool ThumbsUp { get; set; }
        public Review Review { get; set; }
        public int ReviewId { get; set; }
        public User User { get; set; }
        public int UserId { get; set; }
    }
}
