﻿using BeerOverflow.Data.Entities.Lists;
using System.Collections.Generic;

namespace BeerOverflow.Data.Entities
{
    public class Beer
    {  
        public int BeerId { get; set; }
        public string BeerName { get; set; }
        public string Image { get; set; }
        public Style Style { get; set; }
        public int StyleId { get; set; }
        public int RatedTimes { get; set; }
        public Country Country { get; set; }
        public int CountryId { get; set; }
        public double Abv { get; set; }
        public int Sum { get; set; }
        public double Rating { get; set; }
        public int BreweryId { get; set; }
        public Brewery Brewery { get; set; }
        public ICollection<Review> Reviews { get; set; }
        public bool IsDeleted { get; set; }
    }
}
