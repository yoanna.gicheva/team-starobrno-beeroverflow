﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Data.Entities
{
    public class Ban
    {
        public Ban(string description, bool banned)
        {
            Description = description;
            BannedOn = DateTime.Now;
            Banned = banned;
        }
        public int BanId { get; set; }
        public bool Banned { get; set; }
        public string Description { get; set; }
        public DateTime BannedOn { get; set; }
    }
}
