﻿namespace BeerOverflowData.Entities.Navigational
{
    public class BeerCountry
    {
        public int BeerCountryId { get; set; }
        public int BeerId { get; set; }
        public Beer Beer { get; set; }
        public int CountryId { get; set; }
        public Country Country { get; set; }
    }
}
