﻿using SqlDB.Entities;

namespace BeerOverflowData.Entities.Navigational
{
    public class BeerStyle
    {
        public int BeerStyleId { get; set; }
        public int BeerId { get; set; }
        public Beer Beer { get; set; }
        public int StyleId { get; set; }
        public Style Style { get; set; }
    }
}
