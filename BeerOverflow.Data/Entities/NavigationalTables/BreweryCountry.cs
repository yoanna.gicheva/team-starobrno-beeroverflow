﻿namespace BeerOverflowData.Entities.Navigational
{
    public class BreweryCountry
    {
        public int BreweryCountryId { get; set; }
        public int BreweryId { get; set; }
        public Brewery Brewery { get; set; }
        public int CountryId { get; set; }
        public Country Country { get; set; }
    }
}
