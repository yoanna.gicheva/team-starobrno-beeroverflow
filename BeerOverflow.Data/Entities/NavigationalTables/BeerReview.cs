﻿using BeerOverflowData.Entities.Lists;

namespace BeerOverflowData.Entities.Navigational
{
    public class BeerReview
    {
        public int BeerReviewId { get; set; }
        public int BeerId { get; set; }
        public Beer Beer { get; set; }
        public int ReviewId { get; set; }
        public Review Review { get; set; }
    }
}
