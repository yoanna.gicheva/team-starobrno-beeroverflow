﻿namespace BeerOverflowData.Entities.Navigational
{
    public class BeerBrewery
    {
        public int BeerBreweryId { get; set; }
        public int BeerId { get; set; }
        public Beer Beer { get; set; }
        public int BreweryId { get; set; }
        public Brewery Brewery { get; set; }
    }
}
