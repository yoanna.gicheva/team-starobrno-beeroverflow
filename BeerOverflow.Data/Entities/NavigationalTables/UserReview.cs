﻿namespace BeerOverflowData.Entities.Navigational
{
    public class UserReview
    {
        public int UserReviewId { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public int BeerId { get; set; }
        public Beer Beer { get; set; }
    }
}
