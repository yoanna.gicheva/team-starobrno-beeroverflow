﻿using BeerOverflow.Data.Entities.Lists;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace BeerOverflow.Data.Entities
{
    public class User : IdentityUser<int>
    {
        public User()
        {
            UserModifications = new List<UserModification>();
            CreatedOn = DateTime.Now;
            LastAccessedOn = DateTime.Now;
            Reviews = new List<Review>();
            WishList = new List<Wish>();
            DrankList = new List<Drank>();
            Votes = new List<Vote>();
            RatedBeers = new List<Beer>();
        }
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime LastAccessedOn { get; set; }
        public Country Country { get; set; }
        public int? CountryId { get; set; }
        public ICollection<Beer> RatedBeers { get; set; }
        public ICollection<Review> Reviews { get; set; }
        public ICollection<Wish> WishList { get; set; }
        public ICollection<Drank> DrankList { get; set; }
        public ICollection<Vote> Votes { get; set; }
        public ICollection<UserModification> UserModifications { get; set; }
        public bool IsDeleted { get; set; }
        public Ban Ban { get; set; }
        public int? BanId { get; set; }
    }
}
