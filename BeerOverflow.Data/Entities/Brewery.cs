﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Data.Entities
{
    public class Brewery
    {
        public Brewery()
        {
            Beers = new List<Beer>();
            IsDeleted = false;
        }
        public string Description { get; set; }
        public string Image { get; set; }
        public int BreweryId { get; set; }
        public string BreweryName { get; set; }
        public Country Country { get; set; }
        public int CountryId { get; set; }
        public bool IsDeleted { get; set; }
        public ICollection<Beer> Beers { get; set; }
    }
}
