﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Data.Entities
{
    public class Style
    {
        public int StyleId { get; set; }
        public string StyleName { get; set; }
    }
}
