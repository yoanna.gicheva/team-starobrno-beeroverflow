﻿using BeerOverflow.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Data.Seeder
{
    public static class ModelBuilderExtension
    {
        public static void RelationshipSetter(this ModelBuilder model)
        {
            foreach (var relationship in model.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
        }
        public static void Seeder(this ModelBuilder builder)
        {

            builder.Entity<Country>().HasData
                (
                    new Country
                    {
                        CountryId = 1,
                        CountryName = "Austria",
                        //IsDeleted = false
                    },
                    new Country
                    {
                        CountryId = 2,
                        CountryName = "Azerbaijan",
                        //IsDeleted = false
                    },
                    new Country
                    {
                        CountryId = 3,
                        CountryName = "Armenia",
                        // IsDeleted = false
                    },
                    new Country
                    {
                        CountryId = 4,
                        CountryName = "Antigua and Barbuda",
                        // IsDeleted = false
                    },
                    new Country
                    {
                        CountryId = 5,
                        CountryName = "Afghanistan",
                        // IsDeleted = false
                    },
                    new Country
                    {
                        CountryId = 6,
                        CountryName = "Albania",
                        // IsDeleted = false
                    },
                    new Country
                    {
                        CountryId = 7,
                        CountryName = "Andorra",
                        // IsDeleted = false
                    },
                    new Country
                    {
                        CountryId = 8,
                        CountryName = "Argentina",
                        // IsDeleted = false
                    },
                    new Country
                    {
                        CountryId = 9,
                        CountryName = "Bangladesh",
                        // IsDeleted = false
                    },
                    new Country
                    {
                        CountryId =10,
                        CountryName = "Bahamas",
                        // IsDeleted = false
                    },
                    new Country
                    {
                        CountryId = 11,
                        CountryName = "Belize",
                        // IsDeleted = false
                    },
                    new Country
                    {
                        CountryId = 12,
                        CountryName = "Belgium",
                        // IsDeleted = false
                    },
                    new Country
                    {
                        CountryId = 13,
                        CountryName = "Bulgaria",
                        // IsDeleted = false
                    },
                    new Country
                    {
                        CountryId = 14,
                        CountryName = "Burundi",
                        // IsDeleted = false
                    },
                    new Country
                    {
                        CountryId = 15,
                        CountryName = "Cameroon",
                        // IsDeleted = false
                    },
                    new Country
                    {
                        CountryId = 16,
                        CountryName = "Chile",
                        // IsDeleted = false
                    },
                    new Country
                    {
                        CountryId = 17,
                        CountryName = "Croatia",
                        // IsDeleted = false
                    },
                    new Country
                    {
                        CountryId = 18,
                        CountryName = "Djibouti",
                        // IsDeleted = false
                    },
                    new Country
                    {
                        CountryId = 19,
                        CountryName = "Egypt",
                        // IsDeleted = false
                    },
                    new Country
                    {
                        CountryId = 20,
                        CountryName = "Ethiopia",
                        // IsDeleted = false
                    },
                    new Country
                    {
                        CountryId = 21,
                        CountryName = "France",
                        // IsDeleted = false
                    },
                    new Country
                    {
                        CountryId = 22,
                        CountryName = "Gabon",
                        // IsDeleted = false
                    },
                    new Country
                    {
                        CountryId = 23,
                        CountryName = "Iraq",
                        // IsDeleted = false
                    },
                    new Country
                    {
                        CountryId = 24,
                        CountryName = "Maldives",
                        // IsDeleted = false
                    },
                    new Country
                    {
                        CountryId = 25,
                        CountryName = "Netherlands",
                        // IsDeleted = false
                    },
                    new Country
                    {
                        CountryId = 26,
                        CountryName = "Niger",
                        // IsDeleted = false
                    },
                    new Country
                    {
                        CountryId = 27,
                        CountryName = "Belgium",
                        // IsDeleted = false
                    },
                    new Country
                    {
                        CountryId = 28,
                        CountryName = "Canada",
                        // IsDeleted = false
                    },
                    new Country
                    {
                        CountryId = 29,
                        CountryName = "Philippines",
                        // IsDeleted = false
                    },
                    new Country
                    {
                        CountryId = 30,
                        CountryName = "Poland",
                        // IsDeleted = false
                    },
                    new Country
                    {
                        CountryId = 31,
                        CountryName = "Senegal",
                        // IsDeleted = false
                    },
                    new Country
                    {
                        CountryId = 32,
                        CountryName = "Seychelles",
                        // IsDeleted = false
                    },
                    new Country
                    {
                        CountryId = 33,
                        CountryName = "Slovenia",
                        // IsDeleted = false
                    },
                    new Country
                    {
                        CountryId = 34,
                        CountryName = "Somalia",
                        // IsDeleted = false
                    }
                );
            builder.Entity<Brewery>().HasData
                (
                    new Brewery
                    {
                        BreweryId = 1,
                        BreweryName = "Sofiisko Pivo",
                        CountryId = 1
                    },
                    new Brewery
                    {
                        BreweryId = 2,
                        BreweryName = "Carlsberg Group",
                        CountryId = 2
                    },
                    new Brewery
                    {
                        BreweryId = 3,
                        BreweryName = "Heineken",
                        CountryId = 3
                    },
                    new Brewery
                    {
                        BreweryId = 4,
                        BreweryName = "Stella Artois",
                        CountryId = 4
                    }, new Brewery
                    {
                        BreweryId = 5,
                        BreweryName = "Ritual Brew Co.",
                        CountryId = 1
                    },
                    new Brewery
                    {
                        BreweryId = 6,
                        BreweryName = "Rebel Days Brew Co.",
                        CountryId = 3
                    },
                    new Brewery
                    {
                        BreweryId = 7,
                        BreweryName = "Sanctity",
                        CountryId = 5
                    },
                    new Brewery
                    {
                        BreweryId = 8,
                        BreweryName = "White Rabbit",
                        CountryId = 6
                    }, new Brewery
                    {
                        BreweryId = 9,
                        BreweryName = "Lone Rider",
                        CountryId = 7
                    },
                    new Brewery
                    {
                        BreweryId = 10,
                        BreweryName = "9 Lives",
                        CountryId = 8
                    },
                    new Brewery
                    {
                        BreweryId = 11,
                        BreweryName = "Acid Rest",
                        CountryId = 9
                    },
                    new Brewery
                    {
                        BreweryId = 12,
                        BreweryName = "Chill Haze",
                        CountryId = 10
                    }, new Brewery
                    {
                        BreweryId = 13,
                        BreweryName = "Mouthfeel",
                        CountryId = 11
                    },
                    new Brewery
                    {
                        BreweryId =14,
                        BreweryName = "Yeast Cake",
                        CountryId = 12
                    },
                    new Brewery
                    {
                        BreweryId = 15,
                        BreweryName = "Dank Brew House",
                        CountryId = 13
                    },
                    new Brewery
                    {
                        BreweryId = 16,
                        BreweryName = "Barley and Basil Brewery",
                        CountryId = 14
                    }, new Brewery
                    {
                        BreweryId = 17,
                        BreweryName = "Apprentice Brewing",
                        CountryId = 15
                    },
                    new Brewery
                    {
                        BreweryId = 18,
                        BreweryName = "El Diablo Brewing",
                        CountryId = 16
                    },
                    new Brewery
                    {
                        BreweryId = 19,
                        BreweryName = "Candle Wick",
                        CountryId = 17
                    },
                    new Brewery
                    {
                        BreweryId = 20,
                        BreweryName = "The Mistress",
                        CountryId = 18
                    }, new Brewery
                    {
                        BreweryId = 21,
                        BreweryName = "Space Age",
                        CountryId = 19
                    },
                    new Brewery
                    {
                        BreweryId = 22,
                        BreweryName = "Alpha Beta",
                        CountryId = 20
                    },
                    new Brewery
                    {
                        BreweryId = 23,
                        BreweryName = "Heineken",
                        CountryId = 21
                    },
                    new Brewery
                    {
                        BreweryId = 24,
                        BreweryName = "Cold Break",
                        CountryId = 22
                    }, new Brewery
                    {
                        BreweryId = 25,
                        BreweryName = "Turbid Days",
                        CountryId = 23
                    },
                    new Brewery
                    {
                        BreweryId = 26,
                        BreweryName = "Slow Coast Brewing",
                        CountryId = 25
                    },
                    new Brewery
                    {
                        BreweryId = 27,
                        BreweryName = "Brier Brewing",
                        CountryId = 26
                    },
                    new Brewery
                    {
                        BreweryId = 28,
                        BreweryName = "Alpha Waves",
                        CountryId = 24
                    }, new Brewery
                    {
                        BreweryId = 29,
                        BreweryName = "Nitrogen Brew Co.",
                        CountryId = 25
                    },
                    new Brewery
                    {
                        BreweryId = 30,
                        BreweryName = "Disaster Brewing",
                        CountryId = 26
                    },
                    new Brewery
                    {
                        BreweryId = 31,
                        BreweryName = "Apothecary Brewing",
                        CountryId = 27
                    },
                    new Brewery
                    {
                        BreweryId = 32,
                        BreweryName = "Gone Rogue",
                        CountryId = 28
                    }, new Brewery
                    {
                        BreweryId = 33,
                        BreweryName = "Narwhal Brewing Co",
                        CountryId = 29
                    },
                    new Brewery
                    {
                        BreweryId = 34,
                        BreweryName = "Spyglass",
                        CountryId = 30
                    },
                    new Brewery
                    {
                        BreweryId = 35,
                        BreweryName = "Bomber Bottles",
                        CountryId = 31
                    },
                    new Brewery
                    {
                        BreweryId = 36,
                        BreweryName = "Final Gravity",
                        CountryId = 32
                    }
                );
            builder.Entity<Style>().HasData
                (
                   new Style
                   {
                       StyleId = 1,
                       StyleName = "Amber ale"
                   },
                   new Style
                   {
                       StyleId = 2,
                       StyleName = "Barley wine"
                   },
                   new Style
                   {
                       StyleId = 3,
                       StyleName = "Bitter"
                   },
                   new Style
                   {
                       StyleId = 4,
                       StyleName = "Blonde Ale"
                   },
                   new Style
                   {
                       StyleId = 5,
                       StyleName = "Light ale"
                   },
                   new Style
                   {
                       StyleId = 6,
                       StyleName = "Old ale"
                   },
                   new Style
                   {
                       StyleId = 7,
                       StyleName = "Light"
                   },
                   new Style
                   {
                       StyleId = 8,
                       StyleName = "Dark"
                   }

                );
            builder.Entity<Beer>().HasData
                (
                       new Beer
                       {

                           BeerId = 1,
                           Image = "https://p1.akcdn.net/full/570589092.bira-ariana-500ml.jpg",
                           BeerName = "Ariana",
                           StyleId = 1,
                           CountryId = 1,
                           Abv = 5.5,
                           Rating = 5,
                           BreweryId = 1,
                           IsDeleted = false
                       },
                       new Beer
                       {
                           BeerId = 2,
                           BeerName = "Carlsberg",
                           Image = "https://www.carlsberg.com/media/2399/cb_nd_stemmed_glass_with_33cl_pilsner_bottle_wet_combo_rgb_72dpi.jpg",
                           StyleId = 2,
                           CountryId = 2,
                           Abv = 4,
                           Rating = 5,
                           RatedTimes = 1,
                           BreweryId = 2,
                           IsDeleted = false
                       },
                       new Beer
                       {
                           Image = "https://cdn.pixabay.com/photo/2020/04/18/04/24/beer-5057609_960_720.jpg",
                           BeerId = 3,
                           BeerName = "Heineken",
                           StyleId = 3,
                           CountryId = 3,
                           Abv = 5.3,
                           Rating = 5,
                           RatedTimes = 1,
                           BreweryId = 3,
                           IsDeleted = false
                       },
                       new Beer
                       {
                           Image= "https://tobood.bg/assets/products/large/pd_ab8117195266147249c980cd04934420_9a6ad59857e04bb2f0ada20d164be708.jpg",
                           BeerId = 4,
                           BeerName = "Stella Artois",
                           StyleId = 4,
                           CountryId = 4,
                           Abv = 5.3,
                           Rating = 5,
                           RatedTimes = 1,
                           BreweryId = 4,
                           IsDeleted = false
                       },
                        new Beer
                        {
                            BeerId = 5,
                            Image = "https://coolmaterial.com/wp-content/uploads/2017/05/Toppling-Goliath-Kentucky-Brunch-Brand-Stout.jpg",
                            BeerName = "Kentucky Brunch Brand Stout",
                            StyleId = 5,
                            CountryId = 5,
                            Abv = 5.5,
                            Rating = 5,
                            BreweryId = 1,
                            IsDeleted = false
                        },
                       new Beer
                       {
                           BeerId = 6,
                           BeerName = "Marshmallow Handjee",
                           Image = "https://i.imgur.com/zq9u2bN.jpg",
                           StyleId = 6,
                           CountryId = 6,
                           Abv = 4,
                           Rating = 5,
                           RatedTimes = 1,
                           BreweryId = 2,
                           IsDeleted = false
                       },
                       new Beer
                       {
                           Image= "https://cdn.shopify.com/s/files/1/0227/0581/products/Goose-Island-Bourbon-County-Brand-Stout-2019-500ML-BTL_grande.jpg",
                           BeerId = 7,
                           BeerName = "Bourbon County Brand Vanilla Stout",
                           StyleId = 7,
                           CountryId = 7,
                           Abv = 5.3,
                           Rating = 5,
                           RatedTimes = 1,
                           BreweryId = 3,
                           IsDeleted = false
                       },
                       new Beer
                       {
                           Image= "https://products2.imgix.drizly.com/ci-three-floyds-zombie-dust-4b8ac4a627956811.png?auto=format%2Ccompress&fm=jpg",
                           BeerId = 8,
                           BeerName = "Zombie Dust",
                           StyleId = 8,
                           CountryId = 8,
                           Abv = 5.3,
                           Rating = 5,
                           RatedTimes = 1,
                           BreweryId = 4,
                           IsDeleted = false
                       }, new Beer
                       {
                           BeerId = 9,
                           Image = "https://cdn.webshopapp.com/shops/65337/files/56029926/800x600x2/wb-fles-cantillon-lou-pepe-pure-kriek.jpg",
                           BeerName = "Lou Pepe - Kriek",
                           StyleId = 1,
                           CountryId = 9,
                           Abv = 5.5,
                           Rating = 5,
                           BreweryId = 1,
                           IsDeleted = false
                       },
                       new Beer
                       {
                           BeerId = 10,
                           BeerName = "Morning Wood",
                           Image = "https://48tk9j3a74jb133e1k2fzz2s-wpengine.netdna-ssl.com/wp-content/uploads/2015/11/Funky-Buddha-Bourbon-Barrel-Aged-Morning-Wood-1.jpg",
                           StyleId = 2,
                           CountryId = 10,
                           Abv = 4,
                           Rating = 5,
                           RatedTimes = 1,
                           BreweryId = 2,
                           IsDeleted = false
                       },
                       new Beer
                       {
                           Image= "https://48tk9j3a74jb133e1k2fzz2s-wpengine.netdna-ssl.com/wp-content/uploads/2014/10/The-Lost-Abbey-Duck-Duck-Gooze-2013-Bottle.jpg",
                           BeerId = 11,
                           BeerName = "Duck Duck Gooze",
                           StyleId = 3,
                           CountryId = 11,
                           Abv = 5.3,
                           Rating = 5,
                           RatedTimes = 1,
                           BreweryId = 3,
                           IsDeleted = false
                       },
                       new Beer
                       {
                           Image= "https://healthyspiritssf.com/wp-content/uploads/2015/04/parabola-img.png",
                           BeerId = 12,
                           BeerName = "Parabola",
                           StyleId = 4,
                           CountryId = 12,
                           Abv = 5.3,
                           Rating = 5,
                           RatedTimes = 1,
                           BreweryId = 4,
                           IsDeleted = false
                       }, new Beer
                       {
                           BeerId = 13,
                           Image = "https://thefullpint.com/wp-content/uploads/Lawsons-Finest-Liquids-Double-Sunshine-IPA-scaled.jpg",
                           BeerName = "Double Sunshine",
                           StyleId = 5,
                           CountryId = 13,
                           Abv = 5.5,
                           RatedTimes=1,
                           Rating = 5,
                           BreweryId = 1,
                           IsDeleted = false
                       },
                       new Beer
                       {
                           BeerId = 14,
                           BeerName = "Julius",
                           Image = "https://cdn.webshopapp.com/shops/19852/files/302874105/brouwerij-hoegaarden-julius-blond.jpg",
                           StyleId = 6,
                           CountryId = 14,
                           Abv = 4,
                           Rating = 5,
                           RatedTimes = 1,
                           BreweryId = 2,
                           IsDeleted = false
                       },
                       new Beer
                       {
                           Image= "https://www.mybeercollectibles.com/uploads/cache/DxC3WBmXQAApXyj-500x500-crop.jpg",
                           BeerId = 15,
                           BeerName = "Very Green",
                           StyleId = 7,
                           CountryId = 15,
                           Abv = 5.3,
                           Rating = 5,
                           RatedTimes = 1,
                           BreweryId = 3,
                           IsDeleted = false
                       },
                       new Beer
                       {
                           Image= "https://res.cloudinary.com/ratebeer/image/upload/w_250,c_limit/beer_547307.jpg",
                           BeerId = 16,
                           BeerName = "Assassin",
                           StyleId = 8,
                           CountryId = 16,
                           Abv = 5.3,
                           Rating = 5,
                           RatedTimes = 1,
                           BreweryId = 4,
                           IsDeleted = false
                       }, new Beer
                       {
                           BeerId = 17,
                           Image = "https://store.belgianshop.com/3543-large_default/cantillon-foufoune-5-34l-v.jpg",
                           BeerName = "Fou' Foune",
                           StyleId = 1,
                           RatedTimes=1,
                           CountryId = 17,
                           Abv = 5.5,
                           Rating = 5,
                           BreweryId = 1,
                           IsDeleted = false
                       },
                       new Beer
                       {
                           BeerId = 18,
                           BeerName = "Pliny The Elder",
                           Image = "https://thefullpint.com/wp-content/uploads/Russian-River-Pliny-the-Elder.jpg",
                           StyleId = 2,
                           CountryId = 18,
                           Abv = 4,
                           Rating = 5,
                           RatedTimes = 1,
                           BreweryId = 29,
                           IsDeleted = false
                       },
                       new Beer
                       {
                           Image= "https://cdn.shopify.com/s/files/1/1351/8119/products/Founders-CBS-2017-355ml_IMG_6958_1024x1024.jpg",
                           BeerId = 19,
                           BeerName = "CBS (Canadian Breakfast Stout)",
                           StyleId = 3,
                           CountryId = 19,
                           Abv = 5.3,
                           Rating = 5,
                           RatedTimes = 1,
                           BreweryId = 3,
                           IsDeleted = false
                       },
                       new Beer
                       {
                           Image= "https://joshbeerme.files.wordpress.com/2015/07/imag0657_1.jpg",
                           BeerId = 20,
                           BeerName = "Fundamental Observation",
                           StyleId = 4,
                           CountryId = 20,
                           Abv = 5.3,
                           Rating = 5,
                           RatedTimes = 1,
                           BreweryId = 8,
                           IsDeleted = false
                       }, new Beer
                       {
                           BeerId = 21,
                           Image = "https://www.tgbrews.com/wp-content/uploads/2017/04/0.SR71.REFORMAT.1-480x720.jpg",
                           BeerName = "SR-71",
                           StyleId = 5,
                           CountryId = 21,
                           Abv = 5.5,
                           Rating = 5,
                           RatedTimes = 1,
                           BreweryId = 7,
                           IsDeleted = false
                       },
                       new Beer
                       {
                           BeerId = 22,
                           BeerName = "Very Hazy",
                           Image = "https://i.redd.it/vcw2vl1j2vj11.jpg",
                           StyleId = 6,
                           CountryId = 22,
                           Abv = 4,
                           Rating = 5,
                           RatedTimes = 1,
                           BreweryId = 6,
                           IsDeleted = false
                       },
                       new Beer
                       {
                           Image= "https://www.thebeersbeer.com/wp-content/uploads/2019/06/beer-king-julius-style-ipa.jpg",
                           BeerId = 23,
                           BeerName = "King Julius",
                           StyleId = 7,
                           CountryId = 23,
                           Abv = 5.3,
                           Rating = 5,
                           RatedTimes = 1,
                           BreweryId = 5,
                           IsDeleted = false
                       },
                       new Beer
                       {
                           Image= "https://s.hdnux.com/photos/01/07/56/07/18801124/5/920x920.jpg",
                           BeerId = 24,
                           BeerName = "Pliny The Younger",
                           StyleId = 8,
                           CountryId = 24,
                           Abv = 5.3,
                           Rating = 5,
                           RatedTimes = 1,
                           BreweryId = 4,
                           IsDeleted = false
                       }, new Beer
                       {
                           BeerId = 25,
                           Image = "https://i.pinimg.com/originals/5a/47/4d/5a474dfa0f673a1ff237a3c30d3872d1.jpg",
                           BeerName = "Mornin' Delight",
                           StyleId = 1,
                           CountryId = 25,
                           Abv = 5.5,
                           Rating = 5,
                           BreweryId = 4,
                           RatedTimes = 1,
                           IsDeleted = false
                       },
                       new Beer
                       {
                           BeerId = 26,
                           BeerName = "Heady Topper",
                           Image = "https://i1.wp.com/www.delmesaliquor.com/wp-content/uploads/2018/12/The-Alchemist-Heady-Topper-American-DIPA.jpg",
                           StyleId = 2,
                           CountryId = 26,
                           Abv = 4,
                           Rating = 5,
                           RatedTimes = 1,
                           BreweryId = 3,
                           IsDeleted = false
                       },
                       new Beer
                       {
                           Image= "https://i.pinimg.com/originals/33/f9/03/33f903416491175c99f110b9c8b25cfc.jpg",
                           BeerId = 27,
                           BeerName = "Barrel-Aged Abraxas",
                           StyleId = 3,
                           CountryId = 27,
                           Abv = 5.3,
                           Rating = 5,
                           RatedTimes = 1,
                           BreweryId = 2,
                           IsDeleted = false
                       },
                       new Beer
                       {
                           Image= "https://48tk9j3a74jb133e1k2fzz2s-wpengine.netdna-ssl.com/wp-content/uploads/2015/01/Cigar-City-Hunahpu%E2%80%99s-Imperial-Stout.jpg",
                           BeerId = 28,
                           BeerName = "Hunahpu's Imperial Stout - Double Barrel Aged",
                           StyleId = 4,
                           CountryId = 28,
                           Abv = 5.3,
                           Rating = 5,
                           RatedTimes = 1,
                           BreweryId = 1,
                           IsDeleted = false
                       }
                );
            builder.Entity<Ban>().HasData
                (
                new Ban("Default", false) { BanId = 1 },
                new Ban("Default", false) { BanId = 2 },    
                new Ban("Default", false) { BanId = 999 }
                );
            var hasher = new PasswordHasher<User>();

            User user1 = new User
            {
                Id = 1,
                BanId = 1,
                UserName = "bona",
                NormalizedUserName = "BONA",
                Description = "C# developer",
                Email = "bona@gmail.com",
                NormalizedEmail = "BONA@GMAIL.COM",
                CountryId = 1,
                CreatedOn = DateTime.Now,
                SecurityStamp = "DC6E275DD221E24957A7781D42BB68293B"

            };
            User user2 = new User
            {
                Id = 2,
                BanId = 2,
                UserName = "yoja",
                NormalizedUserName = "YOJA",
                Description = "Studying in Telerik",
                Email = "yoja@gmail.com",
                NormalizedEmail = "YOJA@GMAIL.COM",
                CountryId = 2,
                CreatedOn = DateTime.Now,
                SecurityStamp = "DC6E275DD1E2495347A7781D42BB68293B"
            };
            user1.PasswordHash = hasher.HashPassword(user1, "123456gg");
            user2.PasswordHash = hasher.HashPassword(user2, "amo967");

            builder.Entity<User>().HasData
                (user1,user2);
            builder.Entity<Role>().HasData(
               new Role { Id = 1, Name = "Admin", NormalizedName = "ADMIN" },
               new Role { Id = 2, Name = "Member", NormalizedName = "MEMBER" }
           );

            //Admin Account

            User adminUser = new User
            {
                Id = 999,
                BanId = 999,
                UserName = "admin1",
                NormalizedUserName = "ADMIN1",
                Email = "admin@bo.com",
                NormalizedEmail = "ADMIN@BO.COM",
                CreatedOn = DateTime.UtcNow,
                LockoutEnabled = true,
                SecurityStamp = "DC6E275DD1E24957A7781D42BB68293B"
            };
            adminUser.PasswordHash = hasher.HashPassword(adminUser, "admin1");
            builder.Entity<User>().HasData(adminUser);

            builder.Entity<IdentityUserRole<int>>().HasData(
                new IdentityUserRole<int>
                {
                    RoleId = 1,
                    UserId = adminUser.Id
                },
                new IdentityUserRole<int>
                {
                    RoleId = 2,
                    UserId = 2
                },
                new IdentityUserRole<int>
                {
                    RoleId = 2,
                    UserId = 1
                });
        }
    }
}