﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using BeerOverflow.Data.Entities;

namespace BeerOverflowBeerOverflow.DataData.ConfigurationModels
{
    public class BreweryConfig : IEntityTypeConfiguration<Brewery>
    {
        public void Configure(EntityTypeBuilder<Brewery> builder)
        {
            builder.HasKey(p => p.BreweryId);
            builder.Property(p => p.BreweryName);
            builder.HasMany(p => p.Beers).WithOne(p => p.Brewery);             
        }
    }
}
