﻿using BeerOverflow.Data.Entities.Lists;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BeerOverflow.Data.ConfigurationModels.ListsConfig
{
    public class WishListConfig : IEntityTypeConfiguration<Wish>
    {
        public void Configure(EntityTypeBuilder<Wish> builder)
        {
            builder.HasKey(p => p.WishListId);
            builder.HasOne(p => p.User).WithMany(p => p.WishList);
            builder.HasOne(p => p.Beer);
        }
    }
}
