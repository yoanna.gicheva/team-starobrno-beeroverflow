﻿using BeerOverflow.Data.Entities.Lists;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Data.ConfigurationModels.ListsConfig
{
    public class UserModificationConfig : IEntityTypeConfiguration<UserModification>
    {
        public void Configure(EntityTypeBuilder<UserModification> builder)
        {
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Message);
            builder.Property(p => p.Time).IsRequired();
            builder.HasOne(p => p.User).WithMany(p => p.UserModifications);
        }
    }
}
