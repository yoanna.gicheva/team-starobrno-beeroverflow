﻿using BeerOverflow.Data.Entities.Lists;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Data.ConfigurationModels
{
    public class ReviewConfig : IEntityTypeConfiguration<Review>
    {
        public void Configure(EntityTypeBuilder<Review> builder)
        {
            builder.HasKey(p => p.ReviewId);
            builder.Property(p => p.Text);
            builder.HasOne(p => p.User).WithMany(p => p.Reviews);
            builder.HasOne(p => p.Beer).WithMany(p => p.Reviews);
            builder.HasMany(p => p.Votes).WithOne(p => p.Review);

        }
    }
}
