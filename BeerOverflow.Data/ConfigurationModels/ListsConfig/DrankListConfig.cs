﻿using BeerOverflow.Data.Entities.Lists;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Data.ConfigurationModels.ListsConfig
{
    public class DrankListConfig : IEntityTypeConfiguration<Drank>
    {
        public void Configure(EntityTypeBuilder<Drank> builder)
        {
            builder.HasKey(p => p.DrankId);
            builder.HasOne(p => p.User).WithMany(p => p.DrankList);
            builder.HasOne(p => p.Beer);
        }
    }
}
