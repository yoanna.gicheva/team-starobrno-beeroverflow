﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using BeerOverflow.Data.Entities;

namespace BeerOverflow.Data.ConfigurationModels
{
    public class BeerConfig : IEntityTypeConfiguration<Beer>
    {
        public void Configure(EntityTypeBuilder<Beer> builder)
        {
            builder.HasKey(p => p.BeerId);
            builder.Property(p => p.BeerName).IsRequired();
            builder.Property(p => p.Sum);
            builder.HasOne(p => p.Country);            
            builder.HasOne(p => p.Style);
            builder.Property(b => b.Abv).IsRequired();
            builder.Property(b => b.Rating).IsRequired();
            builder.HasOne(o => o.Brewery).WithMany(o => o.Beers);
            builder.HasMany(p => p.Reviews).WithOne(p => p.Beer);
        }
    }
}
