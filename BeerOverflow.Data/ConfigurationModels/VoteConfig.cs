﻿using BeerOverflow.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BeerOverflow.Data.ConfigurationModels
{
    public class VoteConfig : IEntityTypeConfiguration<Vote>
    {
        public void Configure(EntityTypeBuilder<Vote> builder)
        {
            builder.HasKey(p => p.VoteId);
            builder.HasOne(p => p.Review).WithMany(p=>p.Votes);
            builder.HasOne(p => p.User).WithMany(p => p.Votes);

        }
    }
}
