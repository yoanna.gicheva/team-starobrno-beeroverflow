﻿using BeerOverflow.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Data.ConfigurationModels
{
    public class CountryConfig : IEntityTypeConfiguration<Country>
    {
        public void Configure(EntityTypeBuilder<Country> builder)
        {
            builder.HasKey(p => p.CountryId);
            builder.Property(x => x.CountryName).HasMaxLength(20);
            builder.HasMany(p => p.Breweries).WithOne(p => p.Country);
            builder.Property(x => x.IsDeleted);
        }
    }
}
