﻿using BeerOverflow.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BeerOverflow.Data.ConfigurationModels
{
    public class UserConfig : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasMany(p => p.Reviews).WithOne(p => p.User);
            builder.HasMany(p => p.Votes).WithOne(p => p.User);
            builder.HasMany(p => p.UserModifications).WithOne(p => p.User);
            builder.HasOne(u => u.Ban);
        }
    }
}
