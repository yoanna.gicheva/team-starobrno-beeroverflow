﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BeerOverflow.Data.Migrations
{
    public partial class Logg : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Bans",
                columns: table => new
                {
                    BanId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Banned = table.Column<bool>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    BannedOn = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bans", x => x.BanId);
                });

            migrationBuilder.CreateTable(
                name: "Countries",
                columns: table => new
                {
                    CountryId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CountryName = table.Column<string>(maxLength: 20, nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Countries", x => x.CountryId);
                });

            migrationBuilder.CreateTable(
                name: "Styles",
                columns: table => new
                {
                    StyleId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StyleName = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Styles", x => x.StyleId);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    LastAccessedOn = table.Column<DateTime>(nullable: false),
                    CountryId = table.Column<int>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    BanId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUsers_Bans_BanId",
                        column: x => x.BanId,
                        principalTable: "Bans",
                        principalColumn: "BanId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AspNetUsers_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Countries",
                        principalColumn: "CountryId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Breweries",
                columns: table => new
                {
                    BreweryId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Description = table.Column<string>(nullable: true),
                    Image = table.Column<string>(nullable: true),
                    BreweryName = table.Column<string>(nullable: true),
                    CountryId = table.Column<int>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Breweries", x => x.BreweryId);
                    table.ForeignKey(
                        name: "FK_Breweries_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Countries",
                        principalColumn: "CountryId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(maxLength: 128, nullable: false),
                    ProviderKey = table.Column<string>(maxLength: 128, nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    RoleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    LoginProvider = table.Column<string>(maxLength: 128, nullable: false),
                    Name = table.Column<string>(maxLength: 128, nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserModifications",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Time = table.Column<DateTime>(nullable: false),
                    Message = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserModifications", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserModifications_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Beers",
                columns: table => new
                {
                    BeerId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BeerName = table.Column<string>(nullable: false),
                    Image = table.Column<string>(nullable: true),
                    StyleId = table.Column<int>(nullable: false),
                    RatedTimes = table.Column<int>(nullable: false),
                    CountryId = table.Column<int>(nullable: false),
                    Abv = table.Column<double>(nullable: false),
                    Rating = table.Column<double>(nullable: false),
                    BreweryId = table.Column<int>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    UserId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Beers", x => x.BeerId);
                    table.ForeignKey(
                        name: "FK_Beers_Breweries_BreweryId",
                        column: x => x.BreweryId,
                        principalTable: "Breweries",
                        principalColumn: "BreweryId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Beers_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Countries",
                        principalColumn: "CountryId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Beers_Styles_StyleId",
                        column: x => x.StyleId,
                        principalTable: "Styles",
                        principalColumn: "StyleId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Beers_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DrankLists",
                columns: table => new
                {
                    DrankId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(nullable: false),
                    BeerId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DrankLists", x => x.DrankId);
                    table.ForeignKey(
                        name: "FK_DrankLists_Beers_BeerId",
                        column: x => x.BeerId,
                        principalTable: "Beers",
                        principalColumn: "BeerId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DrankLists_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Reviews",
                columns: table => new
                {
                    ReviewId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(nullable: false),
                    Text = table.Column<string>(nullable: true),
                    BeerId = table.Column<int>(nullable: false),
                    PositiveVotes = table.Column<int>(nullable: false),
                    NegativeVotes = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    Flagged = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reviews", x => x.ReviewId);
                    table.ForeignKey(
                        name: "FK_Reviews_Beers_BeerId",
                        column: x => x.BeerId,
                        principalTable: "Beers",
                        principalColumn: "BeerId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Reviews_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WishLists",
                columns: table => new
                {
                    WishListId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(nullable: false),
                    BeerId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WishLists", x => x.WishListId);
                    table.ForeignKey(
                        name: "FK_WishLists_Beers_BeerId",
                        column: x => x.BeerId,
                        principalTable: "Beers",
                        principalColumn: "BeerId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WishLists_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Votes",
                columns: table => new
                {
                    VoteId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ThumbsUp = table.Column<bool>(nullable: false),
                    ReviewId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Votes", x => x.VoteId);
                    table.ForeignKey(
                        name: "FK_Votes_Reviews_ReviewId",
                        column: x => x.ReviewId,
                        principalTable: "Reviews",
                        principalColumn: "ReviewId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Votes_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { 2, "dc66a48b-f944-42af-940d-75a64b99491a", "Member", "MEMBER" },
                    { 1, "78127db6-e26c-4f9f-b7c3-24a2d6c7b7ba", "Admin", "ADMIN" }
                });

            migrationBuilder.InsertData(
                table: "Bans",
                columns: new[] { "BanId", "Banned", "BannedOn", "Description" },
                values: new object[,]
                {
                    { 1, false, new DateTime(2020, 5, 5, 3, 14, 31, 146, DateTimeKind.Local).AddTicks(4340), "Default" },
                    { 2, false, new DateTime(2020, 5, 5, 3, 14, 31, 150, DateTimeKind.Local).AddTicks(82), "Default" },
                    { 999, false, new DateTime(2020, 5, 5, 3, 14, 31, 150, DateTimeKind.Local).AddTicks(198), "Default" }
                });

            migrationBuilder.InsertData(
                table: "Countries",
                columns: new[] { "CountryId", "CountryName", "IsDeleted" },
                values: new object[,]
                {
                    { 22, "Gabon", false },
                    { 23, "Iraq", false },
                    { 24, "Maldives", false },
                    { 25, "Netherlands", false },
                    { 26, "Niger", false },
                    { 27, "Belgium", false },
                    { 30, "Poland", false },
                    { 29, "Philippines", false },
                    { 20, "Ethiopia", false },
                    { 31, "Senegal", false },
                    { 32, "Seychelles", false },
                    { 33, "Slovenia", false },
                    { 34, "Somalia", false },
                    { 28, "Canada", false },
                    { 19, "Egypt", false },
                    { 21, "France", false },
                    { 17, "Croatia", false },
                    { 1, "Austria", false },
                    { 2, "Azerbaijan", false },
                    { 3, "Armenia", false },
                    { 4, "Antigua and Barbuda", false },
                    { 5, "Afghanistan", false },
                    { 18, "Djibouti", false },
                    { 7, "Andorra", false },
                    { 8, "Argentina", false },
                    { 6, "Albania", false },
                    { 10, "Bahamas", false },
                    { 11, "Belize", false },
                    { 12, "Belgium", false },
                    { 13, "Bulgaria", false },
                    { 14, "Burundi", false },
                    { 15, "Cameroon", false },
                    { 16, "Chile", false },
                    { 9, "Bangladesh", false }
                });

            migrationBuilder.InsertData(
                table: "Styles",
                columns: new[] { "StyleId", "StyleName" },
                values: new object[,]
                {
                    { 7, "Light" },
                    { 1, "Amber ale" },
                    { 2, "Barley wine" },
                    { 3, "Bitter" },
                    { 4, "Blonde Ale" },
                    { 5, "Light ale" },
                    { 6, "Old ale" },
                    { 8, "Dark" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "BanId", "ConcurrencyStamp", "CountryId", "CreatedOn", "Description", "Email", "EmailConfirmed", "IsDeleted", "LastAccessedOn", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { 999, 0, 999, "6d8bab8b-c925-42be-a08a-82d306ab0a4c", null, new DateTime(2020, 5, 5, 9, 14, 31, 178, DateTimeKind.Utc).AddTicks(1844), null, "admin@bo.com", false, false, new DateTime(2020, 5, 5, 3, 14, 31, 178, DateTimeKind.Local).AddTicks(1827), true, null, "ADMIN@BO.COM", "ADMIN1", "AQAAAAEAACcQAAAAELLjdKTDKo7tpUlIenS21NfXQKKsgzki5cDGA4et855zk4nk3So5fribh+QBWZbcng==", null, false, "DC6E275DD1E24957A7781D42BB68293B", false, "admin1" },
                    { 1, 0, 1, "69f94960-bf60-47b3-9773-4a89658f556f", 1, new DateTime(2020, 5, 5, 3, 14, 31, 152, DateTimeKind.Local).AddTicks(501), "C# developer", "bona@gmail.com", false, false, new DateTime(2020, 5, 5, 3, 14, 31, 151, DateTimeKind.Local).AddTicks(2178), false, null, "BONA@GMAIL.COM", "BONA", "AQAAAAEAACcQAAAAEM+LUINid1L2O4jblt0chlMaecqcMqsuUW9Z9FdOagY9k4lilfdCzDBNzIfd1xZKRw==", null, false, "DC6E275DD221E24957A7781D42BB68293B", false, "bona" },
                    { 2, 0, 2, "d51c8bfa-7394-414d-b3d0-de5e2902f457", 2, new DateTime(2020, 5, 5, 3, 14, 31, 152, DateTimeKind.Local).AddTicks(1264), "Studying in Telerik", "yoja@gmail.com", false, false, new DateTime(2020, 5, 5, 3, 14, 31, 152, DateTimeKind.Local).AddTicks(1120), false, null, "YOJA@GMAIL.COM", "YOJA", "AQAAAAEAACcQAAAAEFC+h4dG9Ae2+1n0mOzz3qsXMfbr4pRpNO6E9w3FLRgv21njlntbB6XF1mgO/7N7sg==", null, false, "DC6E275DD1E2495347A7781D42BB68293B", false, "yoja" }
                });

            migrationBuilder.InsertData(
                table: "Breweries",
                columns: new[] { "BreweryId", "BreweryName", "CountryId", "Description", "Image", "IsDeleted" },
                values: new object[,]
                {
                    { 20, "The Mistress", 18, null, null, false },
                    { 21, "Space Age", 19, null, null, false },
                    { 22, "Alpha Beta", 20, null, null, false },
                    { 23, "Heineken", 21, null, null, false },
                    { 24, "Cold Break", 22, null, null, false },
                    { 25, "Turbid Days", 23, null, null, false },
                    { 28, "Alpha Waves", 24, null, null, false },
                    { 29, "Nitrogen Brew Co.", 25, null, null, false },
                    { 19, "Candle Wick", 17, null, null, false },
                    { 27, "Brier Brewing", 26, null, null, false },
                    { 30, "Disaster Brewing", 26, null, null, false },
                    { 31, "Apothecary Brewing", 27, null, null, false },
                    { 32, "Gone Rogue", 28, null, null, false },
                    { 33, "Narwhal Brewing Co", 29, null, null, false },
                    { 34, "Spyglass", 30, null, null, false },
                    { 26, "Slow Coast Brewing", 25, null, null, false },
                    { 18, "El Diablo Brewing", 16, null, null, false },
                    { 17, "Apprentice Brewing", 15, null, null, false },
                    { 16, "Barley and Basil Brewery", 14, null, null, false },
                    { 1, "Sofiisko Pivo", 1, null, null, false },
                    { 5, "Ritual Brew Co.", 1, null, null, false },
                    { 2, "Carlsberg Group", 2, null, null, false },
                    { 3, "Heineken", 3, null, null, false },
                    { 6, "Rebel Days Brew Co.", 3, null, null, false },
                    { 4, "Stella Artois", 4, null, null, false },
                    { 7, "Sanctity", 5, null, null, false },
                    { 8, "White Rabbit", 6, null, null, false },
                    { 9, "Lone Rider", 7, null, null, false },
                    { 10, "9 Lives", 8, null, null, false },
                    { 11, "Acid Rest", 9, null, null, false },
                    { 12, "Chill Haze", 10, null, null, false },
                    { 13, "Mouthfeel", 11, null, null, false },
                    { 14, "Yeast Cake", 12, null, null, false },
                    { 15, "Dank Brew House", 13, null, null, false },
                    { 35, "Bomber Bottles", 31, null, null, false },
                    { 36, "Final Gravity", 32, null, null, false }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[,]
                {
                    { 999, 1 },
                    { 1, 2 },
                    { 2, 2 }
                });

            migrationBuilder.InsertData(
                table: "Beers",
                columns: new[] { "BeerId", "Abv", "BeerName", "BreweryId", "CountryId", "Image", "IsDeleted", "RatedTimes", "Rating", "StyleId", "UserId" },
                values: new object[,]
                {
                    { 21, 5.5, "SR-71", 7, 21, "https://www.tgbrews.com/wp-content/uploads/2017/04/0.SR71.REFORMAT.1-480x720.jpg", false, 1, 5.0, 5, null },
                    { 25, 5.5, "Mornin' Delight", 4, 25, "https://i.pinimg.com/originals/5a/47/4d/5a474dfa0f673a1ff237a3c30d3872d1.jpg", false, 1, 5.0, 1, null },
                    { 24, 5.2999999999999998, "Pliny The Younger", 4, 24, "https://s.hdnux.com/photos/01/07/56/07/18801124/5/920x920.jpg", false, 1, 5.0, 8, null },
                    { 16, 5.2999999999999998, "Assassin", 4, 16, "https://res.cloudinary.com/ratebeer/image/upload/w_250,c_limit/beer_547307.jpg", false, 1, 5.0, 8, null },
                    { 12, 5.2999999999999998, "Parabola", 4, 12, "https://healthyspiritssf.com/wp-content/uploads/2015/04/parabola-img.png", false, 1, 5.0, 4, null },
                    { 8, 5.2999999999999998, "Zombie Dust", 4, 8, "https://products2.imgix.drizly.com/ci-three-floyds-zombie-dust-4b8ac4a627956811.png?auto=format%2Ccompress&fm=jpg", false, 1, 5.0, 8, null },
                    { 4, 5.2999999999999998, "Stella Artois", 4, 4, "https://tobood.bg/assets/products/large/pd_ab8117195266147249c980cd04934420_9a6ad59857e04bb2f0ada20d164be708.jpg", false, 1, 5.0, 4, null },
                    { 22, 4.0, "Very Hazy", 6, 22, "https://i.redd.it/vcw2vl1j2vj11.jpg", false, 1, 5.0, 6, null },
                    { 26, 4.0, "Heady Topper", 3, 26, "https://i1.wp.com/www.delmesaliquor.com/wp-content/uploads/2018/12/The-Alchemist-Heady-Topper-American-DIPA.jpg", false, 1, 5.0, 2, null },
                    { 19, 5.2999999999999998, "CBS (Canadian Breakfast Stout)", 3, 19, "https://cdn.shopify.com/s/files/1/1351/8119/products/Founders-CBS-2017-355ml_IMG_6958_1024x1024.jpg", false, 1, 5.0, 3, null },
                    { 15, 5.2999999999999998, "Very Green", 3, 15, "https://www.mybeercollectibles.com/uploads/cache/DxC3WBmXQAApXyj-500x500-crop.jpg", false, 1, 5.0, 7, null },
                    { 11, 5.2999999999999998, "Duck Duck Gooze", 3, 11, "https://48tk9j3a74jb133e1k2fzz2s-wpengine.netdna-ssl.com/wp-content/uploads/2014/10/The-Lost-Abbey-Duck-Duck-Gooze-2013-Bottle.jpg", false, 1, 5.0, 3, null },
                    { 7, 5.2999999999999998, "Bourbon County Brand Vanilla Stout", 3, 7, "https://cdn.shopify.com/s/files/1/0227/0581/products/Goose-Island-Bourbon-County-Brand-Stout-2019-500ML-BTL_grande.jpg", false, 1, 5.0, 7, null },
                    { 3, 5.2999999999999998, "Heineken", 3, 3, "https://cdn.pixabay.com/photo/2020/04/18/04/24/beer-5057609_960_720.jpg", false, 1, 5.0, 3, null },
                    { 27, 5.2999999999999998, "Barrel-Aged Abraxas", 2, 27, "https://i.pinimg.com/originals/33/f9/03/33f903416491175c99f110b9c8b25cfc.jpg", false, 1, 5.0, 3, null },
                    { 14, 4.0, "Julius", 2, 14, "https://cdn.webshopapp.com/shops/19852/files/302874105/brouwerij-hoegaarden-julius-blond.jpg", false, 1, 5.0, 6, null },
                    { 10, 4.0, "Morning Wood", 2, 10, "https://48tk9j3a74jb133e1k2fzz2s-wpengine.netdna-ssl.com/wp-content/uploads/2015/11/Funky-Buddha-Bourbon-Barrel-Aged-Morning-Wood-1.jpg", false, 1, 5.0, 2, null },
                    { 6, 4.0, "Marshmallow Handjee", 2, 6, "https://i.imgur.com/zq9u2bN.jpg", false, 1, 5.0, 6, null },
                    { 2, 4.0, "Carlsberg", 2, 2, "https://www.carlsberg.com/media/2399/cb_nd_stemmed_glass_with_33cl_pilsner_bottle_wet_combo_rgb_72dpi.jpg", false, 1, 5.0, 2, null },
                    { 23, 5.2999999999999998, "King Julius", 5, 23, "https://www.thebeersbeer.com/wp-content/uploads/2019/06/beer-king-julius-style-ipa.jpg", false, 1, 5.0, 7, null },
                    { 28, 5.2999999999999998, "Hunahpu's Imperial Stout - Double Barrel Aged", 1, 28, "https://48tk9j3a74jb133e1k2fzz2s-wpengine.netdna-ssl.com/wp-content/uploads/2015/01/Cigar-City-Hunahpu%E2%80%99s-Imperial-Stout.jpg", false, 1, 5.0, 4, null },
                    { 17, 5.5, "Fou' Foune", 1, 17, "https://store.belgianshop.com/3543-large_default/cantillon-foufoune-5-34l-v.jpg", false, 1, 5.0, 1, null },
                    { 13, 5.5, "Double Sunshine", 1, 13, "https://thefullpint.com/wp-content/uploads/Lawsons-Finest-Liquids-Double-Sunshine-IPA-scaled.jpg", false, 1, 5.0, 5, null },
                    { 9, 5.5, "Lou Pepe - Kriek", 1, 9, "https://cdn.webshopapp.com/shops/65337/files/56029926/800x600x2/wb-fles-cantillon-lou-pepe-pure-kriek.jpg", false, 1, 5.0, 1, null },
                    { 5, 5.5, "Kentucky Brunch Brand Stout", 1, 5, "https://coolmaterial.com/wp-content/uploads/2017/05/Toppling-Goliath-Kentucky-Brunch-Brand-Stout.jpg", false, 1, 5.0, 5, null },
                    { 1, 5.5, "Ariana", 1, 1, "https://p1.akcdn.net/full/570589092.bira-ariana-500ml.jpg", false, 1, 5.0, 1, null },
                    { 20, 5.2999999999999998, "Fundamental Observation", 8, 20, "https://joshbeerme.files.wordpress.com/2015/07/imag0657_1.jpg", false, 1, 5.0, 4, null },
                    { 18, 4.0, "Pliny The Elder", 29, 18, "https://thefullpint.com/wp-content/uploads/Russian-River-Pliny-the-Elder.jpg", false, 1, 5.0, 2, null }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_BanId",
                table: "AspNetUsers",
                column: "BanId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_CountryId",
                table: "AspNetUsers",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Beers_BreweryId",
                table: "Beers",
                column: "BreweryId");

            migrationBuilder.CreateIndex(
                name: "IX_Beers_CountryId",
                table: "Beers",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_Beers_StyleId",
                table: "Beers",
                column: "StyleId");

            migrationBuilder.CreateIndex(
                name: "IX_Beers_UserId",
                table: "Beers",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Breweries_CountryId",
                table: "Breweries",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_DrankLists_BeerId",
                table: "DrankLists",
                column: "BeerId");

            migrationBuilder.CreateIndex(
                name: "IX_DrankLists_UserId",
                table: "DrankLists",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Reviews_BeerId",
                table: "Reviews",
                column: "BeerId");

            migrationBuilder.CreateIndex(
                name: "IX_Reviews_UserId",
                table: "Reviews",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserModifications_UserId",
                table: "UserModifications",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Votes_ReviewId",
                table: "Votes",
                column: "ReviewId");

            migrationBuilder.CreateIndex(
                name: "IX_Votes_UserId",
                table: "Votes",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_WishLists_BeerId",
                table: "WishLists",
                column: "BeerId");

            migrationBuilder.CreateIndex(
                name: "IX_WishLists_UserId",
                table: "WishLists",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "DrankLists");

            migrationBuilder.DropTable(
                name: "UserModifications");

            migrationBuilder.DropTable(
                name: "Votes");

            migrationBuilder.DropTable(
                name: "WishLists");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "Reviews");

            migrationBuilder.DropTable(
                name: "Beers");

            migrationBuilder.DropTable(
                name: "Breweries");

            migrationBuilder.DropTable(
                name: "Styles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Bans");

            migrationBuilder.DropTable(
                name: "Countries");
        }
    }
}
