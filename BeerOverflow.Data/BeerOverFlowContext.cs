﻿using System.Linq;
using BeerOverflow.Data.ConfigurationModels;
using BeerOverflow.Data.ConfigurationModels.ListsConfig;
using BeerOverflow.Data.Entities;
using BeerOverflow.Data.Entities.Lists;
using BeerOverflow.Data.Seeder;
using BeerOverflowBeerOverflow.DataData.ConfigurationModels;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BeerOverFlowData
{
    public class BeerOverFlowContext : IdentityDbContext<User, Role, int>
    {
        public BeerOverFlowContext(DbContextOptions<BeerOverFlowContext> options)
            : base(options)
        {
        }

        // Main Entities
        public DbSet<Beer> Beers { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Brewery> Breweries { get; set; }
        public DbSet<Style> Styles { get; set; }
        public DbSet<Vote> Votes { get; set; }

        // Lists
        public DbSet<Review> Reviews { get; set; }
        public DbSet<Drank> DrankLists { get; set; }
        public DbSet<Wish> WishLists { get; set; }
        public DbSet<UserModification> UserModifications { get; set; }
        public DbSet<Ban> Bans { get; set; }

        protected override void OnModelCreating(ModelBuilder model)
        {
            model.ApplyConfiguration(new BeerConfig());
            model.ApplyConfiguration(new CountryConfig());
            model.ApplyConfiguration(new BreweryConfig());
            model.ApplyConfiguration(new UserConfig());
            model.ApplyConfiguration(new ReviewConfig());
            model.ApplyConfiguration(new WishListConfig());
            model.ApplyConfiguration(new DrankListConfig());
            model.ApplyConfiguration(new VoteConfig());
            model.ApplyConfiguration(new StyleConfig());
            model.ApplyConfiguration(new UserModificationConfig());
            model.RelationshipSetter();
            model.Seeder();

            base.OnModelCreating(model);
        }
    }
}
