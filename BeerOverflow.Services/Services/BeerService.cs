﻿using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverflow.Data.Entities.Lists;
using BeerOverflow.Services.ModelsDTo.WebModels;
using BeerOverFlow.Services.Contracts;
using BeerOverFlow.Services.ModelsDTo;
using BeerOverFlowData;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverFlow.Services.Services
{
    public class BeerService : IBeerService
    {
        private BeerOverFlowContext database;

        public BeerService(BeerOverFlowContext database)
        {
            this.database = database;
        }

        public BeerDetailsModel BeerDetails(int id, IMapper mapper)
        {
            var beers = database.Beers.Where(b => b.BeerId.Equals(id) && b.IsDeleted.Equals(false)).
               Include(b => b.Style).Include(b => b.Brewery).Include(b => b.Country).Include(b => b.Reviews).ThenInclude(r=>r.User)
               .Include(b => b.Reviews).ThenInclude(r => r.Votes);
            Beer beer = beers.FirstOrDefault();
            var result = mapper.Map<BeerDetailsModel>(beer);
            result.Id = beer.BeerId;
            result.Rating = String.Format("{0:0.00}", beer.Rating); ;
            result.BreweryName = beer.Brewery.BreweryName;
            result.StyleName = beer.Style.StyleName;
            result.CountryName = beer.Country.CountryName;
            return result;
        }

        public ICollection<BeerDTO> SortByName(IMapper mapper)
        {
            var beers = database.Beers.Include(p => p.Brewery).ThenInclude(p => p.Country)
                .Include(p => p.Style)
                .Include(p => p.Country)
                .OrderBy(p => p.BeerName).Where(p => p.IsDeleted != true).ToArray();
            var result = mapper.Map<ICollection<BeerDTO>>(beers);
            return result;
        }

        public ICollection<BeerDTO> SortByABV(IMapper mapper)
        {
            var abv = database.Beers.Include(p => p.Brewery).
                ThenInclude(p => p.Country)
                .Include(p => p.Style)
                .Include(p => p.Country)
                .OrderBy(p => p.Abv).Where(p => p.IsDeleted != true).ToArray();
            var result = mapper.Map<ICollection<BeerDTO>>(abv);
            return result;
        }
        public async Task<ICollection<BeerDTO>> SortByRatingAsync(IMapper mapper)
        {
            var abv = await database.Beers.Where(p => p.IsDeleted != true).Include(p => p.Brewery).
                ThenInclude(p => p.Country)
                .Include(p => p.Style)
                .Include(p => p.Country)
                .OrderBy(p => p.Rating).ToArrayAsync();
            var result = mapper.Map<ICollection<BeerDTO>>(abv);
            return result;
        }

        public async Task<BeerDTO> CreateAsync(BeerDTO beerDto, IMapper mapper)
        {
            if (database.Beers.Any(b => b.BeerName.Equals(beerDto.BeerName)))
                return null;

            var beer = new Beer();
            beer.BeerName = beerDto.BeerName;

            try
            {
                if (beerDto.Image!=null)
                {
                    beer.Image = beerDto.Image;
                }
                else
                {
                    beer.Image = "https://birka.bg/wp-content/uploads/2018/11/da.png";
                }
              
                beer.Country = await database.Countries.FirstOrDefaultAsync(p => p.CountryName.Equals(beerDto.Country.CountryName)
           || p.CountryId.Equals(int.Parse(beerDto.Country.CountryName)));
                beer.Style = await database.Styles.FirstOrDefaultAsync(p => p.StyleId.ToString().Equals(beerDto.Style.StyleName));
                beer.Abv = beerDto.Abv;
                beer.Brewery = await database.Breweries.Include(p => p.Beers).Include(p => p.Country)
                    .FirstOrDefaultAsync(p => p.BreweryName.Equals(beerDto.Brewery.BreweryName)||p.BreweryId.Equals(int.Parse(beerDto.Brewery.BreweryName)));
            }
            catch (Exception)
            {

                return null;
            }          

            if (beer.Country == null || beer.Brewery == null || beer.Style == null)
                return null;

            _ = await this.database.Beers.AddAsync(beer);
            _ = await database.SaveChangesAsync();

            var resultDTO = mapper.Map<BeerDTO>(beer);
            //resultDTO.Brewery.BeerDTOs = mapper.Map<ICollection<BeerDTO>>(beer.Brewery.Beers);
            return resultDTO;
        }

        public BeerDTO Update(int id, BeerDTO beerDto, IMapper mapper)
        {
            var entityToUpdate = database.Beers.Include(p => p.Style).
                Include(p => p.Brewery).Include(p => p.Country).Where(p => p.IsDeleted != true).
                FirstOrDefault(b => b.BeerId.Equals(id));

            if (entityToUpdate != null)
            {
                entityToUpdate.Abv = beerDto.Abv;

               
                entityToUpdate.Country = database.Countries.
                    FirstOrDefault(c =>c.CountryName.Equals(beerDto.Country.CountryName) || c.CountryId.ToString().
                    Equals(beerDto.Country.CountryName));


                entityToUpdate.Brewery = database.Breweries.
                    Include(p => p.Country).
                    FirstOrDefault(b => b.BreweryName.Equals(beerDto.Brewery.BreweryName) ||b.CountryId.ToString().
                    Equals(beerDto.Brewery.BreweryName));

                entityToUpdate.BeerName = beerDto.BeerName;

                entityToUpdate.Style = database.Styles.
                    FirstOrDefault(p => p.StyleId.ToString().
                    Equals(beerDto.Style.StyleName)||
                    p.StyleName.Equals(beerDto.Style.StyleName));

                if (entityToUpdate.Brewery != null
                    && entityToUpdate.Country != null
                    && entityToUpdate.BeerName != null
                    && entityToUpdate.Style != null)
                {
                    database.SaveChanges();
                    return mapper.Map<BeerDTO>(entityToUpdate);
                }
            }
            return null;
        }
        public async Task<BeerDTO> RateBeer(int beerId, int rating, IMapper mapper)
        {
            var result = database.Beers.FirstOrDefault(p => p.BeerId.Equals(beerId));
            if (result != null)
            {
                int timesRated = await Task.Run(() => result.RatedTimes + 1);
                double currentRating = result.Rating;
                double avgRating = (currentRating + rating) / timesRated;
                if(currentRating == 0)
                    result.Rating = rating;
                else
                    result.Rating = avgRating;
                await database.SaveChangesAsync();
                return mapper.Map<BeerDTO>(result);
            }
            return null;
        }
        public BeerDTO RateIdenticalBeer(int userId, int beerId, int rating, IMapper mapper)
        {
            var beer = database.Beers.First(b => b.BeerId == beerId);
            var user = database.Users.First(u => u.Id == userId);
            if (!user.RatedBeers.Any(b=>b.BeerId == beerId))
            {
                if (beer.Sum.Equals(0))
                {
                    beer.Sum = (int)beer.Rating;
                }
                user.RatedBeers.Add(beer);
                beer.RatedTimes = beer.RatedTimes + 1;
                beer.Sum = beer.Sum + rating;
                beer.Rating = (double)(beer.Sum)/ beer.RatedTimes;   
                database.SaveChanges();
                return mapper.Map<BeerDTO>(beer);
            }
            return null;
        }
        public bool Delete(int id)
        {
            var subUndrDel = database.Beers.FirstOrDefault(p => p.BeerId.Equals(id)&&p.IsDeleted!=true);
            if (subUndrDel!=null)
            {            
                subUndrDel.IsDeleted = true;
                database.SaveChanges();
                return true;
            }
            return false;
        }
        public bool DeleteAll()
        {
            var subUndrDel = database.Beers.Where(p=>p.IsDeleted != true);
            if (subUndrDel != null)
            {
                foreach(var beer in subUndrDel)
                {
                    beer.IsDeleted = true;
                }
                return true;
            }
            return false;
        }

        public ICollection<BeerDTO> GetAll(IMapper mapper)
        {
            var res = database.Beers.Where(p => p.IsDeleted == false).
                Include(p=>p.Style).Include(p=>p.Brewery).Include(p=>p.Country).ToList();

            var final = mapper.Map<ICollection<BeerDTO>>(res);
            return final;
            
        }

        public ICollection<BeerDTO> FilterByName(string name, IMapper mapper)
        {
            var result = database.Beers.
                Where(p => p.BeerName.Contains(name)==true && p.IsDeleted == false)
                .Include(p => p.Style).Include(p => p.Brewery).Include(p => p.Country).ToList();

            var final = mapper.Map<ICollection<BeerDTO>>(result);

            return final;
        }

        public ICollection<BeerDTO> FilterByCountry(string country, IMapper mapper)
        {
            var result = database.Beers.
                Where(p => p.Country.CountryName.Equals(country)&&p.IsDeleted==false)
                .Include(p => p.Style).Include(p => p.Brewery).Include(p => p.Country).ToList();

            var final = mapper.Map<ICollection<BeerDTO>>(result);

            return final;
        }

        public ICollection<BeerDTO> FilterByStyle(string style, IMapper mapper)
        {

            var result = database.Beers.
                  Where(p => p.Style.StyleName.Contains(style) == true && p.IsDeleted == false).
                  Include(p => p.Style).Include(p => p.Brewery).Include(p => p.Country).ToList();

            var final = mapper.Map<ICollection<BeerDTO>>(result);

            return final;
        }

        public ICollection<BeerDTO> FilterByStyleAndCountry(string style, string country, IMapper mapper)
        {
            var result = database.Beers.
                    Where(p => p.Style.StyleName.Contains(style) == true && 
                    p.Country.CountryName.Contains(country) == true && p.IsDeleted == false).
                    Include(p => p.Style).Include(p => p.Brewery).Include(p => p.Country).ToList(); 

            var final = mapper.Map<ICollection<BeerDTO>>(result);

            return final;
        }

        public BeerDTO GetById(int id, IMapper mapper)
        {
            var res = database.Beers.Where(p => p.BeerId.Equals(id) && p.IsDeleted.Equals(false)).
               Include(p => p.Style).Include(p => p.Brewery).Include(p => p.Country);
            var final = mapper.Map<BeerDTO>(res.FirstOrDefault());
            return final;
        }
    }
}
