﻿using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverFlow.Services.Contracts;
using BeerOverFlow.Services.ModelsDTo;
using BeerOverFlowData;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace BeerOverFlow.Services.Services
{
    public class BreweryService : IBreweryService
    {
        private BeerOverFlowContext database;
        public BreweryService(BeerOverFlowContext database)
        {
            this.database = database;
        }
        public BreweryDTO Create(BreweryDTO breweryDto, IMapper mapper)
        {
            if (database.Breweries.Any(p => p.BreweryName == breweryDto.BreweryName))
            {
                return null;
            }
            var brewery = new Brewery();
            brewery.BreweryName = breweryDto.BreweryName;
            brewery.Country = database.Countries.Include(p => p.Breweries).
                FirstOrDefault(p => p.CountryId.Equals(int.Parse(breweryDto.Country.CountryName)));
            brewery.Description = breweryDto.Description;
            if (brewery.Country == null)
            {
                return null;
            }
            try
            {
                this.database.Breweries.Add(brewery);
                database.SaveChanges();
                var result = mapper.Map<BreweryDTO>(brewery);
                return result;
            }
            catch (System.Exception)
            {
                return null;
            }

        }

        public BreweryDTO Update(BreweryDTO breweryDto, IMapper mapper)
        {
            var brewery = database.Breweries.FirstOrDefault(p => p.BreweryId.Equals(breweryDto.BreweryId));

            if (brewery == null ||database.Breweries.Any(p=>p.BreweryName.Equals(breweryDto.BreweryName)))
            {
                return null;
            }
            brewery.BreweryName = breweryDto.BreweryName;
            brewery.Country = database.Countries.
                FirstOrDefault(p => (p.CountryId).ToString().Equals(breweryDto.Country.CountryName)||
                p.CountryName.Equals(breweryDto.Country.CountryName));
            try
            {
                if (brewery.Country==null)
                {
                    return null;
                }
                database.SaveChanges();
                return mapper.Map<BreweryDTO>(brewery);
            }
            catch (System.Exception)
            {

                return null;
            }
        }

        public ICollection<BreweryDTO> GetBreweries(IMapper mapper)
        {

            var brewery = database.Breweries.Include(p => p.Country).Where(p=>p.IsDeleted==false).ToList();
            var count = brewery.Count();
            if (brewery == null)
            {
                return null;
            }
            var res = mapper.Map<ICollection<BreweryDTO>>(brewery);
            return res;
        }

        public BreweryDTO GetBrewery(int n, IMapper mapper)
        {
            var brewery = database.Breweries.Include(p => p.Beers).Include(p => p.Country).
                FirstOrDefault(p => p.BreweryId.Equals(n));
            if (brewery == null)
            {
                return null;
            }
            var result = mapper.Map<BreweryDTO>(brewery);
            return result;
        }

        public bool Delete(int id)
        {
            var res = database.Breweries.FirstOrDefault(p => p.BreweryId.Equals(id));
            if (res == null||res.IsDeleted.Equals(true))
            {
                return false;
            }
            res.IsDeleted = true;
            database.SaveChanges();
            return true;
        }

        public ICollection<BeerDTO> GetBeers(int id, IMapper mapper)
        {
            var brewery = database.Breweries.Include(p=>p.Beers).ThenInclude(b=>b.Style).Include(b=>b.Country).FirstOrDefault(p => p.BreweryId == id);
            IList<BeerDTO> beers = new List<BeerDTO>();
            foreach (var beer in brewery.Beers)
            {
                if (beer.IsDeleted != true)
                {
                    beers.Add(mapper.Map<BeerDTO>(beer));
                }
            }
            return beers;
        }

        
    }
}
