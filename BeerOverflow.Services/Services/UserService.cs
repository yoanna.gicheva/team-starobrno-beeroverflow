﻿using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverflow.Data.Entities.Lists;
using BeerOverflow.Services.ModelsDTo.WebModels;
using BeerOverFlow.Services.Contracts;
using BeerOverFlow.Services.ModelsDTo;
using BeerOverFlow.Services.ModelsDTO;
using BeerOverFlowData;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverFlow.Services.Services
{
    public class UserService : IUserService
    {
        private BeerOverFlowContext context;
        public UserService(BeerOverFlowContext data)
        {
            context = data;
        }
        public async Task<ICollection<UserReturnDTO>> GetAllUsersAsync(IMapper mapper)
        {
            var res = await context.Users.Include(p => p.Country).Include(u => u.Ban).Where(p=>p.IsDeleted.Equals(false)).ToListAsync();
            var final = mapper.Map<ICollection<UserReturnDTO>>(res);
            return final;
        }

        public UserReturnDTO GetUserById(int id, IMapper mapper)
        {
            User toPass = context.Users.Include(p => p.Country).FirstOrDefault(p => p.Id.Equals(id));
            return mapper.Map<UserReturnDTO>(toPass);
        }
        public bool CreateUser(UserViewDTO fromBody, IMapper mapper)
        {
            if (context.Users.Any(prop => prop.UserName == fromBody.UserName) || context.Users.Any(prop => prop.Email == fromBody.Email))
                return false;
            User user = mapper.Map<User>(fromBody);
            UserModification modification = new UserModification(user.CreatedOn, $"#{user.UserName} was created!");
            user.UserModifications.Add(modification);
            context.Users.Add(user);
            context.SaveChanges();
            return true;
        }
        public UserReturnDTO UpdateUser(int id, UserViewDTO fromBody, IMapper mapper)
        {
            var toupdate = context.Users.FirstOrDefault(p => p.Id.Equals(id));
            if (toupdate == null || context.Users.Where(p => p.Id != id).Where(p => p.Email == fromBody.Email).Count() > 0)
                return null;
            toupdate.Email = fromBody.Email;
            toupdate.UserName = fromBody.UserName;
            (toupdate as User).Description = fromBody.Description;
            context.SaveChanges();
            var result = context.Users.First(p => p.Id.Equals(id));
            return mapper.Map<UserReturnDTO>(result);
        }
        // ASYNC
        public bool DeleteAll()
        {
            var rows = from o in context.Users
                       select o;
            if (rows.Count() == 0)
                return false;
            foreach (var row in rows)
            {
                //DeleteModifications(row.Id);
                context.Users.Remove(row);
            }
            context.SaveChanges();
            return true;
        }
        // ASYNC
        public bool DeleteById(int id)
        {
            var toDelete = context.Users.FirstOrDefault(p => p.Id.Equals(id));
            if (toDelete == null)
                return false;
            DeleteModifications(id);
            context.Users.Remove(toDelete);
            context.SaveChanges();
            return true;
        }
        public bool AddToWishlist(int beerId, int userId)
        {
            if (!context.Users.Any(prop => prop.Id.Equals(userId)))
                return false;
            User user = context.Users.Include(u => u.WishList).FirstOrDefault(p => p.Id.Equals(userId));
            if (user.WishList.Any(w => w.BeerId == beerId) || !context.Beers.Any(b => b.BeerId.Equals(beerId)))
                return false;
            Wish wish = new Wish(beerId, userId);
            wish.Beer = context.Beers.FirstOrDefault(p => p.BeerId.Equals(beerId));
            user.WishList.Add(wish);
            context.SaveChanges();
            return true;
        }
        // ASYNC
        public bool RemoveFromWishlist(int beerId, int userId)
        {
            if (!context.Users.Any(prop => prop.Id.Equals(userId)))
                return false;
            var user = context.Users.Include(u => u.WishList).FirstOrDefault(p => p.Id.Equals(userId));
            if (!user.WishList.Any(w => w.BeerId == beerId) || !context.Beers.Any(b => b.BeerId.Equals(beerId)))
                return false;
            Wish wish = user.WishList.First(b => b.BeerId == beerId);
            user.WishList.Remove(wish);
            context.WishLists.Remove(wish);
            context.SaveChanges();
            return true;
        }
        public bool AddToDranklist(int beerId, int userId)
        {
            if (!context.Users.Any(prop => prop.Id.Equals(userId)))
                return false;
            User user = context.Users.Include(u => u.DrankList).FirstOrDefault(p => p.Id.Equals(userId));
            if (user.DrankList.Any(w => w.BeerId == beerId) || !context.Beers.Any(b => b.BeerId.Equals(beerId)))
                return false;
            Drank drank = new Drank(beerId, userId);
            user.DrankList.Add(drank);
            context.SaveChanges();
            return true;
        }
        public bool RemoveFromDranklist(int beerId, int userId)
        {
            if (!context.Users.Any(prop => prop.Id.Equals(userId)))
                return false;
            var user = context.Users.Include(u => u.DrankList).FirstOrDefault(p => p.Id.Equals(userId));
            if (!user.DrankList.Any(w => w.BeerId == beerId) || !context.Beers.Any(b => b.BeerId.Equals(beerId)))
                return false;
            Drank wish = user.DrankList.First(b => b.BeerId == beerId);
            user.DrankList.Remove(wish);
            context.DrankLists.Remove(wish);
            context.SaveChanges();
            return true;
        }

        public bool BanUser(int id)
        {
            if (!context.Users.Any(u => u.Id == id))
                return false;
            User user = context.Users.Include(u => u.Ban).FirstOrDefault(u => u.Id == id);
            if (user.Ban == null)
            {
                var ban = new Ban("Default description", true);
                context.Bans.Add(ban);
                context.SaveChanges();
                int banId = context.Bans.Max(p => p.BanId);
                user.Ban = ban;
                user.BanId = banId;
                context.SaveChanges();
            }
            else
                user.Ban.Banned = true;
            context.SaveChanges();
            return true;
        }
        public UserDetailsModel UserDetails(string id, IMapper mapper)
        {
            User user = context.Users.Include(u => u.Country).Include(u => u.WishList)
                .Include(u => u.DrankList).Include(u => u.Reviews).FirstOrDefault(u => u.UserName.Equals(id));
            if (user == null)
                return null;
            UserDetailsModel result = mapper.Map<UserDetailsModel>(user);
            if (user.Country == null)
                result.CountryName = "Unidentified";
            else
                result.CountryName = user.Country.CountryName;
            return result;
        }
        public ICollection<BeerDTO> UserWishlist(string name, IMapper mapper)
        {
            User user = context.Users.Include(u => u.WishList)
                .Include(u => u.WishList).ThenInclude(w => w.Beer)
                .Include(u => u.WishList).ThenInclude(w => w.Beer).ThenInclude(p => p.Style)
                .Include(u => u.WishList).ThenInclude(w => w.Beer).ThenInclude(p => p.Brewery)
                .Include(u => u.WishList).ThenInclude(w => w.Beer).ThenInclude(p => p.Country)
                .FirstOrDefault(u => u.UserName.Equals(name));
            var wishlist = new List<BeerDTO>();
            if (user.WishList == null && user.WishList.Count == 0)
                return null;
            foreach (var wish in user.WishList)
            {
                var beer = wish.Beer;
                if (!beer.IsDeleted)
                {
                    wishlist.Add(mapper.Map<BeerDTO>(beer));
                }
            }
            return wishlist;
        }
        public ICollection<BeerDTO> UserDranklist(string name, IMapper mapper)
        {
            User user = context.Users.Include(u => u.DrankList).ThenInclude(w => w.Beer).ThenInclude(p => p.Style)
                .Include(u => u.DrankList).ThenInclude(w => w.Beer).ThenInclude(p => p.Brewery)
                .Include(u => u.DrankList).ThenInclude(w => w.Beer).ThenInclude(p => p.Country)
                .FirstOrDefault(u => u.UserName.Equals(name));
            var dranklist = new List<BeerDTO>();
            if (user.DrankList == null && user.DrankList.Count == 0)
                return null;
            foreach (var wish in user.DrankList)
            {
                var beer = wish.Beer;
                if (!beer.IsDeleted)
                {
                    dranklist.Add(mapper.Map<BeerDTO>(beer));
                }
            }
            return dranklist;
        }
        public ICollection<ReviewDetailModel> UserReview(string name)
        {
            User user = context.Users.Include(u => u.Reviews).ThenInclude(w => w.Beer)
                .FirstOrDefault(u => u.UserName.Equals(name));
            if (user == null || user.Reviews == null)
                return null;
            var reviewList = new List<ReviewDetailModel>();
            foreach (var review in user.Reviews)
            {
                var beer = review.Beer;
                if (!beer.IsDeleted && !review.IsDeleted)
                {
                    ReviewDetailModel reviewToAdd = new ReviewDetailModel(review.User.UserName, review.Text, review.Beer.BeerName,
                        review.CreatedOn, review.Flagged, review.ReviewId);
                    reviewList.Add(reviewToAdd);
                }
            }
            return reviewList;
        }
        public int UserId(string name)
        {
            User user = context.Users.FirstOrDefault(u => u.UserName.Equals(name));
            if (user == null)
                return 0;
            return user.Id;
        }
        public bool UserIsBanned(int id)
        {
            User user = context.Users.Include(u => u.Ban).FirstOrDefault(u => u.Id.Equals(id));
            if (user.Ban == null || !user.Ban.Banned)
                return false;
            return true;
        }
        // ASYNC
        private void DeleteModifications(int id)
        {
            foreach (var entity in context.UserModifications.Where(o => o.UserId == id))
                context.UserModifications.Remove(entity);
            context.SaveChanges();
        }

        public bool Delete(int id)
        {
            var user = context.Users.FirstOrDefault(p => p.IsDeleted.Equals(false) && p.Id.Equals(id));
            if (user != null)
            {
                user.IsDeleted = true;
                context.SaveChanges();
                return true;
            }
            return false;
        }
        public bool UserIsBanned(string userName)
        {
            User user = context.Users.Include(u => u.Ban).FirstOrDefault(u => u.UserName.Equals(userName));
            if (user.Ban == null || !user.Ban.Banned)
            {
                return false;
            }
            return true;
        }
        public string BannDescription(string userName)
        {
            User user = context.Users.Include(u => u.Ban).FirstOrDefault(u => u.UserName.Equals(userName));
            if (user.Ban == null)
            {
                return "No ban detected";
            }
            return user.Ban.Description + user.Ban.Description;
        }
    }
}
