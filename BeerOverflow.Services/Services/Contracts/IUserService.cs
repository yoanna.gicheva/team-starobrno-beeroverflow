﻿using AutoMapper;
using BeerOverflow.Services.ModelsDTo.WebModels;
using BeerOverFlow.Services.ModelsDTo;
using BeerOverFlow.Services.ModelsDTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeerOverFlow.Services.Contracts
{
    public interface IUserService
    {
        bool CreateUser(UserViewDTO user, IMapper mapper);
        Task<ICollection<UserReturnDTO>> GetAllUsersAsync(IMapper mapper);
        UserReturnDTO GetUserById(int id, IMapper mapper);
        bool AddToDranklist(int beerId, int userId);
        bool RemoveFromDranklist(int beerId, int userId);
        UserReturnDTO UpdateUser(int id, UserViewDTO fromBody, IMapper mapper);
        bool DeleteAll();
        bool Delete(int id);
        bool DeleteById(int id);
        bool RemoveFromWishlist(int beerId, int userId);
        bool AddToWishlist(int beerId, int userId);
        bool BanUser(int id);
        UserDetailsModel UserDetails(string id, IMapper mapper);
        ICollection<BeerDTO> UserWishlist(string name, IMapper mapper);
        ICollection<BeerDTO> UserDranklist(string name, IMapper mapper);
        ICollection<ReviewDetailModel> UserReview(string name);
        int UserId(string name);
        bool UserIsBanned(int id);
        bool UserIsBanned(string userName);
        string BannDescription(string userName);

    }
}
