﻿using AutoMapper;
using BeerOverFlow.Services.ModelsDTo;
using System.Collections.Generic;

namespace BeerOverFlow.Services.Contracts
{
    public interface IStyleService
    {
        public ICollection<StyleDTO> GetAllStyles(IMapper mapper);
        public StyleDTO GetStyleById(int id, IMapper mapper);
        public StyleDTO CreateStyle(StyleDTO beerStyleDTO, IMapper mapper);
        public StyleDTO UpdateStyle(int id, StyleDTO newStyle, IMapper mapper);
        public bool DeleteAll();
        public bool DeleteById(int id);
    }
}
