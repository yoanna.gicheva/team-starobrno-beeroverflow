﻿using BeerOverflow.Services.ModelsDTo.WebModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Services.Services.Contracts
{
    public interface IReviewService
    {
        void PostReview(string reviewText, int id, int userId);
        bool FlagReview(int reviewId);
        void DeleteReview(int reviewId);
        void LikeReview(int id, int userId);
        void DislikeReview(int id, int userId);
    }
}
