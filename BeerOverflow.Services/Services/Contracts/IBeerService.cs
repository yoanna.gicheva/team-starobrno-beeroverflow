﻿using AutoMapper;
using BeerOverflow.Services.ModelsDTo.WebModels;
using BeerOverFlow.Services.ModelsDTo;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeerOverFlow.Services.Contracts
{
    public interface IBeerService
    {
        BeerDetailsModel BeerDetails(int id, IMapper mapper);
        Task<BeerDTO> CreateAsync(BeerDTO beerDto, IMapper mapper);
        BeerDTO Update(int id, BeerDTO beerDto, IMapper mapper);
        ICollection<BeerDTO> SortByName(IMapper mapper);
        ICollection<BeerDTO> SortByABV(IMapper mapper);
        Task<ICollection<BeerDTO>> SortByRatingAsync(IMapper mapper);
        Task<BeerDTO> RateBeer(int index, int rating, IMapper mapper);
        BeerDTO RateIdenticalBeer(int userId, int beerId, int rating, IMapper mapper);
        ICollection<BeerDTO> GetAll(IMapper mapper);
        bool DeleteAll();
        BeerDTO GetById(int id, IMapper mapper);
        ICollection<BeerDTO> FilterByName(string name, IMapper mapper);
        ICollection<BeerDTO> FilterByCountry(string country,IMapper mapper);
        ICollection<BeerDTO> FilterByStyle(string style,IMapper mapper);
        ICollection<BeerDTO> FilterByStyleAndCountry(string country, string style,IMapper mapper);
        bool Delete(int id);
    }
}
