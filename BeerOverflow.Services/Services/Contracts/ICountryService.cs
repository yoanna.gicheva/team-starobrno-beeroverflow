﻿using AutoMapper;
using BeerOverFlow.Services.ModelsDTo;
using System.Collections.Generic;

namespace BeerOverFlow.Services.Contracts
{
    public interface ICountryService
    {
        CountryDTO Create(CountryDTO countryDto, IMapper mapper);
        bool Delete(int id);       
        CountryDTO Update(string name,CountryDTO country,IMapper mapper);
        ICollection<CountryDTO> GetCountry(IMapper mapper, int n = -1);

    }
}
