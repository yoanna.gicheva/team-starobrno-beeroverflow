﻿using AutoMapper;
using BeerOverFlow.Services.ModelsDTo;
using System.Collections.Generic;

namespace BeerOverFlow.Services.Contracts
{
    public interface IBreweryService
    {
        BreweryDTO Create(BreweryDTO breweryDto,IMapper mapper);
        BreweryDTO Update(BreweryDTO breweryDto,IMapper mapper);
        ICollection<BreweryDTO> GetBreweries(IMapper mapper);
        BreweryDTO GetBrewery(int n, IMapper mapper);
        bool Delete(int id);
        public ICollection<BeerDTO> GetBeers(int id, IMapper mapper);
    }
}
