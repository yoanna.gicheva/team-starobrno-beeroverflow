﻿using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverFlow.Services.Contracts;
using BeerOverFlow.Services.ModelsDTo;
using BeerOverFlowData;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BeerOverFlow.Services.Services
{
    public class StyleService : IStyleService
    {
        private BeerOverFlowContext context;
        public StyleService(BeerOverFlowContext data)
        {
            context = data;
        }
        // ASYNC
        public ICollection<StyleDTO> GetAllStyles(IMapper mapper)
        {
            List<StyleDTO> styles = new List<StyleDTO>();
            foreach (var style in context.Styles)
            {
                styles.Add(mapper.Map<StyleDTO>(style));
            }
            return styles;
        }
        public StyleDTO GetStyleById(int id, IMapper mapper)
        {
            return mapper.Map<StyleDTO>(context.Styles.FirstOrDefault(p => p.StyleId.Equals(id)));
        }
        public StyleDTO CreateStyle(StyleDTO fromBody, IMapper mapper)
        {
            if (context.Styles.Any(prop => prop.StyleName == fromBody.StyleName))
                return null;

            var newStyle = new Style();
            newStyle.StyleName = fromBody.StyleName;
            context.Styles.Add(newStyle);
            context.SaveChanges();
            var result = context.Styles.First(p => p.StyleName.Equals(fromBody.StyleName));
            return mapper.Map<StyleDTO>(result);
        }
        public StyleDTO UpdateStyle(int id, StyleDTO newStyle, IMapper mapper)
        {
            var toUpdate = context.Styles.FirstOrDefault(p => p.StyleId.Equals(id));
            if (toUpdate == null)
                return null;
            toUpdate.StyleName = newStyle.StyleName;
            context.SaveChanges();
            var result = context.Styles.First(p => p.StyleName.Equals(newStyle.StyleName));
            return mapper.Map<StyleDTO>(result);
        }
        // ASYNC
        public bool DeleteAll()
        {
            var rows = from o in context.Styles
                       select o;
            if (rows.Count() == 0)
                return false;
            foreach (var row in rows)
            {
                context.Styles.Remove(row);
            }
            context.SaveChanges();
            return true;
        }
        public bool DeleteById(int id)
        {
            var toDelete = context.Styles.FirstOrDefault(p => p.StyleId.Equals(id));
            if (toDelete == null)
                return false;
            context.Styles.Remove(toDelete);
            context.SaveChanges();
            return true;
        }
    }
}
