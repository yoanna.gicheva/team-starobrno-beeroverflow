﻿using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverFlow.Services.Contracts;
using BeerOverFlow.Services.ModelsDTo;
using BeerOverFlowData;
using System.Collections.Generic;
using System.Linq;

namespace BeerOverFlow.Services.Services
{
    public class CountryService : ICountryService
    {
        private BeerOverFlowContext database;
    
        public CountryService(BeerOverFlowContext data)
        {
            database = data;
        }
        public CountryDTO Create(CountryDTO countryDto, IMapper mapper)
        {
            var tmp = database.Countries.FirstOrDefault(p => p.CountryName.Equals(countryDto.CountryName));

            if (tmp != null && tmp.IsDeleted.Equals(false))
            {
                return null;
            }
            else if (tmp != null && tmp.IsDeleted.Equals(true))
            {
                tmp.IsDeleted = false;
                var mappedResult01 = mapper.Map<CountryDTO>(tmp);
                database.SaveChanges();
                return mappedResult01;
            }
            else
            {
                var country = new Country();
                country.CountryName = countryDto.CountryName;
                database.Countries.Add(country);
                database.SaveChanges();
                return mapper.Map<Country,CountryDTO>(country);                
            }
        }
        public bool Delete(int id)
        {
            var res = database.Countries.FirstOrDefault(p => p.CountryId.Equals(id)&&p.IsDeleted.Equals(false));
            if (res == null)
            {
                return false;
            }
            res.IsDeleted = true;
            database.SaveChanges();
            return true;
        }

        public ICollection<CountryDTO> GetCountry(IMapper mapper, int n = -1)
        {
            try
            {
                if (n < 0)
                {
                    var rest = database.Countries.Where(p => p.IsDeleted != true).ToList();
                   var res =  mapper.Map<ICollection<CountryDTO>>(rest);
                    return res;
                }
                var result = mapper.Map<ICollection<CountryDTO>>(this.database.Countries).Where(p => p.CountryId.Equals(n)).ToList();
                return result;
            }
            catch (System.Exception)
            {
                return null;
            }            
        }

        public CountryDTO Update(string name,CountryDTO country,IMapper mapper)
        {
            var result = this.database.Countries.FirstOrDefault(p => p.CountryId.Equals(country.CountryId));
           
            if (result != null&&result.IsDeleted!=true)
            {
                result.CountryName = name;
                try
                {
                    database.SaveChanges();
                }
                catch (System.Exception)
                {
                    return null;
                }
                
                return mapper.Map<CountryDTO>(result);
            }
            return null;
        }
        

    }

}
