﻿using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverflow.Data.Entities.Lists;
using BeerOverflow.Services.ModelsDTo.WebModels;
using BeerOverflow.Services.Services.Contracts;
using BeerOverFlowData;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Services.Services
{
    public class ReviewService : IReviewService
    {
        private BeerOverFlowContext context;
        public ReviewService(BeerOverFlowContext context)
        {
            this.context = context;
        }

        public void PostReview(string reviewText, int beerId, int userId)
        {
            var beer = context.Beers.First(b => b.BeerId == beerId);
            var user = context.Users.Include(u=>u.Reviews).First(u => u.Id == userId);
            if (!user.Reviews.Any(b => b.BeerId == beerId))
            {
                Review review = new Review(reviewText, beerId, userId);
                context.Reviews.Add(review);
                context.SaveChanges();
            }
        }
        public bool FlagReview(int reviewId)
        {
            var review = context.Reviews.FirstOrDefault(u => u.ReviewId == reviewId);
            if (review == null)
                return false;
            review.Flagged = true;
            context.SaveChanges();
            return true;
        }
        public void DeleteReview(int reviewId)
        {
            var review = context.Reviews.FirstOrDefault(u => u.ReviewId == reviewId);
            review.IsDeleted = true;
            context.SaveChanges();
        }
        public void LikeReview(int id, int userId)
        {
            Review review = context.Reviews.Include(r => r.Votes).ThenInclude(v=>v.User).FirstOrDefault(r => r.ReviewId == id);
            if(!review.Votes.Any(v=>v.UserId == userId && v.ThumbsUp == true))
            {
                Vote vote = new Vote(true, id, userId);
                context.Votes.Add(vote);
                review.PositiveVotes++;
                context.SaveChanges();
            }
        }
        public void DislikeReview(int id, int userId)
        {
            Review review = context.Reviews.Include(r => r.Votes).ThenInclude(v => v.User).FirstOrDefault(r => r.ReviewId == id);
            if (!review.Votes.Any(v => v.UserId == userId && v.ThumbsUp == false))
            {
                Vote vote = new Vote(false, id, userId);
                context.Votes.Add(vote);
                review.NegativeVotes++;
                context.SaveChanges();
            }
        }
    }
}
