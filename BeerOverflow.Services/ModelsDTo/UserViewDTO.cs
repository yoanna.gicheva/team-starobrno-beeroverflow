﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverFlow.Services.ModelsDTO
{
    public class UserViewDTO
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public int CountryId { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
    }
}
