﻿using BeerOverflow.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverFlow.Services.ModelsDTo
{
    public class UserReturnDTO
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string CountryName { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        public Ban Ban { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime LastAccessedOn { get; set; }
    }
}
