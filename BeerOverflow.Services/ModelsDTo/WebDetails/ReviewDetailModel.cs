﻿using BeerOverflow.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Services.ModelsDTo.WebModels
{
    public class ReviewDetailModel
    {
        public ReviewDetailModel(string username, string text, string beername, DateTime postedOn, bool flagged, int id)
        {
            Username = username;
            Text = text;
            BeerName = beername;
            PostedOn = postedOn;
            Flagged = flagged.ToString();
            Id = id;
        }
        public int Id { get; set; }
        public string Username { get; set; }
        public string Text { get; set; }
        public string BeerName { get; set; }
        public DateTime PostedOn { get; set; }
        public string Flagged { get; set; }
    }
}

