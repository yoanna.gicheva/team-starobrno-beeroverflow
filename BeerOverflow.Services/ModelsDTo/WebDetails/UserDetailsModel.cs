﻿using BeerOverflow.Data.Entities.Lists;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Services.ModelsDTo.WebModels
{
	public class UserDetailsModel
	{
		public string Email { get; set; }
		public string Description { get; set; }
		public DateTime CreatedOn { get; set; }
		public string CountryName { get; set; }
		public ICollection<Review> Reviews { get; set; }
		public ICollection<Wish> WishList { get; set; }
		public ICollection<Drank> DrankList { get; set; }
	}
}
