﻿using BeerOverflow.Data.Entities.Lists;
using BeerOverFlow.Services.ModelsDTo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Services.ModelsDTo.WebModels
{
    public class BeerDetailsModel
    {
        public int Id { get; set; }
        public string BeerName { get; set; }
        public string StyleName { get; set; }
        public string Image { get; set; }
        public string CountryName { get; set; }
        public string BreweryName { get; set; }
        public double Abv { get; set; }
        public string Rating { get; set; }
        public ICollection<Review> Reviews { get; set; }
    }
}
