﻿using System;
using System.Collections.Generic;

namespace BeerOverFlow.Services.ModelsDTo
{
    public class BreweryDTO
    {
        public BreweryDTO()
        {
            BeerDTOs = new List<BeerDTO>();
        }
        public int BreweryId { get; set; }
        public string BreweryName { get; set; }
        public ICollection<BeerDTO> BeerDTOs { get; set; }
        public string Description { get; set; }
        public CountryDTO Country { get; set; }

        internal bool Any(Func<object, object> p)
        {
            throw new NotImplementedException();
        }
    }
}
