﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverFlow.Services.ModelsDTo
{
    public class StyleDTO
    {
        public int StyleId { get; set; }
        public string StyleName { get; set; }
    }
}
