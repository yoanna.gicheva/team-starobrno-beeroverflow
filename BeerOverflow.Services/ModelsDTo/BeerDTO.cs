﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverFlow.Services.ModelsDTo
{
   public class BeerDTO
    {
        public int BeerId { get; set; }
        public BreweryDTO Brewery { get; set; }
        public string BeerName { get; set; }
        public StyleDTO Style { get; set; }
        public double Rating { get; set; }
        public CountryDTO Country { get; set; }
        public double Abv { get; set; }
        public string Image { get; set; }
    }
}
