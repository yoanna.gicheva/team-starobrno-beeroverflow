﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverFlow.Services.ModelsDTo
{
    public class CountryDTO
    {
        public int CountryId { get; set; }
        public bool  IsDeleted{ get; set; }
        public string CountryName { get; set; }
    }
}
