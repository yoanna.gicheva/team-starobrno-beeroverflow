﻿using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverFlow.AutoMapperAndProfiles;
using BeerOverFlow.Services.Services;
using BeerOverFlowData;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Tests.UserService_Should
{
    [TestClass]
    public class DeleteAll_Should
    {
        [TestMethod]
        public void Success_UserExist()
        {
            var options = Utility.GetOptions(nameof(Success_UserExist));
            var contextConfig = new BeerOverFlowContext(options);

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                arrangeContext.Countries.Add(new Country()
                {
                    CountryName = "Bulgaria"
                });
                arrangeContext.Users.Add(new User()
                {
                    UserName = "yoja",
                    Description = "Studying in Telerik",
                    PasswordHash = "amo967",
                    Email = "yoja@gmail.com",
                    CountryId = 1,
                    CreatedOn = DateTime.Now
                });
                arrangeContext.Users.Add(new User()
                {
                    UserName = "dogokun",
                    Description = "The best dog ever :D",
                    PasswordHash = "dogidogi",
                    Email = "dogi@gmail.com",
                    CountryId = 1,
                    CreatedOn = DateTime.Now
                });
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                var result = sut.DeleteAll();

                Assert.AreEqual(assertContext.Users.Count(), 0);
                Assert.IsTrue(result);
            }
        }
        [TestMethod]
        public void Fail_NoUsers()
        {
            var options = Utility.GetOptions(nameof(Fail_NoUsers));
            var contextConfig = new BeerOverFlowContext(options);

            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                var result = sut.DeleteAll();
                ICollection<Style> users = assertContext.Styles.ToList();

                Assert.AreEqual(users.Count, 0);
                Assert.IsFalse(result);

            }
        }
    }
}
