﻿using BeerOverflow.Data.Entities;
using BeerOverFlow.Services.Services;
using BeerOverFlowData;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Tests.UserService_Should
{
    [TestClass]
    public class UserId_Should
    {
        [TestMethod]
        public void Success_CorrectUser()
        {
            var options = Utility.GetOptions(nameof(Success_CorrectUser));
            var contextConfig = new BeerOverFlowContext(options);
            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                ContextSet(arrangeContext);
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                var result = sut.UserId("yoja");

                Assert.AreEqual(result, 2);
            }
        }
        [TestMethod]
        public void Fail_NoSuchId()
        {
            var options = Utility.GetOptions(nameof(Fail_NoSuchId));
            var contextConfig = new BeerOverFlowContext(options);
            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                ContextSet(arrangeContext);
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                var result = sut.UserId("bonito");

                Assert.AreEqual(result, 0);
            }
        }
        private void ContextSet(BeerOverFlowContext arrangeContext)
        {
            arrangeContext.Users.Add(new User()
            {
                UserName = "dogokun",
                Description = "The best dog ever :D",
                PasswordHash = "dogidogi",
                Email = "dogi@gmail.com",
                CountryId = 1,
                CreatedOn = DateTime.Now
            });
            arrangeContext.Users.Add(new User()
            {
                UserName = "yoja",
                Description = "Studying in Telerik",
                PasswordHash = "amo967",
                Email = "yoja@gmail.com",
                CountryId = 1,
                CreatedOn = DateTime.Now
            });
            arrangeContext.SaveChanges();
        }

    }
}
