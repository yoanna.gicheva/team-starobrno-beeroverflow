﻿using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverFlow.AutoMapperAndProfiles;
using BeerOverFlow.Services.Services;
using BeerOverFlowData;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Tests.UserService_Should
{
    [TestClass]
    public class DeleteById_Should
    {
        [TestMethod]
        public void SuccessDelete_UserExists()
        {
            var options = Utility.GetOptions(nameof(SuccessDelete_UserExists));
            var contextConfig = new BeerOverFlowContext(options);

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                ContextSet(arrangeContext);
            }

            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                var result = sut.DeleteById(2);

                Assert.AreEqual(assertContext.Users.Count(), 1);
                Assert.IsTrue(result);
            }
        }
        [TestMethod]
        public void FailDeleteById_NoSuchUser()
        {
            var options = Utility.GetOptions(nameof(FailDeleteById_NoSuchUser));
            var contextConfig = new BeerOverFlowContext(options);

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                ContextSet(arrangeContext);

            }

            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                var result = sut.DeleteById(10);

                Assert.AreEqual(assertContext.Users.Count(), 2);
                Assert.IsFalse(result);
            }
        }
        [TestMethod]
        public void FailDeleteById_DatabaseIsEmpty()
        {
            var options = Utility.GetOptions(nameof(FailDeleteById_DatabaseIsEmpty));
            var contextConfig = new BeerOverFlowContext(options);


            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                var result = sut.DeleteById(2);

                Assert.AreEqual(assertContext.Users.Count(), 0);
                Assert.IsFalse(result);
            }
        }
        private void ContextSet(BeerOverFlowContext arrangeContext)
        {
            arrangeContext.Countries.Add(new Country()
            {
                CountryName = "Bulgaria"
            });
            arrangeContext.Users.Add(new User()
            {
                UserName = "yoja",
                Description = "Studying in Telerik",
                PasswordHash = "amo967",
                Email = "yoja@gmail.com",
                CountryId = 1,
                CreatedOn = DateTime.Now
            });
            arrangeContext.Users.Add(new User()
            {
                UserName = "dogokun",
                Description = "The best dog ever :D",
                PasswordHash = "dogidogi",
                Email = "dogi@gmail.com",
                CountryId = 1,
                CreatedOn = DateTime.Now
            });
            arrangeContext.SaveChanges();
        }
    }
}
