﻿using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverFlow.AutoMapperAndProfiles;
using BeerOverFlow.Services.Services;
using BeerOverFlowData;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Tests.UserService_Should
{
    [TestClass]
    public class AddToDrankList_Should
    {
        [TestMethod]
        public void Success_AddToDranklist()
        {
            var options = Utility.GetOptions(nameof(Success_AddToDranklist));
            var contextConfig = new BeerOverFlowContext(options);

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                ContextSet(arrangeContext);
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                var result = sut.AddToDranklist(1, 1);


                Assert.IsTrue(result);
                Assert.AreEqual(assertContext.Users.Include(u => u.DrankList).FirstOrDefault(u=>u.Id==1).DrankList.Count, 1);
            }

        }
        [TestMethod]
        public void Fail_NoSuchUserId()
        {
            var options = Utility.GetOptions(nameof(Fail_NoSuchUserId));
            var contextConfig = new BeerOverFlowContext(options);

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                ContextSet(arrangeContext);
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                var result = sut.AddToDranklist(1, 3);

                Assert.IsFalse(result);
                Assert.AreEqual(assertContext.Users.Include(u => u.DrankList).FirstOrDefault(u => u.Id == 1).DrankList.Count, 0);
            }

        }
        [TestMethod]
        public void Fail_NoSuchBeerId()
        {
            var options = Utility.GetOptions(nameof(Fail_NoSuchBeerId));
            var contextConfig = new BeerOverFlowContext(options);

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                ContextSet(arrangeContext);
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                var result = sut.AddToDranklist(3, 1);

                Assert.IsFalse(result);
                Assert.AreEqual(assertContext.Users.Include(u => u.DrankList).FirstOrDefault(u => u.Id == 1).DrankList.Count, 0);
            }
        }
        [TestMethod]
        public void Fail_AlreadyContains()
        {
            var options = Utility.GetOptions(nameof(Fail_AlreadyContains));
            var contextConfig = new BeerOverFlowContext(options);

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                ContextSet(arrangeContext);
                var service = new UserService(arrangeContext);
                service.AddToDranklist(1, 1);
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                var result = sut.AddToDranklist(1, 1);

                Assert.IsFalse(result);
                Assert.AreEqual(assertContext.Users.Include(u => u.DrankList).FirstOrDefault(u => u.Id == 1).DrankList.Count, 1);
            }

        }
        private void ContextSet(BeerOverFlowContext arrangeContext)
        {
            arrangeContext.Countries.Add(new Country()
            {
                CountryName = "Bulgaria"
            });
            arrangeContext.Users.Add(new User()
            {
                UserName = "dogokun",
                Description = "The best dog ever :D",
                PasswordHash = "dogidogi",
                Email = "dogi@gmail.com",
                CountryId = 1,
                CreatedOn = DateTime.Now
            });
            arrangeContext.Beers.Add(new Beer()
            {
                BeerName = "Ariana",
                CountryId = 1,
                Rating = 5,
                BreweryId = 1,
                IsDeleted = false
            });
            arrangeContext.SaveChanges();
        }
    }
}
