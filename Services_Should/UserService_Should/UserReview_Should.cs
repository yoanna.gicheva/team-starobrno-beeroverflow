﻿using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverflow.Services.Services;
using BeerOverflow.Services.Services.Contracts;
using BeerOverFlow.AutoMapperAndProfiles;
using BeerOverFlow.Services.Services;
using BeerOverFlowData;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Tests.UserService_Should
{
    [TestClass]
    public class UserReview_Should
    {
        [TestMethod]
        public void Success_CorrectReviews()
        {
            var options = Utility.GetOptions(nameof(Success_CorrectReviews));
            var contextConfig = new BeerOverFlowContext(options);

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                ContextSet(arrangeContext);
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                var result = sut.UserReview("dogokun");

                Assert.IsNotNull(result);
                Assert.AreEqual(result.Count, 1);
            }
        }
        [TestMethod]
        public void Fail_NoSuchUser()
        {
            var options = Utility.GetOptions(nameof(Success_CorrectReviews));
            var contextConfig = new BeerOverFlowContext(options);
            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                ContextSet(arrangeContext);
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                var result = sut.UserReview("222");

                Assert.IsNull(result);
            }
        }
        [TestMethod]
        public void Fail_ReviewsAreNull()
        {
            var options = Utility.GetOptions(nameof(Success_CorrectReviews));
            var contextConfig = new BeerOverFlowContext(options);

            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                var result = sut.UserReview("222");

                Assert.IsNull(result);
            }
        }
        private void ContextSet(BeerOverFlowContext arrangeContext)
        {
            arrangeContext.Countries.Add(new Country()
            {
                CountryName = "Bulgaria"
            });
            arrangeContext.Users.Add(new User()
            {
                UserName = "dogokun",
                Description = "The best dog ever :D",
                PasswordHash = "dogidogi",
                Email = "dogi@gmail.com",
                CountryId = 1,
                CreatedOn = DateTime.Now
            });
            arrangeContext.Beers.Add(new Beer()
            {
                BeerName = "Ariana",
                CountryId = 1,
                Rating = 5,
                BreweryId = 1,
                IsDeleted = false
            });
            arrangeContext.SaveChanges();
            var service = new ReviewService(arrangeContext);
            service.PostReview("Default", 1, 1);

        }

    }
}
