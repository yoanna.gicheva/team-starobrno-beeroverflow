﻿using BeerOverflow.Data.Entities;
using BeerOverFlow.Services.Services;
using BeerOverFlowData;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Tests.UserService_Should
{
    [TestClass]
    public class UserIsBanned_Should
    {
        [TestMethod]
        public void Success_IsBanned()
        {
            var options = Utility.GetOptions(nameof(Success_IsBanned));
            var contextConfig = new BeerOverFlowContext(options);
            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                ContextSet(arrangeContext);
                var service = new UserService(arrangeContext);
                service.BanUser(1);
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                var result = sut.UserIsBanned(1);

                Assert.IsTrue(result);
            }
        }
        [TestMethod]
        public void Success_IsNotBanned()
        {
            var options = Utility.GetOptions(nameof(Success_IsNotBanned));
            var contextConfig = new BeerOverFlowContext(options);
            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                ContextSet(arrangeContext);
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                var result = sut.UserIsBanned(1);

                Assert.IsFalse(result);
            }
        }
        private void ContextSet(BeerOverFlowContext arrangeContext)
        {
            arrangeContext.Users.Add(new User()
            {
                UserName = "dogokun",
                Description = "The best dog ever :D",
                PasswordHash = "dogidogi",
                Email = "dogi@gmail.com",
                CountryId = 1,
                CreatedOn = DateTime.Now
            });
            arrangeContext.SaveChanges();
        }
    }
}
