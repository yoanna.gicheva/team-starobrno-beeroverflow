﻿using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverFlow.AutoMapperAndProfiles;
using BeerOverFlow.Services.ModelsDTO;
using BeerOverFlow.Services.Services;
using BeerOverFlowData;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Tests.UserService_Should
{
    [TestClass]
    public class CreateUser_Should
    {
        [TestMethod]
        public void Success_CreateUser()
        {
            var myProfile = new AutoMapperConfiguration();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            var mapper = new Mapper(configuration);
            var options = Utility.GetOptions(nameof(Success_CreateUser));
            var contextConfig = new BeerOverFlowContext(options);

            var toCreate = new UserViewDTO()
            {
                UserName = "dogokun",
                Description = "The best dog ever :D",
                Password = "dogidogi",
                Email = "dogi@gmail.com",
                CountryId = 1,
            };

            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                bool actual = sut.CreateUser(toCreate, mapper);

                Assert.IsTrue(actual);
                Assert.AreEqual(toCreate.UserName, assertContext.Users.FirstOrDefault(u => u.Id.Equals(1)).UserName);
            }
        }
        [TestMethod]
        public void Fail_ThereIsAlreadySuchUsername()
        {
            var myProfile = new AutoMapperConfiguration();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            var mapper = new Mapper(configuration);
            var options = Utility.GetOptions(nameof(Fail_ThereIsAlreadySuchUsername));
            var contextConfig = new BeerOverFlowContext(options);

            var toCreate = new UserViewDTO()
            {
                UserName = "dogokun",
                Description = "The best dog ever :D",
                Password = "dogidogi",
                CountryId = 1,
            };
            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                ContextSet(arrangeContext);
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                bool actual = sut.CreateUser(toCreate, mapper);

                Assert.IsFalse(actual);
                Assert.IsTrue(assertContext.Users.Any(u=>u.UserName.Equals(toCreate.UserName)));
            }
        }
        [TestMethod]
        public void Success_ThereIsAlreadySuchEmail()
        {
            var myProfile = new AutoMapperConfiguration();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            var mapper = new Mapper(configuration);
            var options = Utility.GetOptions(nameof(Fail_ThereIsAlreadySuchUsername));
            var contextConfig = new BeerOverFlowContext(options);

            var toCreate = new UserViewDTO()
            {
                Description = "The best dog ever :D",
                Password = "dogidogi",
                Email = "dogi@gmail.com",
                CountryId = 1,
            };
            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                ContextSet(arrangeContext);
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                bool actual = sut.CreateUser(toCreate, mapper);

                Assert.IsFalse(actual);
                Assert.IsTrue(assertContext.Users.Any(u => u.Email.Equals(toCreate.Email)));
            }
        }

        private void ContextSet(BeerOverFlowContext arrangeContext)
        {
            arrangeContext.Countries.Add(new Country()
            {
                CountryName = "Bulgaria"
            });
            arrangeContext.Users.Add(new User()
            {
                UserName = "dogokun",
                Description = "The best dog ever :D",
                PasswordHash = "dogidogi",
                Email = "dogi@gmail.com",
                CountryId = 1,
                CreatedOn = DateTime.Now
            });
            arrangeContext.Beers.Add(new Beer()
            {
                BeerName = "Ariana",
                CountryId = 1,
                Rating = 5,
                BreweryId = 1,
                IsDeleted = false
            });
            arrangeContext.SaveChanges();
        }
    }
}
