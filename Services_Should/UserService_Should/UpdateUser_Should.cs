﻿using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverFlow.AutoMapperAndProfiles;
using BeerOverFlow.Services.ModelsDTO;
using BeerOverFlow.Services.Services;
using BeerOverFlowData;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Tests.UserService_Should
{
    [TestClass]
    public class UpdateUser_Should
    {
        [TestMethod]
        public void Success_UserUpdated()
        {
            var myProfile = new AutoMapperConfiguration();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            var mapper = new Mapper(configuration);
            var options = Utility.GetOptions(nameof(Success_UserUpdated));
            var contextConfig = new BeerOverFlowContext(options);

            var toUpdate = new UserViewDTO()
            {
                UserName = "dogokun23",
                Description = "The best dog ever :D",
                Password = "dogidogi",
                Email = "dogi@gmail.com",
                CountryId = 1,
            };

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                ContextSet(arrangeContext);
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                var actual = sut.UpdateUser(2, toUpdate, mapper);

                Assert.IsNotNull(actual);
                Assert.IsTrue(assertContext.Users.Any(u => u.UserName.Equals(toUpdate.UserName)));
            }

        }
        [TestMethod]
        public void Fail_NoSuchId()
        {
            var myProfile = new AutoMapperConfiguration();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            var mapper = new Mapper(configuration);
            var options = Utility.GetOptions(nameof(Fail_NoSuchId));
            var contextConfig = new BeerOverFlowContext(options);

            var toUpdate = new UserViewDTO()
            {
                UserName = "dogokun23",
                Description = "The best dog ever :D",
                Password = "dogidogi",
                Email = "dogi@gmail.com",
                CountryId = 1,
            };

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                ContextSet(arrangeContext);
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                var actual = sut.UpdateUser(3, toUpdate, mapper);

                Assert.IsNull(actual);
                Assert.IsFalse(assertContext.Users.Any(u => u.UserName.Equals(toUpdate.UserName)));
            }
        }

        [TestMethod]
        public void Fail_AlreadySuchEmail()
        {
            var myProfile = new AutoMapperConfiguration();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            var mapper = new Mapper(configuration);
            var options = Utility.GetOptions(nameof(Fail_AlreadySuchEmail));
            var contextConfig = new BeerOverFlowContext(options);

            var toUpdate = new UserViewDTO()
            {
                UserName = "dogokun23",
                Description = "The best dog ever :D",
                Password = "dogidogi",
                Email = "yoja@gmail.com",
                CountryId = 1,
            };

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                ContextSet(arrangeContext);
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                var actual = sut.UpdateUser(2, toUpdate, mapper);

                Assert.IsNull(actual);
                Assert.IsFalse(assertContext.Users.Any(u => u.UserName.Equals(toUpdate.UserName)));
            }
        }
        private void ContextSet(BeerOverFlowContext arrangeContext)
        {
            arrangeContext.Countries.Add(new Country()
            {
                CountryName = "Bulgaria"
            });
            arrangeContext.Users.Add(new User()
            {
                UserName = "yoja",
                Description = "Studying in Telerik",
                PasswordHash = "amo967",
                Email = "yoja@gmail.com",
                CountryId = 1,
                CreatedOn = DateTime.Now
            });
            arrangeContext.Users.Add(new User()
            {
                UserName = "dogokun",
                Description = "The best dog ever :D",
                PasswordHash = "dogidogi",
                Email = "dogi@gmail.com",
                CountryId = 1,
                CreatedOn = DateTime.Now
            });
            arrangeContext.SaveChanges();
        }
    }
}
