﻿using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverFlow.AutoMapperAndProfiles;
using BeerOverFlow.Services.Services;
using BeerOverFlowData;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Tests.UserService_Should
{
    [TestClass]
    public class RemoveFromWishlist_Should
    {
        [TestMethod]
        public void Success_RemovedFromWishlist()
        {
            var options = Utility.GetOptions(nameof(Success_RemovedFromWishlist));
            var contextConfig = new BeerOverFlowContext(options);

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                ContextSet(arrangeContext);
                var service = new UserService(arrangeContext);
                service.AddToWishlist(1, 1);
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                var result = sut.RemoveFromWishlist(1, 1);

                Assert.IsTrue(result);
                var user = assertContext.Users.Include(u => u.WishList).FirstOrDefault(u => u.Id == 1);
                Assert.AreEqual(user.WishList.Count(), 0);
            }
        }
        [TestMethod]
        public void FailRemove_NoSuchUserId()
        {
            var options = Utility.GetOptions(nameof(FailRemove_NoSuchUserId));
            var contextConfig = new BeerOverFlowContext(options);

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                ContextSet(arrangeContext);
                var service = new UserService(arrangeContext);
                service.AddToWishlist(1, 1);
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                var result = sut.RemoveFromWishlist(1, 10);

                Assert.IsFalse(result);
                var user = assertContext.Users.Include(u=>u.WishList).FirstOrDefault(u => u.Id == 1);
                Assert.AreEqual(user.WishList.Count(), 1);
            }
        }
        [TestMethod]
        public void Fail_BeerDoesNotExist()
        {
            var options = Utility.GetOptions(nameof(Fail_BeerDoesNotExist));
            var contextConfig = new BeerOverFlowContext(options);

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                ContextSet(arrangeContext);
                var service = new UserService(arrangeContext);
                service.AddToWishlist(1, 1);
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                var result = sut.RemoveFromWishlist(4, 1);

                Assert.IsFalse(result);
                var user = assertContext.Users.Include(u => u.WishList).FirstOrDefault(u => u.Id == 1);
                Assert.AreEqual(user.WishList.Count, 1);
            }
        }
        [TestMethod]
        public void Fail_BeerIsNotContainedInWishlist()
        {
            var options = Utility.GetOptions(nameof(Fail_BeerIsNotContainedInWishlist));
            var contextConfig = new BeerOverFlowContext(options);

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                ContextSet(arrangeContext);
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                var result = sut.RemoveFromWishlist(1, 1);

                Assert.IsFalse(result);
                var user = assertContext.Users.Include(u => u.WishList).FirstOrDefault(u => u.Id == 1);
                Assert.AreEqual(user.WishList.Count, 0);
            }
        }
        private void ContextSet(BeerOverFlowContext arrangeContext)
        {
            arrangeContext.Countries.Add(new Country()
            {
                CountryName = "Bulgaria"
            });
            arrangeContext.Users.Add(new User()
            {
                UserName = "dogokun",
                Description = "The best dog ever :D",
                PasswordHash = "dogidogi",
                Email = "dogi@gmail.com",
                CountryId = 1,
                CreatedOn = DateTime.Now
            });
            arrangeContext.Beers.Add(new Beer()
            {
                BeerName = "Ariana",
                CountryId = 1,
                Rating = 5,
                BreweryId = 1,
                IsDeleted = false
            });

            arrangeContext.SaveChanges();
        }

    }
}
