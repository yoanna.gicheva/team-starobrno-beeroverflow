﻿using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverFlow.AutoMapperAndProfiles;
using BeerOverFlow.Services.ModelsDTo;
using BeerOverFlow.Services.Services;
using BeerOverFlowData;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Tests.UserService_Should
{

    [TestClass]
    public class BanUser_Should
    {
        [TestMethod]
        public void Success_BanIsNull()
        {
            var options = Utility.GetOptions(nameof(Success_BanIsNull));
            var contextConfig = new BeerOverFlowContext(options);

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                arrangeContext.Countries.Add(new Country()
                {
                    CountryName = "Bulgaria"
                });
                arrangeContext.Users.Add(new User()
                {
                    UserName = "dogokun",
                    Description = "The best dog ever :D",
                    PasswordHash = "dogidogi",
                    Email = "dogi@gmail.com",
                    CountryId = 1,
                    CreatedOn = DateTime.Now
                });
          
                arrangeContext.SaveChanges();
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                var result = sut.BanUser(1);
                var user = assertContext.Users.Include(u => u.Ban).FirstOrDefault(u => u.Id == 1);

                Assert.IsTrue(result);
                Assert.IsTrue(user.Ban.Banned);
            }

        }
        [TestMethod]
        public void Success_BanIsFalse()
        {
            var options = Utility.GetOptions(nameof(Success_BanIsFalse));
            var contextConfig = new BeerOverFlowContext(options);

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                arrangeContext.Countries.Add(new Country()
                {
                    CountryName = "Bulgaria"
                });
                arrangeContext.Bans.Add(new Ban("Default", false)
                {
                    BanId = 1,
                });
                arrangeContext.Users.Add(new User()
                {
                    UserName = "dogokun",
                    BanId = 1,
                    Description = "The best dog ever :D",
                    PasswordHash = "dogidogi",
                    Email = "dogi@gmail.com",
                    CountryId = 1,
                    CreatedOn = DateTime.Now
                }) ;
               

                arrangeContext.SaveChanges();
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                var result = sut.BanUser(1);
                var user = assertContext.Users.Include(u => u.Ban).FirstOrDefault(u => u.Id == 1);

                Assert.IsTrue(result);
                Assert.IsTrue(user.Ban.Banned);
            }
        }
        [TestMethod]
        public void Fail_NoSuchUser()
        {
            var options = Utility.GetOptions(nameof(Fail_NoSuchUser));
            var contextConfig = new BeerOverFlowContext(options);

            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                var result = sut.BanUser(1);

                Assert.IsFalse(result);
            }
        }
    }
}
