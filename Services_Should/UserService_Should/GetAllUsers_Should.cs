﻿using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverFlow.AutoMapperAndProfiles;
using BeerOverFlow.Services.ModelsDTo;
using BeerOverFlow.Services.Services;
using BeerOverFlowData;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Tests.UserService_Should
{
    [TestClass]
    public class GetAllUsers_Should
    {
        [TestMethod]
        public void Succeess_GetAllUsers()
        {
            var myProfile = new AutoMapperConfiguration();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            var mapper = new Mapper(configuration);
            var options = Utility.GetOptions(nameof(Succeess_GetAllUsers));
            var contextConfig = new BeerOverFlowContext(options);

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                arrangeContext.Countries.Add(new Country()
                {
                    CountryName = "Bulgaria"
                });
                arrangeContext.Users.Add(new User()
                {
                    UserName = "yoja",
                    Description = "Studying in Telerik",
                    PasswordHash = "amo967",
                    Email = "yoja@gmail.com",
                    CountryId = 1,
                    CreatedOn = DateTime.Now
                });
                arrangeContext.Users.Add(new User()
                {
                    UserName = "dogokun",
                    Description = "The best dog ever :D",
                    PasswordHash = "dogidogi",
                    Email = "dogi@gmail.com",
                    CountryId = 1,
                    CreatedOn = DateTime.Now
                });
                arrangeContext.SaveChanges();
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                var actual = sut.GetAllUsersAsync(mapper).Result;
                var actualFirst = assertContext.Users.FirstOrDefault(u => u.Id.Equals(1));
                var actualSecond = assertContext.Users.FirstOrDefault(u => u.Id.Equals(2));

                Assert.AreEqual(actual.Count, 2);
                Assert.AreEqual(actualFirst.UserName, assertContext.Users.First(u => u.Id.Equals(1)).UserName);
                Assert.AreEqual(actualSecond.UserName, assertContext.Users.First(u => u.Id.Equals(2)).UserName);
            }
        }
        [TestMethod]
        public void Fail_NoUsers()
        {
            var myProfile = new AutoMapperConfiguration();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            var mapper = new Mapper(configuration);
            var options = Utility.GetOptions(nameof(Fail_NoUsers));
            var contextConfig = new BeerOverFlowContext(options);
          
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                var actual = sut.GetAllUsersAsync(mapper).Result;

                Assert.AreEqual(actual.Count, 0);
            }
        }
    }

}
