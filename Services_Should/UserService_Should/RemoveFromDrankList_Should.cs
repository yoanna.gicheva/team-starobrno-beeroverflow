﻿using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverFlow.AutoMapperAndProfiles;
using BeerOverFlow.Services.Services;
using BeerOverFlowData;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Tests.UserService_Should
{
    [TestClass]
    class RemoveFromDrankList_Should
    {
        [TestMethod]
        public void Success_RemovedFromDranklist()
        {
            var options = Utility.GetOptions(nameof(Success_RemovedFromDranklist));
            var contextConfig = new BeerOverFlowContext(options);

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                ContextSet(arrangeContext);
                var service = new UserService(arrangeContext);
                service.AddToDranklist(1, 1);
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                var result = sut.RemoveFromWishlist(1, 1);

                Assert.IsTrue(result);
                 var user = assertContext.Users.Include(u => u.DrankList).FirstOrDefault(u => u.Id == 1);
                Assert.AreEqual(user.WishList.Count(), 0);
            }
        }
        [TestMethod]
        public void FailRemove_NoSuchUserId2()
        {
            var options = Utility.GetOptions(nameof(FailRemove_NoSuchUserId2));
            var contextConfig = new BeerOverFlowContext(options);

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                ContextSet(arrangeContext);
                var service = new UserService(arrangeContext);
                service.AddToDranklist(1, 1);
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                var result = sut.RemoveFromDranklist(1, 10);

                Assert.IsFalse(result);
                var user = assertContext.Users.Include(u => u.DrankList).FirstOrDefault(u => u.Id == 1);
                Assert.AreEqual(user.WishList.Count(), 1);
            }
        }
        [TestMethod]
        public void Fail_BeerDoesNotExist2()
        {
            var options = Utility.GetOptions(nameof(Fail_BeerDoesNotExist2));
            var contextConfig = new BeerOverFlowContext(options);

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                ContextSet(arrangeContext);
                var service = new UserService(arrangeContext);
                service.AddToDranklist(1, 1);
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                var result = sut.RemoveFromDranklist(4, 1);

                Assert.IsFalse(result);
                var user = assertContext.Users.Include(u => u.DrankList).FirstOrDefault(u => u.Id == 1);
                Assert.AreEqual(user.WishList.Count(), 1);
            }
        }
        [TestMethod]
        public void Fail_BeerIsNotContainedInDrankList()
        {
            var options = Utility.GetOptions(nameof(Fail_BeerIsNotContainedInDrankList));
            var contextConfig = new BeerOverFlowContext(options);

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                ContextSet(arrangeContext);
                var service = new UserService(arrangeContext);
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                var result = sut.RemoveFromDranklist(1, 1);

                Assert.IsFalse(result);
                var user = assertContext.Users.Include(u => u.DrankList).FirstOrDefault(u => u.Id == 1);
                Assert.AreEqual(user.WishList.Count(), 0);
            }
        }
        private void ContextSet(BeerOverFlowContext arrangeContext)
        {
            arrangeContext.Countries.Add(new Country()
            {
                CountryName = "Bulgaria"
            });
            arrangeContext.Users.Add(new User()
            {
                UserName = "dogokun",
                Description = "The best dog ever :D",
                PasswordHash = "dogidogi",
                Email = "dogi@gmail.com",
                CountryId = 1,
                CreatedOn = DateTime.Now
            });
            arrangeContext.Beers.Add(new Beer()
            {
                BeerName = "Ariana",
                CountryId = 1,
                Rating = 5,
                BreweryId = 1,
                IsDeleted = false
            });
        }
    }
}
