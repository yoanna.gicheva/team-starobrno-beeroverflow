﻿using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverFlow.AutoMapperAndProfiles;
using BeerOverFlow.Services.Services;
using BeerOverFlowData;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Tests.UserService_Should
{
    [TestClass]
   public class AddToWishList_Should
    {
        [TestMethod]
        public void Success_AddToWishList()
        {
            var options = Utility.GetOptions(nameof(Success_AddToWishList));
            var contextConfig = new BeerOverFlowContext(options);

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                ContextSet(arrangeContext);
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var beer = assertContext.Beers.FirstOrDefault(b => b.BeerId.Equals(1));
                var user = assertContext.Users.FirstOrDefault(b => b.Id.Equals(1));
                var sut = new UserService(assertContext);
                var result = sut.AddToWishlist(1, 1);


                Assert.IsTrue(result);
                Assert.AreEqual(assertContext.Users.Include(u=>u.WishList).FirstOrDefault(u => u.Id == 1).WishList.Count, 1);
            }

        }
        [TestMethod]
        public void Fail_NoSuchUserIdWishlist()
        {
            var options = Utility.GetOptions(nameof(Fail_NoSuchUserIdWishlist));
            var contextConfig = new BeerOverFlowContext(options);

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                ContextSet(arrangeContext);
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var beer = assertContext.Beers.FirstOrDefault(b => b.BeerId.Equals(1));
                var user = assertContext.Users.FirstOrDefault(b => b.Id.Equals(1));
                var sut = new UserService(assertContext);
                var result = sut.AddToWishlist(1, 3);

                Assert.IsFalse(result);
                Assert.AreEqual(assertContext.Users.Include(u => u.WishList).FirstOrDefault(u => u.Id == 1).WishList.Count, 0);
            }

        }
        [TestMethod]
        public void Fail_NoSuchBeerIdWishlist()
        {
            var options = Utility.GetOptions(nameof(Fail_NoSuchBeerIdWishlist));
            var contextConfig = new BeerOverFlowContext(options);

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                ContextSet(arrangeContext);
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var beer = assertContext.Beers.FirstOrDefault(b => b.BeerId.Equals(1));
                var user = assertContext.Users.FirstOrDefault(b => b.Id.Equals(1));
                var sut = new UserService(assertContext);
                var result = sut.AddToWishlist(3, 1);

                Assert.IsFalse(result);
                Assert.AreEqual(assertContext.Users.Include(u => u.WishList).FirstOrDefault(u => u.Id == 1).WishList.Count, 0);
            }
        }
        [TestMethod]
        public void Fail_AlreadyContainsWishlist()
        {
            var options = Utility.GetOptions(nameof(Fail_AlreadyContainsWishlist));
            var contextConfig = new BeerOverFlowContext(options);

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                ContextSet(arrangeContext);
                arrangeContext.SaveChanges();
                var service = new UserService(arrangeContext);
                service.AddToWishlist(1, 1);
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                var result = sut.AddToWishlist(1, 1);

                Assert.IsFalse(result);
                Assert.AreEqual(assertContext.Users.Include(u => u.WishList).FirstOrDefault(u => u.Id == 1).WishList.Count, 1);
            }

        }

        private void ContextSet(BeerOverFlowContext arrangeContext)
        {
            arrangeContext.Countries.Add(new Country()
            {
                CountryName = "Bulgaria"
            });
            arrangeContext.Users.Add(new User()
            {
                UserName = "dogokun",
                Description = "The best dog ever :D",
                PasswordHash = "dogidogi",
                Email = "dogi@gmail.com",
                CountryId = 1,
                CreatedOn = DateTime.Now
            });
            arrangeContext.Beers.Add(new Beer()
            {
                BeerName = "Ariana",
                CountryId = 1,
                Rating = 5,
                BreweryId = 1,
                IsDeleted = false
            });
            arrangeContext.SaveChanges();
        }
    }
}
