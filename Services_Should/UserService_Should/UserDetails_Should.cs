﻿using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverflow.Services.ModelsDTo.WebModels;
using BeerOverFlow.AutoMapperAndProfiles;
using BeerOverFlow.Services.Services;
using BeerOverFlowData;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Tests.UserService_Should
{
    [TestClass]
    public class UserDetails_Should
    {
        [TestMethod]
        public void Succeed_CorrectUserId()
        {
            var options = Utility.GetOptions(nameof(Succeed_CorrectUserId));
            var contextConfig = new BeerOverFlowContext(options);
            var myProfile = new AutoMapperConfiguration();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            var mapper = new Mapper(configuration);

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                arrangeContext.Countries.Add(new Country()
                {
                    CountryName = "Bulgaria"
                });
                arrangeContext.Users.Add(new User()
                {
                    UserName = "dogokun",
                    Description = "The best dog ever :D",
                    Email = "dogi@gmail.com",
                    CountryId = 1,
                });
                arrangeContext.SaveChanges();
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                UserDetailsModel user = sut.UserDetails("dogokun", mapper);
                Assert.IsNotNull(user);
                Assert.AreEqual(user.CountryName, "Bulgaria");
            }
        }
        [TestMethod]
        public void Success_CountryIsNull()
        {
            var options = Utility.GetOptions(nameof(Success_CountryIsNull));
            var contextConfig = new BeerOverFlowContext(options);
            var myProfile = new AutoMapperConfiguration();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            var mapper = new Mapper(configuration);

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                arrangeContext.Countries.Add(new Country()
                {
                    CountryName = "Bulgaria"
                });
                arrangeContext.Users.Add(new User()
                {
                    UserName = "dogokun",
                    Description = "The best dog ever :D",
                    Email = "dogi@gmail.com",
                });
                arrangeContext.SaveChanges();
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                UserDetailsModel user = sut.UserDetails("dogokun", mapper);
                Assert.IsNotNull(user);
                Assert.AreEqual(user.CountryName, "Unidentified");
            }
        }
        [TestMethod]
        public void Fail_NoSuchId()
        {
            var options = Utility.GetOptions(nameof(Success_CountryIsNull));
            var contextConfig = new BeerOverFlowContext(options);
            var myProfile = new AutoMapperConfiguration();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            var mapper = new Mapper(configuration);

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                arrangeContext.Countries.Add(new Country()
                {
                    CountryName = "Bulgaria"
                });
                arrangeContext.Users.Add(new User()
                {
                    UserName = "dogokun",
                    Description = "The best dog ever :D",
                    Email = "dogi@gmail.com",
                });
                arrangeContext.SaveChanges();
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new UserService(assertContext);
                UserDetailsModel user = sut.UserDetails("22", mapper);

                Assert.IsNull(user);
            }
        }
    }
}
