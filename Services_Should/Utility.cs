using BeerOverFlowData;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BeerOverflow.Tests
{
    [TestClass]
    public class Utility
    {
        [TestMethod]
        public static DbContextOptions<BeerOverFlowContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<BeerOverFlowContext>().
                UseInMemoryDatabase(databaseName).
                Options;
        }
    }
}