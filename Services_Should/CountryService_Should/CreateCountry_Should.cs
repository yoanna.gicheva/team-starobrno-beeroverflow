﻿using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverFlow.AutoMapperAndProfiles;
using BeerOverFlow.Services.ModelsDTo;
using BeerOverFlow.Services.Services;
using BeerOverFlowData;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace BeerOverflow.Tests.CountryService_Should
{
    //var myProfile = new MyProfile();
    //var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
    //var mapper = new Mapper(configuration);
    [TestClass]
    public class CreateCountry_Should
    {

        [TestMethod]
        public void CreateCountry_WithCorrectData_ReturnCountry()
        {
            var options = Utility.GetOptions(nameof(CreateCountry_WithCorrectData_ReturnCountry));
            var contextConfig = new BeerOverFlowContext(options);
            var expected = new Country()
            {
                CountryName = "Bulgaria",
                CountryId = 1,
                IsDeleted = false
            };
          
            var myProfile = new AutoMapperConfiguration();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            var mapper = new Mapper(configuration);

            var mapperMock = new Mock<IMapper>();
            mapperMock.Setup(m => m.Map<Country, CountryDTO>(It.IsAny<Country>())).Returns(new CountryDTO()
            {
                CountryId = 1,
                CountryName = "Bulgaria",
                IsDeleted = false
            });

            using (var arange = new BeerOverFlowContext(options))
            {
                var sut = new CountryService(arange);   

                var res = sut.Create(new CountryDTO() { CountryName = "Bulgaria" }, mapperMock.Object);               
            }

            using (var assertContext = new BeerOverFlowContext(options))
            {
                var count = assertContext.Countries.ToList().Count();
                var actual = assertContext.Countries.FirstOrDefault();
                Assert.AreEqual(1, count);
                Assert.AreEqual(expected.IsDeleted, actual.IsDeleted);
                Assert.AreEqual(expected.CountryId, actual.CountryId);
                Assert.AreEqual(expected.CountryName, actual.CountryName);
            }
        }

        [TestMethod]
        public void Shuld_NotCreateCountry_IFCountryExisicts()
        {
            var options = Utility.GetOptions(nameof(Shuld_NotCreateCountry_IFCountryExisicts));
            var contextConfig = new BeerOverFlowContext(options);

            using (var assertContext = new BeerOverFlowContext(options))
            {
                assertContext.Countries.Add(new Country()
                {
                    CountryId = 1,
                    CountryName = "Bulgaria",
                    IsDeleted = false
                });
                assertContext.SaveChanges();
                var sut = new CountryService(assertContext);
                var mapperMock = new Mock<IMapper>();
                mapperMock.Setup(m => m.Map<Country, CountryDTO>(It.IsAny<Country>())).Returns((Country src) => new CountryDTO()
                {
                    CountryId = src.CountryId,
                    CountryName = src.CountryName,
                    IsDeleted = src.IsDeleted
                });

                var res = sut.Create(new CountryDTO() { CountryName = "Bulgaria" }, mapperMock.Object);
                var count = assertContext.Countries.ToList().Count();
                var actual = assertContext.Countries.FirstOrDefault();
                Assert.AreEqual(1, count);
                Assert.AreEqual(null, res);
            }
        }

        [TestMethod]
        public void Should_ChangeStatusOfAlreadyCreatedCountry()
        {
            var options = Utility.GetOptions(nameof(Should_ChangeStatusOfAlreadyCreatedCountry));
            var contextConfig = new BeerOverFlowContext(options);

            using (var assertContext = new BeerOverFlowContext(options))
            {
                assertContext.Countries.Add(new Country()
                {
                    CountryId = 1,
                    CountryName = "Bulgaria",
                    IsDeleted = true
                });
                assertContext.SaveChanges();

                var sut = new CountryService(assertContext);
                var mapperMock = new Mock<IMapper>();
                mapperMock.Setup(m => m.Map<Country, CountryDTO>(It.IsAny<Country>())).Returns((Country src) => new CountryDTO()
                {
                    CountryId = src.CountryId,
                    CountryName = src.CountryName,
                    IsDeleted = src.IsDeleted
                });
                var expected = new Country()
                {
                    CountryName = "Bulgaria",
                    CountryId = 1,
                    IsDeleted = false
                };

                var res = sut.Create(new CountryDTO() { CountryName = "Bulgaria" }, mapperMock.Object);
                var count = assertContext.Countries.ToList().Count();
                var actual = assertContext.Countries.FirstOrDefault();
                Assert.AreEqual(1, count);
                Assert.AreEqual(expected.IsDeleted, actual.IsDeleted);
                Assert.AreEqual(expected.CountryId, actual.CountryId);
                Assert.AreEqual(expected.CountryName, actual.CountryName);
            }
        }


    }
}
