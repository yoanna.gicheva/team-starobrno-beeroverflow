﻿using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverFlow.AutoMapperAndProfiles;
using BeerOverFlow.Services.ModelsDTo;
using BeerOverFlow.Services.Services;
using BeerOverFlowData;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Tests.CountryService_Should
{
    [TestClass]
   public class Update_Should
    {
        [TestMethod]
        public void Should_ReturnUpdatedCountry()
        {
            var myProfile = new AutoMapperConfiguration();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            var mapper = new Mapper(configuration);
            

            //Arrange
            var options = Utility.GetOptions(nameof(Should_ReturnUpdatedCountry));
            var contextConfig = new BeerOverFlowContext(options);
            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                var testCountry = new Country()
                {
                    CountryName = "Bulgaria",
                    CountryId = 1,
                    IsDeleted = false
                };
                var testCountry02 = new Country()
                {
                    CountryName = "Poland",
                    CountryId = 2,
                    IsDeleted = false
                };

                arrangeContext.Countries.Add(testCountry);
                arrangeContext.Countries.Add(testCountry02);
                arrangeContext.SaveChanges();

            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                //Act  

                var sut = new CountryService(assertContext);

                var expected = new CountryDTO()
                {
                    CountryId=2,
                    CountryName="France",
                    IsDeleted=false
                };

                var actual = sut.Update("France", new CountryDTO() {CountryName="Poland", CountryId=2}, mapper);
                
                Assert.AreEqual(expected.CountryId, actual.CountryId);
                Assert.AreEqual(expected.CountryName, actual.CountryName);
                Assert.AreEqual(expected.IsDeleted, actual.IsDeleted);
                Assert.AreEqual(assertContext.Countries.Skip(1).First().CountryId, actual.CountryId);
                Assert.AreEqual(assertContext.Countries.Skip(1).First().CountryName, actual.CountryName);
                Assert.AreEqual(assertContext.Countries.Skip(1).First().IsDeleted, actual.IsDeleted);
            }
        }
        [TestMethod]
        public void Should_NotUpdatedCountry_ReturnNull()
        {
            var myProfile = new AutoMapperConfiguration();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            var mapper = new Mapper(configuration);


            //Arrange
            var options = Utility.GetOptions(nameof(Should_NotUpdatedCountry_ReturnNull));
            var contextConfig = new BeerOverFlowContext(options);
            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                var testCountry = new Country()
                {
                    CountryName = "Bulgaria",
                    CountryId = 1,
                    IsDeleted = false
                };
                var testCountry02 = new Country()
                {
                    CountryName = "Poland",
                    CountryId = 2,
                    IsDeleted = true
                };

                arrangeContext.Countries.Add(testCountry);
                arrangeContext.Countries.Add(testCountry02);
                arrangeContext.SaveChanges();

            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                //Act  

                var sut = new CountryService(assertContext);

                var expected = new CountryDTO()
                {
                    CountryId = 2,
                    CountryName = "Poland",
                    IsDeleted = true
                };

                var actual = sut.Update("France", new CountryDTO() { CountryName = "Poland", CountryId = 2 }, mapper);

                Assert.AreEqual(null, actual);
                Assert.AreEqual(assertContext.Countries.Skip(1).First().CountryId, expected.CountryId);
                Assert.AreEqual(assertContext.Countries.Skip(1).First().CountryName, expected.CountryName);
                Assert.AreEqual(assertContext.Countries.Skip(1).First().IsDeleted, expected.IsDeleted);
            }
        }
    }
}
