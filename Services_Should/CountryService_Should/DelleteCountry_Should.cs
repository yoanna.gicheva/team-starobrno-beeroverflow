﻿using BeerOverflow.Data.Entities;
using BeerOverFlow.Services.Services;
using BeerOverFlowData;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace BeerOverflow.Tests.CountryService_Should
{
    [TestClass]
    public class DelleteCountry_Should
    {
        [TestMethod]
        public void ChangeStatusButNotDeleteFromData()
        {
            var options = Utility.GetOptions(nameof(ChangeStatusButNotDeleteFromData));
            var contextConfig = new BeerOverFlowContext(options);
            var expected = new Country()
            {
                CountryName = "Bulgaria",
                CountryId = 1,
                IsDeleted = false
            };

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                arrangeContext.Countries.Add(expected);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new CountryService(assertContext);
                var res = sut.Delete(1);
                var res02 = sut.Delete(1);
                var count = assertContext.Countries.ToList().Count();
                var actual = assertContext.Countries.FirstOrDefault();
                Assert.AreEqual(1, count);
                Assert.AreNotEqual(expected.IsDeleted, actual.IsDeleted);
                Assert.AreEqual(true, res);
                Assert.AreEqual(false, res02);

            }
        }
    }
}
