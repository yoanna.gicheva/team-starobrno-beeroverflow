﻿using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverflow.Tests;
using BeerOverFlow.AutoMapperAndProfiles;
using BeerOverFlow.Services.ModelsDTo;
using BeerOverFlow.Services.Services;
using BeerOverFlowData;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace Services_Should.CountryService_Should
{
    [TestClass]
    public class GetCountryShoud
    {
        [TestMethod]
        public void Should_ReturnAllCountries()
        {
            var myProfile = new AutoMapperConfiguration();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            var mapper = new Mapper(configuration);
            var expected = new List<CountryDTO>();

            //Arrange
            var options = Utility.GetOptions(nameof(Should_ReturnAllCountries));
            var contextConfig = new BeerOverFlowContext(options);
            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                var testCountry = new Country()
                {
                    CountryName = "Bulgaria",
                    CountryId = 1,
                    IsDeleted = false
                };

                arrangeContext.Countries.Add(testCountry);
                arrangeContext.SaveChanges();

            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                //Act               
                var testCountry = new Country()
                {
                    CountryName = "Poland",
                    CountryId = 2,
                    IsDeleted = false
                };
                assertContext.Countries.Add(testCountry);
                assertContext.SaveChanges();
                var sut = new CountryService(assertContext);
                
                expected.Add(new CountryDTO
                {
                    CountryName = "Bulgaria",
                    CountryId = 1,
                    IsDeleted = false
                });
                expected.Add(new CountryDTO
                {
                    CountryName = "Poland",
                    CountryId = 2,
                    IsDeleted = false
                });              

                var actual = sut.GetCountry(mapper).ToList();
                Assert.AreEqual(expected[0].CountryId, actual[0].CountryId);
                Assert.AreEqual(expected[0].CountryName, actual[0].CountryName);
                Assert.AreEqual(expected[0].IsDeleted, actual[0].IsDeleted);
                Assert.AreEqual(expected[1].IsDeleted, actual[1].IsDeleted);
                Assert.AreEqual(expected[1].CountryName, actual[1].CountryName);
                Assert.AreEqual(expected[1].CountryId, actual[1].CountryId);
            }
        }

        [TestMethod]
        public void Should_ReturnCountryAtIndex()
        {
            var myProfile = new AutoMapperConfiguration();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            var mapper = new Mapper(configuration);
            var expected = new List<CountryDTO>();

            //Arrange
            var options = Utility.GetOptions(nameof(Should_ReturnCountryAtIndex));
            var contextConfig = new BeerOverFlowContext(options);
            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                var testCountry = new Country()
                {
                    CountryName = "Bulgaria",
                    CountryId = 1,
                    IsDeleted = false
                };
                var testCountry02 = new Country()
                {
                    CountryName = "Poland",
                    CountryId = 2,
                    IsDeleted = false
                };
                var testCountry03 = new Country()
                {
                    CountryName = "France",
                    CountryId = 3,
                    IsDeleted = false
                };
                arrangeContext.Countries.Add(testCountry);
                arrangeContext.Countries.Add(testCountry02);
                arrangeContext.Countries.Add(testCountry03);
                arrangeContext.SaveChanges();

            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                //Act              

                var sut = new CountryService(assertContext);
                var testCountry = new Country()
                {
                    CountryName = "Poland",
                    CountryId = 2,
                    IsDeleted = false
                };
                expected.Add(new CountryDTO
                {
                    IsDeleted = testCountry.IsDeleted,
                    CountryName = testCountry.CountryName,
                    CountryId = testCountry.CountryId
                });
                var actual = sut.GetCountry(mapper,2).ToList();
                Assert.AreEqual(expected[0].CountryId, actual[0].CountryId);
                Assert.AreEqual(expected[0].CountryName, actual[0].CountryName);
                Assert.AreEqual(expected[0].IsDeleted, actual[0].IsDeleted);
            }
        }
        [TestMethod]
        public void Should_BeNull()
        {
            var myProfile = new AutoMapperConfiguration();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            var mapper = new Mapper(configuration);
            var expected = new List<CountryDTO>();

            //Arrange
            var options = Utility.GetOptions(nameof(Should_BeNull));
            var contextConfig = new BeerOverFlowContext(options);
            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                var testCountry = new Country()
                {
                    CountryName = "Bulgaria",
                    CountryId = 1,
                    IsDeleted = true
                };
                var testCountry02 = new Country()
                {
                    CountryName = "Poland",
                    CountryId = 2,
                    IsDeleted = false
                };
                var testCountry03 = new Country()
                {
                    CountryName = "France",
                    CountryId = 3,
                    IsDeleted = true
                };
                arrangeContext.Countries.Add(testCountry);
                arrangeContext.Countries.Add(testCountry02);
                arrangeContext.Countries.Add(testCountry03);
                arrangeContext.SaveChanges();

            }

            using (var assertContext = new BeerOverFlowContext(options))
            {
                //Act              

                var sut = new CountryService(assertContext);                
                var actual = sut.GetCountry(mapper, 2).ToList();
                var actual01 = sut.GetCountry(mapper).ToList();
                
                //Assert
                Assert.AreNotEqual(null, actual);
                Assert.AreEqual(1, actual01.Count);
                Assert.AreEqual(false, actual01[0].IsDeleted);
            }
        }
    }
}
