﻿using BeerOverflow.Data.Entities;
using BeerOverflow.Services.Services;
using BeerOverFlowData;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Tests.ReviewService_Should
{
    [TestClass]
    public class LikeReview_Should
    {
        [TestMethod]
        public void Success_LikeForFirstTime()
        {
            var options = Utility.GetOptions(nameof(Success_LikeForFirstTime));
            var contextConfig = new BeerOverFlowContext(options);
            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                ContextSet(arrangeContext);
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new ReviewService(assertContext);
                sut.LikeReview(1,1);
                var review = assertContext.Reviews.Include(r => r.Votes).FirstOrDefault(r => r.ReviewId == 1);

                Assert.AreEqual(review.Votes.Count, 1);
                Assert.AreEqual(review.PositiveVotes, 1);
            }
        }
        [TestMethod]
        public void Fail_AlreadyLiked()
        {
            var options = Utility.GetOptions(nameof(Fail_AlreadyLiked));
            var contextConfig = new BeerOverFlowContext(options);
            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                ContextSet(arrangeContext);
                var service = new ReviewService(arrangeContext);
                service.LikeReview(1, 1);
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new ReviewService(assertContext);
                sut.LikeReview(1, 1);
                var review = assertContext.Reviews.Include(r => r.Votes).FirstOrDefault(r => r.ReviewId == 1);

                Assert.AreEqual(review.Votes.Count, 1);
                Assert.AreEqual(review.PositiveVotes, 1);
            }
        }
        private void ContextSet(BeerOverFlowContext arrangeContext)
        {
            arrangeContext.Users.Add(new User()
            {
                UserName = "dogokun",
                Description = "The best dog ever :D",
                PasswordHash = "dogidogi",
                Email = "dogi@gmail.com",
                CountryId = 1,
                CreatedOn = DateTime.Now
            });
            arrangeContext.Beers.Add(new Beer()
            {
                BeerName = "Ariana",
                CountryId = 1,
                Rating = 5,
                BreweryId = 1,
                IsDeleted = false
            });
            arrangeContext.SaveChanges();
            var service = new ReviewService(arrangeContext);
            service.PostReview("Default", 1, 1);
        }
    }
}
