﻿using BeerOverflow.Data.Entities;
using BeerOverflow.Services.Services;
using BeerOverFlowData;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Tests.ReviewService_Should
{
    [TestClass]
    public class DeleteReview_Should
    {
        [TestMethod]
        public void Success_DeleteSuccessfully()
        {
            var options = Utility.GetOptions(nameof(Success_DeleteSuccessfully));
            var contextConfig = new BeerOverFlowContext(options);
            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                ContextSet(arrangeContext);
                var service = new ReviewService(arrangeContext);
                service.PostReview("Default", 1, 1);
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new ReviewService(assertContext);
                sut.DeleteReview(1);

                var user = assertContext.Users.Include(u => u.Reviews).FirstOrDefault(u => u.Id == 1);
                Assert.AreEqual(user.Reviews.Count, 1);
                Assert.AreEqual(user.Reviews.FirstOrDefault(r => r.ReviewId == 1).IsDeleted, true);
            }
        }
        private void ContextSet(BeerOverFlowContext arrangeContext)
        {
            arrangeContext.Users.Add(new User()
            {
                UserName = "dogokun",
                Description = "The best dog ever :D",
                PasswordHash = "dogidogi",
                Email = "dogi@gmail.com",
                CountryId = 1,
                CreatedOn = DateTime.Now
            });
            arrangeContext.Beers.Add(new Beer()
            {
                BeerName = "Ariana",
                CountryId = 1,
                Rating = 5,
                BreweryId = 1,
                IsDeleted = false
            });
            arrangeContext.SaveChanges();
        }
    }
}
