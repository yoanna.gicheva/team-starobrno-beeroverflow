﻿using BeerOverflow.Data.Entities;
using BeerOverflow.Services.Services;
using BeerOverFlowData;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Tests.ReviewService_Should
{
    [TestClass]
    public class FlagReview_Should
    {
        [TestMethod]
        public void Success_Flag()
        {
            var options = Utility.GetOptions(nameof(Success_Flag));
            var contextConfig = new BeerOverFlowContext(options);
            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                ContextSet(arrangeContext);
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new ReviewService(assertContext);
                var result = sut.FlagReview(1);

                Assert.IsTrue(result);
                Assert.IsTrue(assertContext.Reviews.FirstOrDefault(r => r.ReviewId == 1).Flagged);
            }
        }
        [TestMethod]
        public void Success_NoSuchReview()
        {
            var options = Utility.GetOptions(nameof(Success_NoSuchReview));
            var contextConfig = new BeerOverFlowContext(options);
            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                ContextSet(arrangeContext);
                var service = new ReviewService(arrangeContext);
                
                service.FlagReview(1);
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new ReviewService(assertContext);
                var result = sut.FlagReview(1);

                Assert.IsTrue(result);
                Assert.IsTrue(assertContext.Reviews.FirstOrDefault(r => r.ReviewId == 1).Flagged);
            }
        }
        private void ContextSet(BeerOverFlowContext arrangeContext)
        {
            arrangeContext.Users.Add(new User()
            {
                UserName = "dogokun",
                Description = "The best dog ever :D",
                PasswordHash = "dogidogi",
                Email = "dogi@gmail.com",
                CountryId = 1,
                CreatedOn = DateTime.Now
            });
            arrangeContext.Beers.Add(new Beer()
            {
                BeerName = "Ariana",
                CountryId = 1,
                Rating = 5,
                BreweryId = 1,
                IsDeleted = false
            });
            arrangeContext.SaveChanges();
            var service = new ReviewService(arrangeContext);
            service.PostReview("Default", 1, 1);
        }
    }
    //public bool FlagReview(int reviewId)
    //{
    //    var review = context.Reviews.FirstOrDefault(u => u.ReviewId == reviewId);
    //    if (review == null)
    //        return false;
    //    review.Flagged = true;
    //    context.SaveChanges();
    //    return true;
    //}
}
