﻿using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverFlow.AutoMapperAndProfiles;
using BeerOverFlow.Services.ModelsDTo;
using BeerOverFlow.Services.Services;
using BeerOverFlowData;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Tests.BeersServices_Should
{
    [TestClass]
    public class Delete_Should
    {
        [TestMethod]
        public void Return_TrueWithCorrectData()
        {
            var options = Utility.GetOptions(nameof(Return_TrueWithCorrectData));

            using (var contextConfig = new BeerOverFlowContext(options))
            {
                contextConfig.Countries.Add(new Country()
                {
                    CountryId = 1,
                    CountryName = "Bulgaria",
                    IsDeleted = false
                });

                 contextConfig.SaveChanges();

                contextConfig.Styles.Add( new Style()
                {
                    StyleId = 1,
                    StyleName = "Ale"
                });
                contextConfig.SaveChanges();
                var br = new Brewery
                {
                    BreweryId = 1,
                    BreweryName = "Shumensko Brewery",
                    Description = "N/A",
                    IsDeleted = false,
                    Country = contextConfig.Countries.FirstOrDefault()
                };            
                
               contextConfig.Breweries.Add(br);
                contextConfig.SaveChanges();

                contextConfig.Beers.Add(new Beer()
                {
                    BeerId = 1,
                    BeerName = "TestBeer",
                    Brewery = contextConfig.Breweries.FirstOrDefault(),
                    Country = contextConfig.Countries.FirstOrDefault(),
                    Style = contextConfig.Styles.First(),
                    IsDeleted = false
                });

                contextConfig.SaveChangesAsync();

                using (var assertContext = new BeerOverFlowContext(options))
                {
                    var myProfile = new AutoMapperConfiguration();
                    var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
                    var mapper = new Mapper(configuration);
                    var sut = new BeerService(assertContext);
                    var actual = sut.Delete(1);

                    Assert.AreEqual(true,actual);
                    Assert.AreEqual(true, assertContext.Beers.First().IsDeleted);                    
                }
            }
        }

        [TestMethod]
        public void Return_FalseWhenBeerIsAlreadyDeleted()
        {
            var options = Utility.GetOptions(nameof(Return_FalseWhenBeerIsAlreadyDeleted));

            using (var contextConfig = new BeerOverFlowContext(options))
            {
                contextConfig.Countries.Add(new Country()
                {
                    CountryId = 1,
                    CountryName = "Bulgaria",
                    IsDeleted = false
                });

                contextConfig.SaveChanges();

                contextConfig.Styles.Add(new Style()
                {
                    StyleId = 1,
                    StyleName = "Ale"
                });
                contextConfig.SaveChanges();
                var br = new Brewery
                {
                    BreweryId = 1,
                    BreweryName = "Shumensko Brewery",
                    Description = "N/A",
                    IsDeleted = false,
                    Country = contextConfig.Countries.FirstOrDefault()
                };

                contextConfig.Breweries.Add(br);
                contextConfig.SaveChanges();

                contextConfig.Beers.Add(new Beer()
                {
                    BeerId = 1,
                    BeerName = "TestBeer",
                    Brewery = contextConfig.Breweries.FirstOrDefault(),
                    Country = contextConfig.Countries.FirstOrDefault(),
                    Style = contextConfig.Styles.First(),
                    IsDeleted = true
                });

                contextConfig.SaveChangesAsync();

                using (var assertContext = new BeerOverFlowContext(options))
                {
                    var myProfile = new AutoMapperConfiguration();
                    var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
                    var mapper = new Mapper(configuration);
                    var sut = new BeerService(assertContext);
                    var actual = sut.Delete(1);
                    var actual01 = sut.Delete(-1);
                    Assert.AreEqual(false, actual);
                    Assert.AreEqual(true, assertContext.Beers.First().IsDeleted);
                    Assert.AreEqual(false, actual01);
                }
            }
        }

    }
}
