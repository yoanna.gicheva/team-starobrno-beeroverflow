﻿using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverFlow.AutoMapperAndProfiles;
using BeerOverFlow.Services.ModelsDTo;
using BeerOverFlow.Services.Services;
using BeerOverFlowData;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace BeerOverflow.Tests.BeersServices_Should
{
    [TestClass]
    public class Update_Should
    {
        [TestMethod]
        public void Update_WithCorectData()
        {
            var options = Utility.GetOptions(nameof(Update_WithCorectData));


            using (var contextConfig = new BeerOverFlowContext(options))
            {

                contextConfig.Countries.Add(new Country()
                {
                    CountryId = 1,
                    CountryName = "Bulgaria",
                    IsDeleted = false
                });
                contextConfig.SaveChanges();
                var br = new Brewery
                {
                    BreweryId = 1,
                    BreweryName = "Sut1",
                    Description = "N/A",
                    IsDeleted = false,
                    Country = contextConfig.Countries.FirstOrDefault()
                };
                var st = new Style
                {
                    StyleId = 1,
                    StyleName = "Ale"
                };
                contextConfig.Styles.Add(st);
                contextConfig.Breweries.Add(br);
                contextConfig.SaveChanges();
                contextConfig.Beers.Add(new Beer()
                {
                    BeerId = 1,
                    BeerName = "TestBeer",
                    Brewery = contextConfig.Breweries.FirstOrDefault(),
                    Country = contextConfig.Countries.FirstOrDefault(),
                    Style = contextConfig.Styles.First(),
                    IsDeleted = false
                });
                contextConfig.SaveChanges();
                br.Beers.Add(contextConfig.Beers.FirstOrDefault());
                contextConfig.SaveChanges();
            }

            using (var assertContext = new BeerOverFlowContext(options))
            {
                var myProfile = new AutoMapperConfiguration();
                var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
                var mapper = new Mapper(configuration);

                var expected = new BeerDTO()
                {
                    BeerId = 1,
                    BeerName = "1",
                    Country = mapper.Map<CountryDTO>(assertContext.Countries.First()),
                    Style = mapper.Map<StyleDTO>(assertContext.Styles.First()),
                    Brewery = mapper.Map<BreweryDTO>(assertContext.Breweries.First()),
                    Abv = 0
                }; 

                var sut = new BeerService(assertContext);
                
                var actual = sut.Update(1,expected,mapper);

                Assert.AreEqual(expected.BeerId, actual.BeerId);
                Assert.AreEqual(expected.BeerName, actual.BeerName);
                Assert.AreEqual(expected.Brewery.BreweryName, actual.Brewery.BreweryName);
                Assert.AreEqual(expected.Country.CountryName, actual.Country.CountryName);
                Assert.AreEqual(expected.Style.StyleName, actual.Style.StyleName);
                Assert.AreEqual(actual.BeerName, assertContext.Beers.First().BeerName);
            }
        }
        [TestMethod]
        public void NotUpdate_WithMissingObjectCorectData()
        {
            var options = Utility.GetOptions(nameof(NotUpdate_WithMissingObjectCorectData));


            using (var contextConfig = new BeerOverFlowContext(options))
            {

                contextConfig.Countries.Add(new Country()
                {
                    CountryId = 1,
                    CountryName = "Bulgaria",
                    IsDeleted = false
                });
                contextConfig.SaveChanges();
                var br = new Brewery
                {
                    BreweryId = 1,
                    BreweryName = "Sut1",
                    Description = "N/A",
                    IsDeleted = false,
                    Country = contextConfig.Countries.FirstOrDefault()
                };
                //var st = new Style
                //{
                //    StyleId = 1,
                //    StyleName = "Ale"
                //};
                //contextConfig.Styles.Add(st);
                contextConfig.Breweries.Add(br);
                contextConfig.SaveChanges();
                contextConfig.Beers.Add(new Beer()
                {
                    BeerId = 1,
                    BeerName = "TestBeer",
                    Brewery = contextConfig.Breweries.FirstOrDefault(),
                    Country = contextConfig.Countries.FirstOrDefault(),                   
                    IsDeleted = false
                });
                contextConfig.SaveChanges();
                br.Beers.Add(contextConfig.Beers.FirstOrDefault());
                contextConfig.SaveChanges();
            }

            using (var assertContext = new BeerOverFlowContext(options))
            {
                var myProfile = new AutoMapperConfiguration();
                var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
                var mapper = new Mapper(configuration);

                var expected = new BeerDTO()
                {
                    BeerId = 1,
                    BeerName = "1",
                    Country = mapper.Map<CountryDTO>(assertContext.Countries.First()),
                   
                    Brewery = mapper.Map<BreweryDTO>(assertContext.Breweries.First()),
                    Abv = 0
                };

                var sut = new BeerService(assertContext);

                var actual = sut.Update(1, expected, mapper);

                Assert.AreEqual(null, actual);
                Assert.AreNotEqual(assertContext.Beers.First().BeerName, expected.BeerName);
               
            }
        }


    }
}
