﻿using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverFlow.AutoMapperAndProfiles;
using BeerOverFlow.Services.ModelsDTo;
using BeerOverFlow.Services.Services;
using BeerOverFlowData;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Tests.BeersServices_Should.Sort
{
    [TestClass]
    public class SortByName_Should
    {
        [TestMethod]
        public void Return_CorrectResults()
        {
            var options = Utility.GetOptions(nameof(Return_CorrectResults));


            using (var contextConfig = new BeerOverFlowContext(options))
            {
                contextConfig.Countries.Add(new Country()
                {
                    CountryId = 1,
                    CountryName = "Bulgaria",
                    IsDeleted = false
                });
                contextConfig.Styles.Add(new Style { StyleId = 1, StyleName = "Ale" });
                contextConfig.SaveChanges();
                var br = new Brewery
                {
                    BreweryId = 1,
                    BreweryName = "Sut1",
                    Description = "N/A",
                    IsDeleted = false,
                    Country = contextConfig.Countries.FirstOrDefault()
                };
                contextConfig.Breweries.Add(br);
                contextConfig.SaveChanges();
                contextConfig.Beers.Add(new Beer
                {
                    BeerId = 1,
                    BeerName = "Alpha",
                    Brewery = contextConfig.Breweries.First(),
                    Country = contextConfig.Countries.FirstOrDefault(),
                    Style = contextConfig.Styles.FirstOrDefault()

                });
                contextConfig.Beers.Add(new Beer
                {
                    BeerId = 2,
                    BeerName = "Gamma",
                    Brewery = contextConfig.Breweries.First(),
                    Country = contextConfig.Countries.FirstOrDefault(),
                    Style = contextConfig.Styles.FirstOrDefault()

                }); contextConfig.Beers.Add(new Beer
                {
                    BeerId = 3,
                    BeerName = "Betta",
                    Brewery = contextConfig.Breweries.First(),
                    Country = contextConfig.Countries.FirstOrDefault(),
                    Style = contextConfig.Styles.FirstOrDefault()

                });

                contextConfig.SaveChanges();
            }

            using (var assertContext = new BeerOverFlowContext(options))
            {
                var myProfile = new AutoMapperConfiguration();
                var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
                var mapper = new Mapper(configuration);
                var sut = new BeerService(assertContext);
                var expected = mapper.Map<ICollection<BeerDTO>>(assertContext.Beers.ToList());
                var actual = sut.SortByName(mapper);

                Assert.AreEqual(expected.Count, actual.Count);
                Assert.AreEqual(expected.FirstOrDefault().BeerName, actual.LastOrDefault().BeerName);
                Assert.AreEqual(expected.Skip(1).FirstOrDefault().BeerName, actual.FirstOrDefault().BeerName);
            }
        }
        [TestMethod]
        public void Return_Null()
        {
            var options = Utility.GetOptions(nameof(Return_Null));


            using (var contextConfig = new BeerOverFlowContext(options))
            {
                contextConfig.Countries.Add(new Country()
                {
                    CountryId = 1,
                    CountryName = "Bulgaria",
                    IsDeleted = false
                });
                contextConfig.Styles.Add(new Style { StyleId = 1, StyleName = "Ale" });
                contextConfig.SaveChanges();
                var br = new Brewery
                {
                    BreweryId = 1,
                    BreweryName = "Sut1",
                    Description = "N/A",
                    IsDeleted = false,
                    Country = contextConfig.Countries.FirstOrDefault()
                };
                contextConfig.Breweries.Add(br);
                contextConfig.SaveChanges();
                contextConfig.Beers.Add(new Beer
                {
                    BeerId = 1,
                    BeerName = "Alpha",
                    Brewery = contextConfig.Breweries.First(),
                    Country = contextConfig.Countries.FirstOrDefault(),
                    Style = contextConfig.Styles.FirstOrDefault(),
                    IsDeleted = true

                });
                contextConfig.Beers.Add(new Beer
                {
                    BeerId = 2,
                    BeerName = "Gamma",
                    Brewery = contextConfig.Breweries.First(),
                    Country = contextConfig.Countries.FirstOrDefault(),
                    Style = contextConfig.Styles.FirstOrDefault(),
                    IsDeleted=true

                }); contextConfig.Beers.Add(new Beer
                {
                    BeerId = 3,
                    BeerName = "Betta",
                    Brewery = contextConfig.Breweries.First(),
                    Country = contextConfig.Countries.FirstOrDefault(),
                    Style = contextConfig.Styles.FirstOrDefault(),
                    IsDeleted = true

                });

                contextConfig.SaveChanges();
            }

            using (var assertContext = new BeerOverFlowContext(options))
            {
                var myProfile = new AutoMapperConfiguration();
                var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
                var mapper = new Mapper(configuration);
                var sut = new BeerService(assertContext);
                var actual = sut.SortByName(mapper);
                Assert.AreEqual(0, actual.Count);               
            }
        }
    }
}
