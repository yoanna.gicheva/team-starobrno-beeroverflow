﻿using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverFlow.AutoMapperAndProfiles;
using BeerOverFlow.Services.ModelsDTo;
using BeerOverFlow.Services.Services;
using BeerOverFlowData;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Tests.BeersServices_Should.Sort
{
    [TestClass]
    public class SortByAbv_Should
    {
        [TestMethod]
        public void SortCorectly()
        {
            var options = Utility.GetOptions(nameof(SortCorectly));

            using (var contextConfig = new BeerOverFlowContext(options))
            {
                contextConfig.Countries.Add(new Country()
                {
                    CountryId = 1,
                    CountryName = "Bulgaria",
                    IsDeleted = false
                });
                contextConfig.Styles.Add(new Style { StyleId = 1, StyleName = "Ale" });
                contextConfig.SaveChanges();
                var br = new Brewery
                {
                    BreweryId = 1,
                    BreweryName = "Sut1",
                    Description = "N/A",
                    IsDeleted = false,
                    Country = contextConfig.Countries.FirstOrDefault()
                };
                contextConfig.Breweries.Add(br);
                contextConfig.SaveChanges();
                contextConfig.Beers.Add(new Beer
                {
                    BeerId = 1,
                    BeerName = "Alpha",
                    Brewery = contextConfig.Breweries.First(),
                    Country = contextConfig.Countries.FirstOrDefault(),
                    Style = contextConfig.Styles.FirstOrDefault(),
                    Abv =2.3

                });
                contextConfig.Beers.Add(new Beer
                {
                    BeerId = 2,
                    BeerName = "Gamma",
                    Brewery = contextConfig.Breweries.First(),
                    Country = contextConfig.Countries.FirstOrDefault(),
                    Style = contextConfig.Styles.FirstOrDefault(),
                    Abv = 7

                }); contextConfig.Beers.Add(new Beer
                {
                    BeerId = 3,
                    BeerName = "Betta",
                    Brewery = contextConfig.Breweries.First(),
                    Country = contextConfig.Countries.FirstOrDefault(),
                    Style = contextConfig.Styles.FirstOrDefault(),
                    Abv = 5

                });

                contextConfig.SaveChanges();
            }

            using (var assertContext = new BeerOverFlowContext(options))
            {
                var myProfile = new AutoMapperConfiguration();
                var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
                var mapper = new Mapper(configuration);
                var sut = new BeerService(assertContext);
                var expected = mapper.Map<ICollection<BeerDTO>>(assertContext.Beers.ToList());
                var actual = sut.SortByABV(mapper);

                Assert.AreEqual(expected.Count, actual.Count);
                Assert.AreEqual(expected.Count, actual.Count);
                Assert.AreEqual(2.3, actual.FirstOrDefault().Abv);
                Assert.AreEqual(5.00, actual.Skip(1).FirstOrDefault().Abv );
                Assert.AreEqual(7.00, actual.LastOrDefault().Abv);
            }
        }
        [TestMethod]
        public void ReturnZero()
        {
            var options = Utility.GetOptions(nameof(ReturnZero));


            using (var contextConfig = new BeerOverFlowContext(options))
            {
                contextConfig.Countries.Add(new Country()
                {
                    CountryId = 1,
                    CountryName = "Bulgaria",
                    IsDeleted = false
                });
                contextConfig.Styles.Add(new Style { StyleId = 1, StyleName = "Ale" });
                contextConfig.SaveChanges();
                var br = new Brewery
                {
                    BreweryId = 1,
                    BreweryName = "Sut1",
                    Description = "N/A",
                    IsDeleted = false,
                    Country = contextConfig.Countries.FirstOrDefault()
                };
                contextConfig.Breweries.Add(br);
                contextConfig.SaveChanges();
                contextConfig.Beers.Add(new Beer
                {
                    BeerId = 1,
                    BeerName = "Alpha",
                    Brewery = contextConfig.Breweries.First(),
                    Country = contextConfig.Countries.FirstOrDefault(),
                    Style = contextConfig.Styles.FirstOrDefault(),
                    Abv = 2.3,
                    IsDeleted=true

                });
                contextConfig.Beers.Add(new Beer
                {
                    BeerId = 2,
                    BeerName = "Gamma",
                    Brewery = contextConfig.Breweries.First(),
                    Country = contextConfig.Countries.FirstOrDefault(),
                    Style = contextConfig.Styles.FirstOrDefault(),
                    Abv = 7,
                    IsDeleted = true

                }); contextConfig.Beers.Add(new Beer
                {
                    BeerId = 3,
                    BeerName = "Betta",
                    Brewery = contextConfig.Breweries.First(),
                    Country = contextConfig.Countries.FirstOrDefault(),
                    Style = contextConfig.Styles.FirstOrDefault(),
                    Abv = 5,
                    IsDeleted = true

                });

                contextConfig.SaveChanges();
            }

            using (var assertContext = new BeerOverFlowContext(options))
            {
                var myProfile = new AutoMapperConfiguration();
                var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
                var mapper = new Mapper(configuration);
                var sut = new BeerService(assertContext);
                var expected = mapper.Map<ICollection<BeerDTO>>(assertContext.Beers.ToList());
                var actual = sut.SortByABV(mapper);
                Assert.AreEqual(0, actual.Count);               
            }
        }
    }
}
