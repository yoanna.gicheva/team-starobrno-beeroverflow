﻿using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverFlow.AutoMapperAndProfiles;
using BeerOverFlow.Services.ModelsDTo;
using BeerOverFlow.Services.Services;
using BeerOverFlowData;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Tests.BeersServices_Should
{
    [TestClass]
    public class GetById_Should
    {
        [TestMethod]
        public void Get_CorrectItem()
        {

            var options = Utility.GetOptions(nameof(Get_CorrectItem));


            using (var contextConfig = new BeerOverFlowContext(options))
            {
                contextConfig.Countries.Add(new Country()
                {
                    CountryId = 1,
                    CountryName = "Bulgaria",
                    IsDeleted = false
                });
                contextConfig.Styles.Add(new Style { StyleId = 1, StyleName = "Ale" });
                contextConfig.SaveChanges();
                var br = new Brewery
                {
                    BreweryId = 1,
                    BreweryName = "Sut1",
                    Description = "N/A",
                    IsDeleted = false,
                    Country = contextConfig.Countries.FirstOrDefault()
                };
                contextConfig.Breweries.Add(br);
                contextConfig.SaveChanges();
                contextConfig.Beers.Add(new Beer
                {
                    BeerId = 1,
                    BeerName = "Shumensko",
                    Brewery = contextConfig.Breweries.First(),
                    Country = contextConfig.Countries.FirstOrDefault(),
                    Style = contextConfig.Styles.FirstOrDefault()

                });
                contextConfig.Beers.Add(new Beer
                {
                    BeerId = 2,
                    BeerName = "Pirinsko",
                    Brewery = contextConfig.Breweries.First(),
                    Country = contextConfig.Countries.FirstOrDefault(),
                    Style = contextConfig.Styles.FirstOrDefault()

                }); contextConfig.Beers.Add(new Beer
                {
                    BeerId = 3,
                    BeerName = "Tuborg",
                    Brewery = contextConfig.Breweries.First(),
                    Country = contextConfig.Countries.FirstOrDefault(),
                    Style = contextConfig.Styles.FirstOrDefault()

                });

                contextConfig.SaveChanges();
            }

            using (var assertContext = new BeerOverFlowContext(options))
            {
                var myProfile = new AutoMapperConfiguration();
                var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
                var mapper = new Mapper(configuration);
                var sut = new BeerService(assertContext);
                var expected = new Beer
                {
                    BeerId = 2,
                    BeerName = "Pirinsko",
                    Brewery = assertContext.Breweries.First(),
                    Country = assertContext.Countries.FirstOrDefault(),
                    Style = assertContext.Styles.FirstOrDefault()

                };
                var actual = sut.GetById(2,mapper);
                Assert.AreEqual(expected.BeerId, actual.BeerId);
                Assert.AreEqual(expected.BeerName, actual.BeerName);
                Assert.AreEqual(expected.Country.CountryId, actual.Country.CountryId);
                Assert.AreEqual(expected.Country.CountryName, actual.Country.CountryName);
                Assert.AreEqual(expected.Style.StyleName, actual.Style.StyleName);
                Assert.AreEqual(expected.Style.StyleId, actual.Style.StyleId);
                Assert.AreEqual(expected.Brewery.BreweryName, actual.Brewery.BreweryName);
            }
        }
        [TestMethod]
        public void ReturnNull()
        {

            var options = Utility.GetOptions(nameof(ReturnNull));


            using (var contextConfig = new BeerOverFlowContext(options))
            {
                contextConfig.Countries.Add(new Country()
                {
                    CountryId = 1,
                    CountryName = "Bulgaria",
                    IsDeleted = false
                });
                contextConfig.Styles.Add(new Style { StyleId = 1, StyleName = "Ale" });
                contextConfig.SaveChanges();
                var br = new Brewery
                {
                    BreweryId = 1,
                    BreweryName = "Sut1",
                    Description = "N/A",
                    IsDeleted = false,
                    Country = contextConfig.Countries.FirstOrDefault()
                };
                contextConfig.Breweries.Add(br);
                contextConfig.SaveChanges();
                contextConfig.Beers.Add(new Beer
                {
                    BeerId = 1,
                    BeerName = "Shumensko",
                    Brewery = contextConfig.Breweries.First(),
                    Country = contextConfig.Countries.FirstOrDefault(),
                    Style = contextConfig.Styles.FirstOrDefault()

                });
                contextConfig.Beers.Add(new Beer
                {
                    BeerId = 2,
                    BeerName = "Pirinsko",
                    Brewery = contextConfig.Breweries.First(),
                    Country = contextConfig.Countries.FirstOrDefault(),
                    Style = contextConfig.Styles.FirstOrDefault(),
                    IsDeleted = true

                }); contextConfig.Beers.Add(new Beer
                {
                    BeerId = 3,
                    BeerName = "Tuborg",
                    Brewery = contextConfig.Breweries.First(),
                    Country = contextConfig.Countries.FirstOrDefault(),
                    Style = contextConfig.Styles.FirstOrDefault()

                });

                contextConfig.SaveChanges();
            }

            using (var assertContext = new BeerOverFlowContext(options))
            {
                var myProfile = new AutoMapperConfiguration();
                var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
                var mapper = new Mapper(configuration);
                var sut = new BeerService(assertContext);
                var actual2 = sut.GetById(-123, mapper);
                var actual = sut.GetById(2, mapper);

                Assert.AreEqual(null, actual);
                Assert.AreEqual(null, actual2);
            }
        }

    }
}
