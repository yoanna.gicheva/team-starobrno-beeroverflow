﻿using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverFlow.AutoMapperAndProfiles;
using BeerOverFlow.Services.ModelsDTo;
using BeerOverFlow.Services.Services;
using BeerOverFlowData;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Tests.BeersServices_Should
{
    [TestClass]
    public class CreateAsync_Should
    {
        [TestMethod]

        public async Task CreateBeer_WithCorrectData()
        {
            var options = Utility.GetOptions(nameof(CreateBeer_WithCorrectData));

            using (var contextConfig = new BeerOverFlowContext(options))
            {
                await contextConfig.Countries.AddAsync(new Country()
                {
                    CountryId = 1,
                    CountryName = "Bulgaria",
                    IsDeleted = false
                });

                await contextConfig.SaveChangesAsync();

                var st = new Style()
                {
                    StyleId = 1,
                    StyleName = "1"
                };

                var br = new Brewery
                {
                    BreweryId = 1,
                    BreweryName = "Shumensko Brewery",
                    Description = "N/A",
                    IsDeleted = false,
                    Country = contextConfig.Countries.FirstOrDefault()
                };

                await contextConfig.Styles.AddAsync(st);
                await contextConfig.Breweries.AddAsync(br);
                await contextConfig.SaveChangesAsync();

                using (var assertContext = new BeerOverFlowContext(options))
                {
                    var expected = new BeerDTO()
                    {
                        BeerId = 1,
                        BeerName = "1",
                        Country = new CountryDTO()
                        {
                            CountryId = 1,
                            CountryName = "1"
                        },
                        Style = new StyleDTO()
                        {
                            StyleId = 1,
                            StyleName = "1"
                        },
                        Brewery = new BreweryDTO()
                        {
                            BreweryId = 1,
                            BreweryName = "1"
                        }
                    };

                    var myProfile = new AutoMapperConfiguration();
                    var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
                    var mapper = new Mapper(configuration);
                    var sut = new BeerService(assertContext);
                    var actual = await sut.CreateAsync(expected, mapper);

                    Assert.AreEqual(expected.BeerName, actual.BeerName);
                    Assert.AreEqual(expected.BeerId, actual.BeerId);
                    Assert.AreEqual("Shumensko Brewery", actual.Brewery.BreweryName);
                    Assert.AreEqual(1, assertContext.Beers.Count());
                }
            }
        }

        [TestMethod]

        public async Task ShouldNotCreateBeer_WithMissingParamsCorrectData()
        {
            var options = Utility.GetOptions(nameof(ShouldNotCreateBeer_WithMissingParamsCorrectData));

            using (var contextConfig = new BeerOverFlowContext(options))
            {
                await contextConfig.Countries.AddAsync(new Country()
                {
                    CountryId = 1,
                    CountryName = "Bulgaria",
                    IsDeleted = false
                });

                await contextConfig.SaveChangesAsync();

                var st = new Style()
                {
                    StyleId = 1,
                    StyleName = "1"
                };

                var br = new Brewery
                {
                    BreweryId = 1,
                    BreweryName = "Shumensko Brewery",
                    Description = "N/A",
                    IsDeleted = false,
                    Country = contextConfig.Countries.FirstOrDefault()
                };

                await contextConfig.Styles.AddAsync(st);
                await contextConfig.Breweries.AddAsync(br);
                await contextConfig.SaveChangesAsync();

                using (var assertContext = new BeerOverFlowContext(options))
                {
                    var expected = new BeerDTO()
                    {
                        BeerId = 1,
                        BeerName = "1",
                        
                        Style = new StyleDTO()
                        {
                            StyleId = 1,
                            StyleName = "1"
                        },
                        Brewery = new BreweryDTO()
                        {
                            BreweryId = 1,
                            BreweryName = "1"
                        }
                    };

                    var myProfile = new AutoMapperConfiguration();
                    var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
                    var mapper = new Mapper(configuration);
                    var sut = new BeerService(assertContext);
                    var actual = await sut.CreateAsync(expected, mapper);

                    Assert.AreEqual(null, actual);
                    Assert.AreEqual(0,assertContext.Beers.Count());
                }
            }
        }

    }
}
