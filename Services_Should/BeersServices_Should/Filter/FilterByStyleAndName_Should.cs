﻿using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverFlow.AutoMapperAndProfiles;
using BeerOverFlow.Services.Services;
using BeerOverFlowData;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Tests.BeersServices_Should.Filter
{
    [TestClass]
    public class FilterByStyleAndName_Should
    {
        [TestMethod]
        public void ReturnCorrect_Data()
        {
            var options = Utility.GetOptions(nameof(ReturnCorrect_Data));


            using (var contextConfig = new BeerOverFlowContext(options))
            {
                contextConfig.Countries.Add(new Country()
                {
                    CountryId = 1,
                    CountryName = "Bulgaria",
                    IsDeleted = false
                });
                contextConfig.Countries.Add(new Country()
                {
                    CountryId = 2,
                    CountryName = "China",
                    IsDeleted = false
                });
                contextConfig.Countries.Add(new Country()
                {
                    CountryId = 3,
                    CountryName = "Israel",
                    IsDeleted = false
                });
                contextConfig.Styles.Add(new Style { StyleId = 1, StyleName = "Ale" });
                contextConfig.Styles.Add(new Style { StyleId = 2, StyleName = "Lagger" });
                contextConfig.Styles.Add(new Style { StyleId = 3, StyleName = "Light" });

                contextConfig.SaveChanges();
                var br = new Brewery
                {
                    BreweryId = 1,
                    BreweryName = "Sut1",
                    Description = "N/A",
                    IsDeleted = false,
                    Country = contextConfig.Countries.FirstOrDefault()
                };
                contextConfig.Breweries.Add(br);
                contextConfig.SaveChanges();
                contextConfig.Beers.Add(new Beer
                {
                    BeerId = 1,
                    BeerName = "Shumensko",
                    Brewery = contextConfig.Breweries.First(),
                    Country = contextConfig.Countries.FirstOrDefault(),
                    Style = contextConfig.Styles.FirstOrDefault()

                });
                contextConfig.Beers.Add(new Beer
                {
                    BeerId = 2,
                    BeerName = "Pirinsko",
                    Brewery = contextConfig.Breweries.First(),
                    Country = contextConfig.Countries.FirstOrDefault(),
                    Style = contextConfig.Styles.Skip(1).FirstOrDefault()

                }); contextConfig.Beers.Add(new Beer
                {
                    BeerId = 3,
                    BeerName = "Tuborg",
                    Brewery = contextConfig.Breweries.First(),
                    Country = contextConfig.Countries.Skip(1).FirstOrDefault(),
                    Style = contextConfig.Styles.Skip(2).FirstOrDefault()

                });

                contextConfig.SaveChanges();
            

                var myProfile = new AutoMapperConfiguration();
                var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
                var mapper = new Mapper(configuration);
                var sut = new BeerService(contextConfig);
                var expected = new Beer
                {
                    BeerId = 3,
                    BeerName = "Tuborg",
                    Brewery = contextConfig.Breweries.First(),
                    Country = contextConfig.Countries.Skip(1).FirstOrDefault(),
                    Style = contextConfig.Styles.Skip(1).FirstOrDefault()
                };
                var actual = sut.FilterByStyleAndCountry("Light", "China", mapper);

                Assert.AreEqual(1, actual.Count);
                Assert.AreEqual(expected.BeerName, actual.FirstOrDefault().BeerName);
                Assert.AreEqual(expected.BeerId, actual.First().BeerId);
            }
        }


        [TestMethod]
        public void ReturnZero_Data()
        {
            var options = Utility.GetOptions(nameof(ReturnZero_Data));


            using (var contextConfig = new BeerOverFlowContext(options))
            {
                contextConfig.Countries.Add(new Country()
                {
                    CountryId = 1,
                    CountryName = "Bulgaria",
                    IsDeleted = false
                });
                contextConfig.Countries.Add(new Country()
                {
                    CountryId = 2,
                    CountryName = "China",
                    IsDeleted = false
                });
                contextConfig.Countries.Add(new Country()
                {
                    CountryId = 3,
                    CountryName = "Israel",
                    IsDeleted = false
                });
                contextConfig.Styles.Add(new Style { StyleId = 1, StyleName = "Ale" });
                contextConfig.Styles.Add(new Style { StyleId = 2, StyleName = "Lagger" });
                contextConfig.Styles.Add(new Style { StyleId = 3, StyleName = "Light" });

                contextConfig.SaveChanges();
                var br = new Brewery
                {
                    BreweryId = 1,
                    BreweryName = "Sut1",
                    Description = "N/A",
                    IsDeleted = false,
                    Country = contextConfig.Countries.FirstOrDefault()
                };
                contextConfig.Breweries.Add(br);
                contextConfig.SaveChanges();
                contextConfig.Beers.Add(new Beer
                {
                    BeerId = 1,
                    BeerName = "Shumensko",
                    Brewery = contextConfig.Breweries.First(),
                    Country = contextConfig.Countries.FirstOrDefault(),
                    Style = contextConfig.Styles.FirstOrDefault()

                });
                contextConfig.Beers.Add(new Beer
                {
                    BeerId = 2,
                    BeerName = "Pirinsko",
                    Brewery = contextConfig.Breweries.First(),
                    Country = contextConfig.Countries.FirstOrDefault(),
                    Style = contextConfig.Styles.Skip(1).FirstOrDefault()

                }); contextConfig.Beers.Add(new Beer
                {
                    BeerId = 3,
                    BeerName = "Tuborg",
                    Brewery = contextConfig.Breweries.First(),
                    Country = contextConfig.Countries.Skip(1).FirstOrDefault(),
                    Style = contextConfig.Styles.Skip(2).FirstOrDefault(),
                    IsDeleted=true

                });

                contextConfig.SaveChanges();


                var myProfile = new AutoMapperConfiguration();
                var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
                var mapper = new Mapper(configuration);
                var sut = new BeerService(contextConfig);
                var expected = new Beer
                {
                    BeerId = 3,
                    BeerName = "Tuborg",
                    Brewery = contextConfig.Breweries.First(),
                    Country = contextConfig.Countries.Skip(1).FirstOrDefault(),
                    Style = contextConfig.Styles.Skip(1).FirstOrDefault()
                };
                var actual = sut.FilterByStyleAndCountry("Lagger", "China", mapper);
                var actual02 = sut.FilterByStyleAndCountry("Light", "China", mapper);

                Assert.AreEqual(0, actual.Count);
                Assert.AreEqual(0, actual02.Count);
            }
        }
    }
}
