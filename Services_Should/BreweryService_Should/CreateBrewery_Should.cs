﻿using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverFlow.AutoMapperAndProfiles;
using BeerOverFlow.Services.ModelsDTo;
using BeerOverFlow.Services.Services;
using BeerOverFlowData;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BeerOverflow.Tests.BreweryService_Should
{
    [TestClass]
    public class CreateBrewery_Should
    {
        [TestMethod]
        public void CreateBrewery_WithCorrectData_ReturnBreweryDTO()
        {
            var options = Utility.GetOptions(nameof(CreateBrewery_WithCorrectData_ReturnBreweryDTO));
            var contextConfig = new BeerOverFlowContext(options);
            var expected = new BreweryDTO
            {

                Country = new CountryDTO { CountryId = 1, CountryName = "Bulgaria" },
                BreweryId=1,
                BreweryName = "TestBrewery",    
            };
            contextConfig.Countries.Add(new Country()
            {
                CountryId = 1,
                CountryName = "Bulgaria",
                //IsDeleted = false
            });
            contextConfig.SaveChanges();

            var myProfile = new AutoMapperConfiguration();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            var mapper = new Mapper(configuration);

            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new BreweryService(assertContext);
                var actual = sut.Create(new BreweryDTO()
                {
                    BreweryName = "TestBrewery",
                    Country = new CountryDTO { CountryId = 1, CountryName = "1" }
                }, mapper);
                Assert.AreEqual(expected.Description,actual.Description);
                Assert.AreEqual(expected.BreweryId, actual.BreweryId);
                Assert.AreEqual(expected.BreweryName, actual.BreweryName);
                Assert.AreEqual(expected.Country.CountryName, actual.Country.CountryName);
                Assert.AreEqual(expected.Description, actual.Description);
                Assert.AreEqual(expected.Country.CountryName, actual.Country.CountryName);
            }
        }

        [TestMethod]
        public void CreateBrewery_Not_ReturnBreweryDTO()
        {
            var options = Utility.GetOptions(nameof(CreateBrewery_Not_ReturnBreweryDTO));
            var contextConfig = new BeerOverFlowContext(options);
            var expected = new BreweryDTO
            {
                Country = new CountryDTO { CountryId = 1, CountryName = "Bulgaria" },
                BreweryId = 1,
                BreweryName = "TestBrewery",
            };
            contextConfig.Countries.Add(new Country()
            {
                CountryId = 1,
                CountryName = "Bulgaria",
                IsDeleted = false
            });
            contextConfig.SaveChanges();

            var myProfile = new AutoMapperConfiguration();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            var mapper = new Mapper(configuration);

            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new BreweryService(assertContext);
                var test = sut.Create(new BreweryDTO()
                {
                    BreweryName = "TestBrewery",
                    Country = new CountryDTO { CountryId = 1, CountryName = "1" }
                }, mapper);
                var actual = sut.Create(new BreweryDTO()
                {
                    BreweryName = "TestBrewery",
                    Country = new CountryDTO { CountryId = 1, CountryName = "1" }
                }, mapper);                
                Assert.AreEqual(null, actual);
            }
        }
    }
}
