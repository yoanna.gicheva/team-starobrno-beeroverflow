﻿using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverFlow.AutoMapperAndProfiles;
using BeerOverFlow.Services.ModelsDTo;
using BeerOverFlow.Services.Services;
using BeerOverFlowData;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BeerOverflow.Tests.BreweryService_Should
{
    [TestClass]
    public class Update_Should
    {
        [TestMethod]
        public void Update_BreweryCorectly()
        {
            var options = Utility.GetOptions(nameof(Update_BreweryCorectly));
            var contextConfig = new BeerOverFlowContext(options);
            var expected = new BreweryDTO
            {
                Country = new CountryDTO { CountryId = 1, CountryName = "Bulgaria" },
                BreweryId = 1,
                BreweryName = "TestBrewery",
            };
            contextConfig.Countries.Add(new Country()
            {
                CountryId = 2,
                CountryName = "1",
                IsDeleted = false
            });
            var myProfile = new AutoMapperConfiguration();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            var mapper = new Mapper(configuration);
            var sut = new BreweryService(contextConfig);

            var br = new Brewery
            {
               BreweryId= 1,
               BreweryName= "Sut",
               Description = "N/A",
               Country = new Country { CountryName="Bulgaria",CountryId=1}
            };
            contextConfig.Breweries.Add(br);
            contextConfig.SaveChanges();           

            using (var assertContext = new BeerOverFlowContext(options))
            {
                var actual = sut.Update(expected, mapper);
                Assert.AreEqual(actual.Country.CountryName, expected.Country.CountryName);
                Assert.AreEqual(actual.BreweryName, expected.BreweryName);
                Assert.AreEqual(br.BreweryId, actual.BreweryId);
                Assert.AreEqual(br.Description, actual.Description);
            }
        }
        [TestMethod]
        public void NotUpdate_BreweryCorectly()
        {
            var options = Utility.GetOptions(nameof(Update_BreweryCorectly));
            var contextConfig = new BeerOverFlowContext(options);
            
         
            var myProfile = new AutoMapperConfiguration();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            var mapper = new Mapper(configuration);
            var sut = new BreweryService(contextConfig);

            using (var assertContext = new BeerOverFlowContext(options))
            {
                var expected = new BreweryDTO
                {
                    Country = new CountryDTO { CountryId = 1, CountryName = "Bulgaria" },
                    BreweryId = 1,
                    BreweryName = "TestBrewery",
                };
                var actual = sut.Update(expected, mapper);
                Assert.AreEqual(null, actual);                
            }
        }
    }
}
