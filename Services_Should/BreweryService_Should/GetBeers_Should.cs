﻿using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverFlow.AutoMapperAndProfiles;
using BeerOverFlow.Services.ModelsDTo;
using BeerOverFlow.Services.Services;
using BeerOverFlowData;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Tests.BreweryService_Should
{
    [TestClass]
    public class GetBeers_Should
    {
        [TestMethod]
        public void ReturnAllBeers()
        {
            var options = Utility.GetOptions(nameof(ReturnAllBeers));
            
            using (var contextConfig = new BeerOverFlowContext(options))
            {
                
                contextConfig.Countries.Add(new Country()
                {
                    CountryId = 1,
                    CountryName = "Bulgaria",
                    IsDeleted = false
                });

                contextConfig.SaveChanges();

                var br = new Brewery
                {
                    BreweryId = 1,
                    BreweryName = "Sut1",
                    Description = "N/A",
                    IsDeleted = false,
                    Country = contextConfig.Countries.FirstOrDefault()
                };

                contextConfig.Breweries.Add(br);

                contextConfig.SaveChanges();

                contextConfig.Beers.Add(new Beer()
                {
                    BeerId = 1,
                    BeerName = "TestBeer",                    
                    Country = contextConfig.Countries.FirstOrDefault(),
                    
                });
                contextConfig.SaveChanges();
               
            }

            using (var assertContext = new BeerOverFlowContext(options))
            {
                assertContext.Breweries.FirstOrDefault().Beers.Add(assertContext.Beers.FirstOrDefault());
                assertContext.SaveChanges();
                var myProfile = new AutoMapperConfiguration();
                var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
                var mapper = new Mapper(configuration);
                var sut = new BreweryService(assertContext);
                var actual = sut.GetBeers(1, mapper);
                
                Assert.AreEqual(1, actual.Count);
                Assert.AreEqual(assertContext.Beers.First().BeerName, actual.First().BeerName);
            }
        }

        [TestMethod]
        public void ReturnNull_IfBeerIsDeleted()
        {
            var options = Utility.GetOptions(nameof(ReturnNull_IfBeerIsDeleted));


            using (var contextConfig = new BeerOverFlowContext(options))
            {

                contextConfig.Countries.Add(new Country()
                {
                    CountryId = 1,
                    CountryName = "Bulgaria",
                    IsDeleted = false
                });
                contextConfig.SaveChanges();
                var br = new Brewery
                {
                    BreweryId = 1,
                    BreweryName = "Sut1",
                    Description = "N/A",
                    IsDeleted = false,
                    Country = contextConfig.Countries.FirstOrDefault()
                };
                contextConfig.Breweries.Add(br);
                contextConfig.SaveChanges();
                contextConfig.Beers.Add(new Beer()
                {
                    BeerId = 1,
                    BeerName = "TestBeer",
                    Brewery = br,
                    Country = contextConfig.Countries.FirstOrDefault(),
                    IsDeleted=true
                });
                contextConfig.SaveChanges();
                br.Beers.Add(contextConfig.Beers.FirstOrDefault());
                contextConfig.SaveChanges();
            }

            using (var assertContext = new BeerOverFlowContext(options))
            {
                var myProfile = new AutoMapperConfiguration();
                var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
                var mapper = new Mapper(configuration);
                var sut = new BreweryService(assertContext);
                var expected = mapper.Map<ICollection<BreweryDTO>>(assertContext.Breweries.ToList());
                var actual = sut.GetBeers(1, mapper);

                Assert.AreEqual(0, actual.Count);
                
            }
        }
    }    
}
