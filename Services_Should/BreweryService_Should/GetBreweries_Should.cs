﻿using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverFlow.AutoMapperAndProfiles;
using BeerOverFlow.Services.ModelsDTo;
using BeerOverFlow.Services.Services;
using BeerOverFlowData;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Tests.BreweryService_Should
{
    [TestClass]
    public class GetBreweries_Should
    {
        [TestMethod]
        public void GetAllBreweries()
        {
            var options = Utility.GetOptions(nameof(GetAllBreweries));           

           
            using (var contextConfig = new BeerOverFlowContext(options))
            {
                contextConfig.Countries.Add(new Country()
                {
                    CountryId = 1,
                    CountryName = "Bulgaria",
                    IsDeleted = false
                });
                contextConfig.SaveChanges();
                var br = new Brewery
                {
                    BreweryId = 1,
                    BreweryName = "Sut1",
                    Description = "N/A",
                    IsDeleted = false,
                    Country = contextConfig.Countries.FirstOrDefault()
                };
                var br01 = new Brewery
                {
                    BreweryId = 2,
                    BreweryName = "Sut2",
                    Description = "N/A",
                    IsDeleted = false,
                    Country = contextConfig.Countries.FirstOrDefault()
                }; var br02 = new Brewery
                {
                    BreweryId = 3,
                    BreweryName = "Sut3",
                    Description = "N/A",
                    IsDeleted = false,
                    Country=contextConfig.Countries.FirstOrDefault()
                }; var br03 = new Brewery
                {
                    BreweryId = 4,
                    BreweryName = "Sut4",
                    IsDeleted = false,
                    Description = "N/A",
                    Country = contextConfig.Countries.FirstOrDefault()
                };

               contextConfig.Breweries.Add(br);
               contextConfig.Breweries.Add(br01);
               contextConfig.Breweries.Add(br02);
               contextConfig.Breweries.Add(br03);
                contextConfig.SaveChanges();
            }
            
            using (var assertContext = new BeerOverFlowContext(options))            
            {
                var myProfile = new AutoMapperConfiguration();
                var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
                var mapper = new Mapper(configuration);
                var sut = new BreweryService(assertContext);
               var expected = mapper.Map<ICollection<BreweryDTO>>(assertContext.Breweries.ToList());
               var actual = sut.GetBreweries(mapper);

                Assert.AreEqual(expected.Count, actual.Count);
                Assert.AreEqual(expected.FirstOrDefault().BreweryName, expected.FirstOrDefault().BreweryName);
                Assert.AreEqual(expected.LastOrDefault().BreweryId, expected.LastOrDefault().BreweryId);
                
            }
        }
        [TestMethod]
        public void NotGetAnyBreweries()
        {
            var options = Utility.GetOptions(nameof(NotGetAnyBreweries));


            using (var contextConfig = new BeerOverFlowContext(options))
            {
                contextConfig.Countries.Add(new Country()
                {
                    CountryId = 1,
                    CountryName = "Bulgaria",
                    IsDeleted = true
                });
                var br = new Brewery
                {
                    BreweryId = 1,
                    BreweryName = "Sut1",
                    Description = "N/A",
                    IsDeleted = true,
                    Country = null
                };
                var br01 = new Brewery
                {
                    BreweryId = 2,
                    BreweryName = "Sut2",
                    Description = "N/A",
                    IsDeleted = true,
                    Country = null
                }; var br02 = new Brewery
                {
                    BreweryId = 3,
                    BreweryName = "Sut3",
                    Description = "N/A",
                    IsDeleted = true,
                    Country = null
                }; var br03 = new Brewery
                {
                    BreweryId = 4,
                    BreweryName = "Sut4",
                    IsDeleted = true,
                    Description = "N/A",
                    Country = null
                };

                contextConfig.Breweries.Add(br);
                contextConfig.Breweries.Add(br01);
                contextConfig.Breweries.Add(br02);
                contextConfig.Breweries.Add(br03);
                contextConfig.SaveChanges();
            }

            using (var assertContext = new BeerOverFlowContext(options))
            {
                var myProfile = new AutoMapperConfiguration();
                var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
                var mapper = new Mapper(configuration);
                var sut = new BreweryService(assertContext);
                var expected = mapper.Map<ICollection<BreweryDTO>>(assertContext.Breweries.ToList());
                var actual = sut.GetBreweries(mapper);
                Assert.AreEqual(0, actual.Count);
            }
        }
    }
}
