﻿using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverFlow.AutoMapperAndProfiles;
using BeerOverFlow.Services.ModelsDTo;
using BeerOverFlow.Services.Services;
using BeerOverFlowData;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Tests.StyleService_Should
{
    [TestClass]
    public class UpdateStyle_Should
    {
        [TestMethod]
        public void Success_Exists()
        {
            var myProfile = new AutoMapperConfiguration();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            var mapper = new Mapper(configuration);
            var options = Utility.GetOptions(nameof(Success_Exists));
            var contextConfig = new BeerOverFlowContext(options);

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                arrangeContext.Styles.AddRange(new Style() { StyleName = "Old ale" }, new Style() { StyleName = "Dark" });
                arrangeContext.SaveChanges();
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new StyleService(assertContext);
                StyleDTO newStyle = new StyleDTO() { StyleName = "Mixed" };
                StyleDTO result = sut.UpdateStyle(1, newStyle, mapper);
                var expectedName = "Mixed";
                var actualName = assertContext.Styles.First(s => s.StyleId == 1).StyleName;

                Assert.AreEqual(actualName, expectedName);
            }
        }

        [TestMethod]
        public void Fail_NoSuchStyle()
        {
            var myProfile = new AutoMapperConfiguration();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            var mapper = new Mapper(configuration);
            var options = Utility.GetOptions(nameof(Fail_NoSuchStyle));
            var contextConfig = new BeerOverFlowContext(options);

            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new StyleService(assertContext);
                StyleDTO actual = sut.GetStyleById(4, mapper);

                Assert.IsNull(actual);
            }
        }
    }
}
