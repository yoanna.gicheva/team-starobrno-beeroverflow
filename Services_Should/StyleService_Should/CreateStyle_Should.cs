﻿using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverFlow.AutoMapperAndProfiles;
using BeerOverFlow.Services.ModelsDTo;
using BeerOverFlow.Services.Services;
using BeerOverFlowData;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Tests.StyleService_Should
{
    [TestClass]
    public class CreateStyle_Should
    {
        [TestMethod]
        public void Succeess_Create()
        {
            var myProfile = new AutoMapperConfiguration();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            var mapper = new Mapper(configuration);
            var options = Utility.GetOptions(nameof(Succeess_Create));
            var contextConfig = new BeerOverFlowContext(options);

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                arrangeContext.Styles.Add(new Style()
                {
                    StyleName = "Old ale"
                });
                arrangeContext.SaveChanges();
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new StyleService(assertContext);
                StyleDTO actual = sut.CreateStyle(new StyleDTO { StyleName = "Dark" }, mapper);
                string expectedName = "Dark";
                string actualName = actual.StyleName;
                Assert.AreEqual(expectedName, actualName);
            }
        }
        
        [TestMethod]
        public void Fail_AlreadyExists()
        {
            var myProfile = new AutoMapperConfiguration();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            var mapper = new Mapper(configuration);
            var options = Utility.GetOptions(nameof(Succeess_Create));
            var contextConfig = new BeerOverFlowContext(options);

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                arrangeContext.Styles.Add(new Style()
                {
                    StyleName = "Old ale"
                });
                arrangeContext.SaveChanges();
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new StyleService(assertContext);
                StyleDTO actual = sut.CreateStyle(new StyleDTO { StyleName = "Old ale" }, mapper);
                string expected = null;

                Assert.AreEqual(expected, actual);
            }
        }

    }
}
