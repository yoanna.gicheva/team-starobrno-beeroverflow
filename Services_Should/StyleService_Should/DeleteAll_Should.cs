﻿using BeerOverflow.Data.Entities;
using BeerOverFlow.Services.Services;
using BeerOverFlowData;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Tests.StyleService_Should
{
    [TestClass]
    public class DeleteAll_Should
    {
        [TestMethod]
        public void Success_Exist()
        {
            var options = Utility.GetOptions(nameof(Success_Exist));
            var contextConfig = new BeerOverFlowContext(options);

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                arrangeContext.Styles.AddRange(new Style() { StyleName = "Old ale" }, new Style() { StyleName = "Dark" });
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new StyleService(assertContext);
                var result = sut.DeleteAll();
                ICollection<Style> styles = assertContext.Styles.ToList();

                Assert.AreEqual(styles.Count, 0);
                Assert.IsTrue(result);
            }
        }
        [TestMethod]
        public void Fail_NoRecords()
        {
            var options = Utility.GetOptions(nameof(Fail_NoRecords));
            var contextConfig = new BeerOverFlowContext(options);

            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new StyleService(assertContext);
                var result = sut.DeleteAll();
                ICollection<Style> styles = assertContext.Styles.ToList();

                Assert.AreEqual(styles.Count, 0);
                Assert.IsFalse(result);

            }
        }
    }
}
