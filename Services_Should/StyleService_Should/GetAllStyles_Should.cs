﻿using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverFlow.AutoMapperAndProfiles;
using BeerOverFlow.Services.ModelsDTo;
using BeerOverFlow.Services.Services;
using BeerOverFlowData;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Tests.StyleService_Should
{
    [TestClass]
    public class GetAllStyles_Should
    {
        [TestMethod]
        public void Success_AllStyles()
        {
            var myProfile = new AutoMapperConfiguration();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            var mapper = new Mapper(configuration);
            var options = Utility.GetOptions(nameof(Success_AllStyles));
            var contextConfig = new BeerOverFlowContext(options);

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                arrangeContext.Styles.AddRange(new Style() { StyleName = "Old ale" }, new Style() { StyleName = "Dark" });
                arrangeContext.SaveChanges();
            }
            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new StyleService(assertContext);
                ICollection<StyleDTO> actual = sut.GetAllStyles(mapper);

                Assert.AreEqual(actual.Count, 2);
                Assert.AreEqual(actual.First().StyleName, "Old ale");
            }
        }

        [TestMethod]
        public void Success_NoStyles()
        {
            var myProfile = new AutoMapperConfiguration();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            var mapper = new Mapper(configuration);
            var options = Utility.GetOptions(nameof(Success_NoStyles));
            var contextConfig = new BeerOverFlowContext(options);

            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new StyleService(assertContext);
                ICollection<StyleDTO> actual = sut.GetAllStyles(mapper);

                Assert.AreEqual(actual.Count, 0);
            }
        }
    }
}
