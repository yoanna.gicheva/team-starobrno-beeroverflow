﻿using BeerOverflow.Data.Entities;
using BeerOverFlow.Services.Services;
using BeerOverFlowData;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Tests.StyleService_Should
{
    [TestClass]
    public class DeleteById_Should
    {
        [TestMethod]
        public void Success_DeleteById()
        {
            var options = Utility.GetOptions(nameof(Success_DeleteById));
            var contextConfig = new BeerOverFlowContext(options);

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                arrangeContext.Styles.AddRange(new Style() { StyleName = "Old ale" }, new Style() { StyleName = "Dark" });
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new StyleService(assertContext);
                sut.DeleteById(1);
                ICollection<Style> styles = assertContext.Styles.ToList();
                string exptectName = styles.First().StyleName;

                Assert.AreEqual(styles.Count, 1);
                Assert.AreEqual(exptectName, "Dark");
            }
        }
        [TestMethod]
        public void Failure_NoSuchId()
        {
            var options = Utility.GetOptions(nameof(Failure_NoSuchId));
            var contextConfig = new BeerOverFlowContext(options);

            using (var arrangeContext = new BeerOverFlowContext(options))
            {
                arrangeContext.Styles.AddRange(new Style() { StyleName = "Old ale" }, new Style() { StyleName = "Dark" });
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new BeerOverFlowContext(options))
            {
                var sut = new StyleService(assertContext);
                bool result = sut.DeleteById(4);
            
                Assert.IsFalse(result);
            }
        }
    }
}
