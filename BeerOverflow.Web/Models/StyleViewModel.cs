﻿namespace BeerOverFlow.Models
{
    public class StyleViewModel
    {
        public int StyleId { get; set; }
        public string StyleName { get; set; }
    }
}
