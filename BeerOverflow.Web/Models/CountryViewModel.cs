﻿namespace BeerOverFlow.Models
{
    public class CountryViewModel
    {
        public int CountryId { get; set; }
        public string CountryName { get; set; }
    }
}
