﻿using BeerOverflow.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Models.UserViewModels
{
    public class ListUserViewModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string CountryName { get; set; }
        public Ban Ban { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime LastAccessedOn { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }

    }
}
