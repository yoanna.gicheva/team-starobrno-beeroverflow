﻿using BeerOverflow.Data.Entities.Lists;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Models.UserViewModels
{
    public class UserDetails
    {
        public string Username { get; set; }
        public string CountryName { get; set; }
        public string Email { get; set; }
        public DateTime CreatedOn { get; set; }
        public ICollection<Review> Reviews { get; set; }
        public ICollection<Wish> Wishes { get; set; }
        public ICollection<Drank> Dranks { get; set; }
    }
}
