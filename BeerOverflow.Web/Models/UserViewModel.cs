﻿namespace BeerOverFlow.Models
{
    public class UserViewModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public int CountryId { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
    }
}
