﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverFlow.Models
{
    public class BreweryViewModel
    {
        public int BreweryId { get; set; }
        public string BreweryName { get; set; }
        public string CountryName { get; set; }
        public string Description { get; set; }
    }
}
