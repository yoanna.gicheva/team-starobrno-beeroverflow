﻿namespace BeerOverFlow.Models
{
    public class BeerViewModel
    {
        public int BeerId { get; set; }
        public string BreweryName { get; set; }
        public string BeerName { get; set; }
        public string StyleName { get; set; }
        public string CountryName { get; set; }
        public double Abv { get; set; }
        public int ListId { get; set; }
        public string Image { get; set; }
    }
}
