﻿using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverflow.Services.ModelsDTo.WebModels;
using BeerOverflow.Web.Models.UserViewModels;
using BeerOverFlow.Models;
using BeerOverFlow.Services.ModelsDTo;
using BeerOverFlow.Services.ModelsDTO;

namespace BeerOverFlow.AutoMapperAndProfiles
{
    public class AutoMapperConfiguration : Profile
    {
        public AutoMapperConfiguration()
        {

            CreateMap<CountryViewModel, CountryDTO>().ReverseMap();
            CreateMap<Country, CountryDTO>();

            //CreateMap<BreweryViewModel, BreweryDTO>().
            //    ForMember(dest => dest.Country, opt => opt.MapFrom(src => new CountryDTO()
            //    {
            //        CountryName = src.CountryName
            //    }));
            CreateMap<BreweryDTO, BreweryViewModel>().
                ForMember(dest => dest.BreweryName, opt => opt.MapFrom(src => src.BreweryName)).
                ForMember(dest=>dest.BreweryId,opt=>opt.MapFrom(src=>src.BreweryId)).
                ForMember(dest => dest.CountryName, opt => opt.MapFrom(src => src.Country.CountryName)).ReverseMap();
            CreateMap<Brewery, BreweryDTO>();

            CreateMap<BeerViewModel, BeerDTO>().

                        ForMember(dest => dest.Country, opt => opt.MapFrom(src => new CountryDTO()
                        {
                            CountryName = src.CountryName
                        })).
                        ForMember(dest => dest.Style, opt => opt.MapFrom(src => new StyleDTO()
                        {
                            StyleName = src.StyleName
                        })).
                        ForMember(dest => dest.Brewery, opt => opt.MapFrom(src => new BreweryDTO()
                        {
                            BreweryName = src.BreweryName
                        }));

            CreateMap<BeerDTO, BeerViewModel>().
                ForMember(dest => dest.BeerName, opt => opt.MapFrom(src => src.BeerName)).
                 ForMember(dest => dest.Abv, opt => opt.MapFrom(src => src.Abv)).
                  ForMember(dest => dest.CountryName, opt => opt.MapFrom(src => src.Country.CountryName)).
                   ForMember(dest => dest.StyleName, opt => opt.MapFrom(src => src.Style.StyleName)).
                    ForMember(dest => dest.BreweryName, opt => opt.MapFrom(src => src.Brewery.BreweryName));


            CreateMap<Beer, BeerDTO>().
                ForMember(dest => dest.Country, opt => opt.MapFrom(src => new Country()
                {
                    CountryId = src.Country.CountryId,
                    CountryName = src.Country.CountryName

                })).
            ForMember(dest => dest.Brewery, opt => opt.MapFrom(src => new Brewery()
            {
                BreweryName = src.Brewery.BreweryName,
                Beers = src.Brewery.Beers,
                Country = src.Brewery.Country
            }
            )).ForMember(dest => dest.Style, opt => opt.MapFrom(src => new Style()
            {
                StyleId = src.Style.StyleId,
                StyleName = src.Style.StyleName
            }
            )).ReverseMap();


            CreateMap<UserReturnDTO, ListUserViewModel>();
            CreateMap<User, UserDetailsModel>();

            // STYLE
            CreateMap<StyleViewModel, StyleDTO>().ReverseMap();
            CreateMap<Style, StyleDTO>();


            this.CreateMap<UserReturnDTO, UserViewModel>();
            this.CreateMap<UserViewModel, UserReturnDTO>();
            this.CreateMap<UserViewModel, User>();
            this.CreateMap<User, UserViewModel>();


            //this.CreateMap<User, UserReturnDTO>()
            //   .ReverseMap();

            CreateMap<User, UserReturnDTO>().
               ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName)).
                ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id)).
                 ForMember(dest => dest.CountryName, opt => opt.MapFrom(src => src.Country.CountryName)).
                  ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email)).
                   ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
            .ForMember(dest => dest.Ban, opt => opt.MapFrom(src => src.Ban)).ReverseMap();




            this.CreateMap<UserViewDTO, User>();

            
            


            this.CreateMap<User, UserViewDTO>();

            this.CreateMap<UserViewDTO, UserViewModel>();
            this.CreateMap<UserViewModel, UserViewDTO>();



            CreateMap<Beer, BeerDetailsModel>();

            //    CreateMap<Beer, BeerDetailsModel>().
            //       ForMember(dest => dest.Country, opt => opt.MapFrom(src => new Country()
            //       {
            //           CountryName = src.Country.CountryName

            //       })).
            //   ForMember(dest => dest.Brewery, opt => opt.MapFrom(src => new Brewery()
            //   {
            //       BreweryName = src.Brewery.BreweryName,
            //   }
            //   )).ForMember(dest => dest.Style, opt => opt.MapFrom(src => new Style()
            //   {
            //       StyleName = src.Style.StyleName
            //   }
            //   )).ReverseMap();
            //}
        }

    }
}
