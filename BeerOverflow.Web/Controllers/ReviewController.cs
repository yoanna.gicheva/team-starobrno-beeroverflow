﻿using System;
using System.Security.Claims;
using AutoMapper;
using BeerOverflow.Services.Services.Contracts;
using BeerOverFlow.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BeerOverflow.Web.Controllers
{
    [Authorize(Roles = "Member,Admin")]
    public class ReviewController : Controller
    {
        private readonly ILogger<ReviewController> _logger;
        IReviewService service;
        IBeerService beerService;
        IUserService userService;
        IMapper mapper;

        public ReviewController(ILogger<ReviewController> logger,
            IReviewService service, IMapper mapper, IBeerService beerService, IUserService userService)
        {
            this.service = service ?? throw new ArgumentNullException(nameof(service));
            this.beerService = beerService ?? throw new ArgumentNullException(nameof(beerService));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(service));
            this.userService = userService ?? throw new ArgumentNullException(nameof(service));
            _logger = logger;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult PostReview(string reviewText, int beerId)
        {
            int userId = Convert.ToInt32(this.User.FindFirstValue(ClaimTypes.NameIdentifier));
            if(!userService.UserIsBanned(userId))
            {
                service.PostReview(reviewText, beerId, userId);
            }
            var beer = beerService.BeerDetails(beerId, mapper);
            return RedirectToAction("BeerDetails", "Beers", beer);
        }
        public IActionResult LikeReview(int id, int beerId)
        {
            int userId = Convert.ToInt32(this.User.FindFirstValue(ClaimTypes.NameIdentifier));
            service.LikeReview(id, userId);
            var beer = beerService.BeerDetails(beerId, mapper);
            return RedirectToAction("BeerDetails", "Beers", beer);
        }

        public IActionResult DislikeReview(int id, int beerId)
        {
            int userId = Convert.ToInt32(this.User.FindFirstValue(ClaimTypes.NameIdentifier));
            service.DislikeReview(id, userId);
            var beer = beerService.BeerDetails(beerId, mapper);
            return RedirectToAction("BeerDetails", "Beers", beer);
        }

        public IActionResult FlagReview(int id, int beerId)
        {
            var result = service.FlagReview(id);
            if (!result)
                return View("Error");
            var beer = beerService.BeerDetails(beerId, mapper);
            return RedirectToAction("BeerDetails", "Beers", beer);
        }

        [Authorize(Roles = "Admin")]
        public IActionResult DeleteReview(int id, string name)
        {
            service.DeleteReview(id);
            var reviewList = userService.UserReview(name);
            return View("~/Views/User/ShowReviews.cshtml", reviewList);
        }
    }
}