﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BeerOverFlow.Models;
using BeerOverFlow.Services.Contracts;
using BeerOverFlow.Services.ModelsDTo;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BeerOverFlow.Controllers
{

    public class CountryController : Controller
    {
        ICountryService service;
        IMapper mapper;
        public CountryController(ICountryService service, IMapper mapper)
        {
            this.service = service;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult ListCountries()
        {
            var res = service.GetCountry(mapper);
            return View(mapper.Map<ICollection<CountryViewModel>>(res));
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public IActionResult CreateCountry()
        {

            return View();
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public IActionResult CreateCountry(CountryViewModel country)
        {
            var res = service.Create(mapper.Map<CountryDTO>(country), mapper);
            return RedirectToAction("ListCountries", "Country");
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        [Route("{id}")]
        public IActionResult DeleteCountry(int id)
        {
            service.Delete(id);
            return RedirectToAction("ListCountries");
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public IActionResult EditCountry(int id)
        {
            var res = service.GetCountry(mapper, id).FirstOrDefault();
            if (res == null)
            {
                return NotFound();
            }
            return View(mapper.Map<CountryViewModel>(res));
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public IActionResult EditCountry(int id, string countryName)
        {
            var res = service.GetCountry(mapper, id).FirstOrDefault();
            service.Update(countryName,res, mapper);
            return RedirectToAction("ListCountries");
        }
    }
}