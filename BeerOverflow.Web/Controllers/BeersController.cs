﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using BeerOverflow.Data.Entities;
using BeerOverflow.Services.ModelsDTo.WebModels;
using BeerOverflow.Services.Services.Contracts;
using BeerOverFlow.Models;
using BeerOverFlow.Services.Contracts;
using BeerOverFlow.Services.ModelsDTo;
using MediaBrowser.Controller.Net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BeerOverflow.Web.Controllers
{

    public class BeersController : Controller
    {
        private readonly IBreweryService brService;
        private readonly ICountryService ctrCountry;
        private readonly ILogger<BeersController> _logger;
        private readonly IReviewService reviewService;
        IBeerService service;
        IMapper mapper;

        private IStyleService styleService;

        public BeersController(IBreweryService brService, IStyleService stService,
            ICountryService ctrCountry, ILogger<BeersController> logger,
            IBeerService service, IMapper mapper, IReviewService reviewService)
        {
            this.service = service ?? throw new ArgumentNullException(nameof(service));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(service));
            this.brService = brService ?? throw new ArgumentNullException(nameof(service));
            styleService = stService ?? throw new ArgumentNullException(nameof(service));
            this.reviewService = reviewService ?? throw new ArgumentNullException(nameof(reviewService));
            this.ctrCountry = ctrCountry;
            _logger = logger;            
        }

        [Authorize(Roles = "Member,Admin")]
        [HttpGet]
        public IActionResult CreateBeer()
        {
            var breweryViewModels = mapper.Map<ICollection<BreweryViewModel>>(brService.GetBreweries(mapper));
            var styleViewModels = mapper.Map<ICollection<StyleViewModel>>(styleService.GetAllStyles(mapper));
            var countryViewModels = mapper.Map<ICollection<CountryViewModel>>(ctrCountry.GetCountry(mapper));

            ViewData["BreweryList"] = new SelectList(breweryViewModels, "BreweryId", "BreweryName");

            ViewData["StyleList"] = new SelectList(styleViewModels, "StyleId", "StyleName");

            ViewData["CountryList"] = new SelectList(countryViewModels, "CountryId", "CountryName");
            return View();
        }

    
        [HttpGet]
        public IActionResult ListBeers()
        {
            var res = service.GetAll(mapper);
            var x = mapper.Map<ICollection<BeerViewModel>>(res);
            return View("ListBeer",x);
        }

        [HttpGet]
        public IActionResult ListSortedBeersBeers(ICollection<CountryDTO> final)
        {
            if (final == null)
            {
                return RedirectToAction("ListBeers");
            }
            return View("ListBeer",final);
        }

        [HttpPost]
        public IActionResult FilterByName(string searchString, string optradio)
        {
           
            switch (optradio)
            {
                case"name":
                    if (searchString==null)
                    {
                        return RedirectToAction("ListBeers");
                    }
                    var nameResult = service.FilterByName(searchString, mapper);
                    return View("ListBeer", mapper.Map<ICollection<BeerViewModel>>(nameResult));
                case "country":
                    var countryResult = service.FilterByCountry(searchString, mapper);
                    return View("ListBeer", mapper.Map<ICollection<BeerViewModel>>(countryResult));
                case "style":
                    var styleResult = service.FilterByStyle(searchString, mapper);
                    return View("ListBeer", mapper.Map<ICollection<BeerViewModel>>(styleResult));
                case "styleandcountry":
                    try
                    {
                        var holder = searchString.Split(new char[] { ' ', ',' }, StringSplitOptions.RemoveEmptyEntries);

                        if (searchString.Split().Length < 2)
                        {
                            BadRequest();
                        }

                        var styleAndCountryResult = service.FilterByStyleAndCountry(holder[0], holder[holder.Length - 1], mapper);
                        return View("ListBeer", mapper.Map<ICollection<BeerViewModel>>(styleAndCountryResult));
                    }
                    catch (Exception)
                    {

                        return RedirectToAction("ListBeers", mapper.Map<ICollection<BeerViewModel>>(default));
                    }
                   
                default:
                    return BadRequest();                   
            }
           
        }

        [HttpGet]
        public IActionResult Sort([FromQuery]string sortOption)
        {
            switch (sortOption)
            {
                case "BeerAscd":
                   var nameOrderAscd = mapper.Map < ICollection < BeerViewModel >> (service.SortByName(mapper));                    
                    return View("ListBeer", nameOrderAscd);

                case "BeerDscd":
                    var nameOrderDscd = mapper.Map<ICollection<BeerViewModel>>(service.SortByName(mapper));
                    return View("ListBeer", nameOrderDscd.Reverse());

                case "AbvAscd":
                    var abvAscd = mapper.Map<ICollection<BeerViewModel>>(service.SortByABV(mapper));
                    return View("ListBeer", abvAscd);

                case "AbvDscd":
                    var abvDscd = mapper.Map<ICollection<BeerViewModel>>(service.SortByABV(mapper));
                    return View("ListBeer", abvDscd.Reverse());
                default:
                    return BadRequest();
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public IActionResult EditBeer(int id)
        {

            var breweryViewModels = mapper.Map<ICollection<BreweryViewModel>>(brService.GetBreweries(mapper));
            var styleViewModels = mapper.Map<ICollection<StyleViewModel>>(styleService.GetAllStyles(mapper));
            var countryViewModels = mapper.Map<ICollection<CountryViewModel>>(ctrCountry.GetCountry(mapper));

            ViewData["BreweryList"] = new SelectList(breweryViewModels, "BreweryId", "BreweryName");

            ViewData["StyleList"] = new SelectList(styleViewModels, "StyleId", "StyleName");

            ViewData["CountryList"] = new SelectList(countryViewModels, "CountryId", "CountryName");

            var res = service.GetById(id, mapper);

            if (res == null)
            {
                return NotFound();
            }
            return View(mapper.Map<BeerViewModel>(res));
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public IActionResult EditBeer(int id,BeerViewModel beer)
        {
            var res = service.Update(id, mapper.Map<BeerDTO>(beer), mapper);
            return RedirectToActionPermanent("BeerDetails", new { id = res.BeerId });           
        }

        [Authorize(Roles = "Member,Admin")]
        [HttpPost]
        public async Task<IActionResult> CreateBeer(BeerViewModel beer)
        {
            var res = await service.CreateAsync(mapper.Map<BeerDTO>(beer), mapper);
            if (res!=null)
            {
                return RedirectToAction("BeerDetails", "Beers", new { id = res.BeerId });
            }
            return RedirectToAction("ListBeers");          
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public IActionResult DeleteBeer(int id)
        {
            var res = service.Delete(id);
            return RedirectToAction("ListBeers");
        }

        [HttpGet]
        public IActionResult BeerDetails(int id)
        {
            var tempVar = TempData["AfterRedirectVar"] as string;
            if (tempVar!=null)
            {
                BeerDetailsModel beer02 = service.BeerDetails(int.Parse(tempVar), mapper);
                return View(beer02);
            }
            BeerDetailsModel beer = service.BeerDetails(id, mapper);
            return View(beer);
        }

        [Authorize(Roles = "Admin,Member")]
        [HttpPost]
        public IActionResult RateBeer(int rating, int beerId)
        {
            int userId = Convert.ToInt32(this.User.FindFirstValue(ClaimTypes.NameIdentifier));



            service.RateIdenticalBeer(userId, beerId, rating, mapper);



            var beer = service.BeerDetails(beerId, mapper);
            return RedirectToAction("BeerDetails", beer);
        }
      
    }
}
