﻿using AutoMapper;
using BeerOverFlow.Models;
using BeerOverFlow.Services.Contracts;
using BeerOverFlow.Services.ModelsDTo;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace BeerOverFlow.Controllers.ApiControllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class CountryController : ControllerBase
    {
        private readonly ICountryService service;
        IMapper mapper;

        public CountryController(ICountryService service, IMapper mapper)
        {
            this.service = service ?? throw new ArgumentNullException(nameof(service));
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var result = service.GetCountry(mapper);
            if (result == null)
            {
                return BadRequest();
            }
            var res = mapper.Map<ICollection<CountryViewModel>>(result);
            return Ok(res);
        }
        [HttpGet]
        [Route("{id}")]

        public IActionResult Get(int id)
        {
            var result = service.GetCountry(mapper, id);
            if (result == null)
            {
                return BadRequest(id);
            }
            return Ok(result);
        }

        [HttpPost]
        public IActionResult Post(CountryViewModel country)
        {
            var tempDTO = mapper.Map<CountryDTO>(country);
            var finalResult = service.Create(tempDTO, mapper);
            if (finalResult != null)
            {
                return Ok(finalResult);
            }
            return BadRequest(country);
        }
        [HttpPut]
        [Route("{id}")]
        public IActionResult Put(CountryViewModel country, int id)
        {
            //TODO
            var holder = this.mapper.Map<CountryDTO>(country);
            holder.CountryId = id;
            var inst = service.Update(country.CountryName, holder, mapper);
            if (inst != null)
            {
                return Ok(inst);
            }
            return BadRequest(holder);
        }

        [HttpDelete]
        [Route("{id}")]

        public IActionResult Delete(int id)
        {
            if (service.Delete(id))
            {
                return Ok("Country was deleted");
            }
            return BadRequest();
        }
    }

}