﻿using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using System;
using BeerOverFlow.Models;
using System.Collections.Generic;
using BeerOverFlow.Services.ModelsDTo;
using BeerOverFlow.Services.Contracts;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace BeerOverFlow.Controllers.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BeerController : ControllerBase
    {
        IBeerService service;
        IMapper mapper;
        public BeerController(IBeerService service, IMapper mapper)
        {
            this.service = service ?? throw new ArgumentNullException(nameof(service));
            this.mapper = mapper;
        }

        [HttpGet]
        [Route("filter")]

        public IActionResult Filter([FromQuery]string country, [FromQuery] string style)
        {
            if (country != null)
            {
                if (style != null)
                {
                    //https://localhost:44353/api/beer/filter?country=Poland&style=Ale
                    var res = service.FilterByStyleAndCountry(country, style, mapper);
                    var final = mapper.Map<ICollection<BeerViewModel>>(res);
                    return Ok(final);
                }
                var resCountry = service.FilterByCountry(country, mapper);
                var finalCountries = mapper.Map<ICollection<BeerViewModel>>(resCountry);
                return Ok(finalCountries);
            }
            else if (style != null)
            {
                var res = service.FilterByStyle(style, mapper);
                var final = mapper.Map<ICollection<BeerViewModel>>(res);
                return Ok(final);
            }
            return BadRequest();
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult Get(int id)
        {
            var result = service.GetById(id, mapper);
            if (result == null)
                return NotFound($"No such beer with Id: {id}");
            string json = JsonConvert.SerializeObject(result, Formatting.Indented);
            return Ok(json);
        }

        [HttpGet]
        public IActionResult Get()
        {
            var serv = service.GetAll(mapper);
            if (serv != null)
            {
                string json = JsonConvert.SerializeObject(serv, Formatting.Indented);
                return Ok(json);
            }
            return NotFound("No beers in the Database");
        }

        [HttpPost]
        [Route("")]
        public IActionResult Post(BeerViewModel brVM)
        {
            var beerDto = mapper.Map<BeerDTO>(brVM);
            var res = service.CreateAsync(beerDto, mapper);
            if (res != null)
            {
                string json = JsonConvert.SerializeObject(res, Formatting.Indented);
                return Ok(json);
            }
            return BadRequest("Invalid request");
        }

        [HttpPut]
        [Route("{id}")]
        public IActionResult Put(int id, BeerViewModel brVM)
        {
            var beerDto = mapper.Map<BeerDTO>(brVM);
            var res = service.Update(id, beerDto, mapper);
            if (res != null)
            {
                string json = JsonConvert.SerializeObject(res, Formatting.Indented);
                return Ok(json);
            }
            string returnStr = JsonConvert.SerializeObject(brVM, Formatting.Indented);
            return BadRequest("The beer could not be updated. Bad request:\n" + returnStr);
        }
        [HttpDelete]
        public IActionResult Delete()
        {
            if (service.DeleteAll())
            {
                return Ok($"All beers were deleted successfully!");
            }
            return BadRequest("No beers to be deleted");
        }
        [HttpDelete]
        [Route("{id}")]
        public IActionResult Delete(int id)
        {
            if (service.Delete(id))
            {
                return Ok($"Beer with Id: {id} was deleted successfully!");
            }
            return BadRequest($"There is no beer with Id: {id} to be deleted");
        }
        [HttpGet]
        [Route("sort/ABV")]
        public IActionResult SortByABV()
        {
            var result = service.SortByABV(mapper);
            if (result == null || result.Count == 0)
                return NotFound("No beers in the Database");
            string json = JsonConvert.SerializeObject(result, Formatting.Indented);
            return Ok(json);
        }

        [HttpGet]
        [Route("sort/name")]
        public IActionResult SortByName()
        {
            var result = service.SortByName(mapper);
            if (result == null || result.Count == 0)
                return NotFound("No beers in the Database");
            string json = JsonConvert.SerializeObject(result, Formatting.Indented);
            return Ok(json);
        }

        [HttpGet]
        [Route("sort/rating")]
        public async Task<IActionResult> SortByRating()
        {
            var result = await service.SortByRatingAsync(mapper);
            if (result == null || result.Count == 0)
                return NotFound("No beers in the Database");
            string json = JsonConvert.SerializeObject(result, Formatting.Indented);
            return Ok(json);
        }

        [HttpPut]
        [Route("{id}/rate")]
        public IActionResult Rate(int id, [FromQuery] int rating)
        {
            var result = service.RateBeer(id, rating, mapper);

            if (result != null)
            {
                string json = JsonConvert.SerializeObject(result, Formatting.Indented);
                return Ok(json);
            }
            return BadRequest("Invalid request");
        }

    }
}
