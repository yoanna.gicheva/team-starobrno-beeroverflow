﻿using AutoMapper;
using BeerOverFlow.Models;
using BeerOverFlow.Services.Contracts;
using BeerOverFlow.Services.ModelsDTo;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace BeerOverFlow.Controllers.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StyleController : ControllerBase
    {
        private readonly IStyleService service;
        IMapper mapper;
        public StyleController(IStyleService service, IMapper mapper)
        {
            this.service = service ?? throw new ArgumentNullException(nameof(service));
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            ICollection<StyleDTO> styles = service.GetAllStyles(mapper);
            if (styles.Count == 0 || styles == null)
                return BadRequest("There are no existing styles!");
            string json = JsonConvert.SerializeObject(styles, Formatting.Indented);
            return Ok(json);
        }
        [HttpGet]
        [Route("{id}")]
        public IActionResult GetStyleById(int id)
        {
            StyleDTO style = service.GetStyleById(id, mapper);
            if (style == null)
                return BadRequest("A Style with the chosen Id does not exist! Please try again!");
            string json = JsonConvert.SerializeObject(style, Formatting.Indented);
            return Ok(json);
        }

        [HttpPost]
        public IActionResult Post(StyleViewModel style)
        {
            var toPass = mapper.Map<StyleDTO>(style);
            var result = service.CreateStyle(toPass, mapper);
            if (result == null)
                 return BadRequest(toPass);
            return Ok(result);
        }

        [HttpPut]
        [Route("{id}")]
        public IActionResult Put(int id, [FromBody] StyleViewModel newStyle)
        {
            StyleDTO toPass = mapper.Map<StyleDTO>(newStyle);
            StyleDTO result = service.UpdateStyle(id, toPass, mapper);
            if (result == null)
                return BadRequest($"BeerStyle with Id: {id} does not exist or could not" +
                    $" be created due to already existing one with the same name!");
            string json = JsonConvert.SerializeObject(result, Formatting.Indented);
            return Ok($"BeerStyle was updated successfully!\n {json}" );
        }

        [HttpDelete]
        public IActionResult DeleteAll()
        {
            bool result = service.DeleteAll();
            if (!result)
                return BadRequest("There is no content to be deleted!");
            return Ok("All beer styles were deleted successfully!");
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult DeleteById(int id)
        {
            bool result = service.DeleteById(id);
            if (!result)
                return BadRequest($"Beer Style with Id: {id} does not exist!");
            return Ok($"Beer Style with Id: {id} was deleted successfully!");
        }
    }
}
