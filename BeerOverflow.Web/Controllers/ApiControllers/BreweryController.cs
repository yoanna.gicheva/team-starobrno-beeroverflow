﻿using AutoMapper;
using BeerOverFlow.Models;
using BeerOverFlow.Services.Contracts;
using BeerOverFlow.Services.ModelsDTo;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace BeerOverFlow.Controllers.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BreweryController : ControllerBase
    {
        IBreweryService service;
        IMapper mapper;
        public BreweryController(IBreweryService service, IMapper mapper)
        {
            this.service = service ?? throw new ArgumentNullException(nameof(service));
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var result = service.GetBreweries(mapper);

            if (result == null)
            {
                return BadRequest();
            }
            return Ok(mapper.Map<ICollection<BreweryViewModel>>(result));
        }
        [HttpGet]
        [Route("{id}")]
        public IActionResult Get(int id)
        {
            var result = service.GetBrewery(id, mapper);

            if (result == null)
            {
                return BadRequest();
            }
            return Ok(mapper.Map<BreweryViewModel>(result));
        }

        [HttpPost]
        public IActionResult Post(BreweryViewModel brVM)
        {
            var breweryDtoM = this.mapper.Map<BreweryDTO>(brVM);
            var res = service.Create(breweryDtoM, mapper);
            if (res == null)
            {
                return BadRequest("Invalid information");
            }
            return Ok(res);
        }

        [HttpPut]
        public IActionResult Update(BreweryViewModel brVm)
        {
            var breweryDtoM = this.mapper.Map<BreweryDTO>(brVm);
            var res = service.Update(breweryDtoM, mapper);
            if (res!=null)
            {
                return Ok(res);
            }

            return BadRequest("Invalid information");

        }
        

        [HttpDelete]        
        public IActionResult Delete(int id)
        {
            var res = service.Delete(id);            
            return Ok(res);
        }


    }
}