﻿using AutoMapper;
using BeerOverFlow.Models;
using BeerOverFlow.Services.Contracts;
using BeerOverFlow.Services.ModelsDTo;
using BeerOverFlow.Services.ModelsDTO;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeerOverFlow.Controllers.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService service;
        IMapper mapper;
        public UserController(IUserService service, IMapper mapper)
        {
            this.service = service ?? throw new ArgumentNullException(nameof(service));
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
          var users = await service.GetAllUsersAsync(mapper);
            string json = JsonConvert.SerializeObject(users, Formatting.Indented);
            return Ok(json);
        }
        [HttpGet]
        [Route("id={id}")]
        public IActionResult Get(int id)
        {
            UserReturnDTO user = service.GetUserById(id, mapper);
            if (user == null)
                return BadRequest("A User with the chosen Id does not exist! Please try again!");
            string json = JsonConvert.SerializeObject(user, Formatting.Indented);
            return Ok(json);
        }

        [HttpPost]
        public IActionResult Post([FromBody] UserViewModel user)
        {
            bool result = service.CreateUser(mapper.Map<UserViewDTO>(user), mapper);
            if (result == false)
                return BadRequest("The user could not be created. Please, try with other parameters.");
            else
                return Ok("The user was created successfully!");
        }

        [HttpPut]
        [Route("{id}")]
        public IActionResult Put([FromBody] UserViewModel user, int id)
        {
            UserReturnDTO result = service.UpdateUser(id, mapper.Map<UserViewDTO>(user), mapper);
            if (result == null)
                return BadRequest("A User could not be updated. Please, try with other parameters!");
            string json = JsonConvert.SerializeObject(result, Formatting.Indented);
            return Ok("The user was updated successfully!\n" + json);
        }

        [HttpDelete]
        public IActionResult Delete()
        {
            bool result = service.DeleteAll();
            if (!result)
                return BadRequest("There is no content to be deleted!");
            return Ok("All users were deleted successfully!");
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult Delete(int id)
        {
            bool result = service.DeleteById(id);
            if (!result)
                return BadRequest($"User with Id: {id} does not exist!");
            return Ok($"User with Id: {id} was deleted successfully!");
        }

        //b.Add beer to wish list

        [HttpPut]
        [Route("{userId}/wishlist/add/{beerId}")]
        public IActionResult AddBeerToWishList(int beerId, int userId)
        {
            bool result = service.AddToWishlist(beerId, userId);
            if (!result)
                return BadRequest($"This beer does not exist or is already added!");
            return Ok($"Beer with Id: {beerId} was added successfully!");
        }

        [HttpPut]
        [Route("{userId}/wishlist/remove/{beerId}")]
        public IActionResult RemoveFromWishList(int beerId, int userId)
        {
            bool result = service.RemoveFromWishlist(beerId, userId);
            if (!result)
                return BadRequest($"This beer does not exist or is already removed!");
            return Ok($"Beer with Id: {beerId} was removed successfully!");
        }
        [HttpPut]
        [Route("{userId}/dranklist/add/{beerId}")]
        public IActionResult AddBeerToDrankList(int beerId, int userId)
        {
            bool result = service.AddToDranklist(beerId, userId);
            if (!result)
                return BadRequest($"This beer does not exist or is already added!");
            return Ok($"Beer drink Id: {beerId} was added successfully!");
        }

        [HttpPut]
        [Route("{userId}/dranklist/remove/{beerId}")]
        public IActionResult RemoveFromDranklist(int beerId, int userId)
        {
            bool result = service.RemoveFromDranklist(beerId, userId);
            if (!result)
                return BadRequest($"This beer does not exist or is already removed!");
            return Ok($"Beer with Id: {beerId} was removed successfully!");
        }
    } 
}
