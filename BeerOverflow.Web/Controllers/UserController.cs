﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using BeerOverflow.Services.ModelsDTo.WebModels;
using BeerOverflow.Web.Models.UserViewModels;
using BeerOverFlow.Models;
using BeerOverFlow.Services.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BeerOverflow.Web.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService service;
        private readonly ILogger<BeersController> logger;
        private readonly IBeerService _beerService;
        private IMapper mapper { get; }

        public UserController(IMapper mapper, IUserService service, ILogger<BeersController> logger, IBeerService _beerService)
        {
            this.mapper = mapper;
            this.service = service;
            this.logger = logger;
            this._beerService = _beerService;
        }
        //GET: User

       [HttpGet]
        public async Task<ActionResult> ListUsers()
        {
            var res = await service.GetAllUsersAsync(mapper);
            var final = mapper.Map<ICollection<ListUserViewModel>>(res);
            return View(final);
        }

        // GET: User/Details/5
        [HttpGet]
        public IActionResult Details(int id)
        {
            var res = service.GetUserById(id, mapper);
            return View();
        }

        // GET: User/Create
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        // POST: User/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(ListUsers));
            }
            catch
            {
                return View();
            }
        }

        // GET: User/Edit/5
        [HttpGet]
        public IActionResult Edit(int id)
        {
            return View();
        }

        // POST: User/Edit/5
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public IActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(ListUsers));
            }
            catch
            {
                return View();
            }
        }

        [HttpGet]
        public IActionResult DeleteUser(int id)
        {
            var result = service.Delete(id);
            return RedirectToAction("ListUsers");
        }

        [HttpPost]
        public IActionResult AddToList(int beerId, string operation)
        {
            int userId = Convert.ToInt32(this.User.FindFirstValue(ClaimTypes.NameIdentifier));
            if (operation.ToLower().Contains("wishlist"))
                service.AddToWishlist(beerId, userId);
            else
                service.AddToDranklist(beerId, userId);
            var beer = _beerService.BeerDetails(beerId, mapper);
            return RedirectToAction("BeerDetails", "Beers", beer);
        }

        [HttpGet]
        //[Route("User/BanUser/{id}")]
        public IActionResult BanUser(int id)
        {
            bool result = service.BanUser(id);
            if (result == false)
                return RedirectToAction("Error");
            return RedirectToAction("ListUsers");
        }
       

        [HttpGet]
        public IActionResult UserAccount(string id)
        {
            UserDetailsModel user = service.UserDetails(id, mapper);
            if(user == null)
                return RedirectToAction("Error");
            return View(user);
        }
        [HttpGet]
        public IActionResult ShowWishlist(string name)
        {
            return View(Wishlist(name));
        }
        [HttpGet]
        public IActionResult ShowDranklist(string name)
        {
            return View(Dranklist(name));
        }
        [HttpGet]
        public IActionResult ShowReviews(string name)
        {
            var reviewList = service.UserReview(name);
            return View(reviewList);
        }
        public IActionResult DranklistRemove(int beerId, string name)
        {
            int userId = service.UserId(name);
            service.RemoveFromDranklist(beerId, userId);
            return View("ShowDranklist", Dranklist(name));
        }
        public IActionResult WishlistRemove(int beerId, string name)
        {
            int userId = service.UserId(name);
            service.RemoveFromWishlist(beerId, userId);
            return View("ShowWishlist", Wishlist(name));
        }
        private ICollection<BeerViewModel> Wishlist(string name)
        {
            var raw = service.UserWishlist(name, mapper);
            return mapper.Map<ICollection<BeerViewModel>>(raw);
        }
        private ICollection<BeerViewModel> Dranklist(string name)
        {
            var raw = service.UserDranklist(name, mapper);
            return mapper.Map<ICollection<BeerViewModel>>(raw);
        }



    }
}