﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BeerOverflow.Services.ModelsDTo.WebModels;
using BeerOverFlow.Models;
using BeerOverFlow.Services.Contracts;
using BeerOverFlow.Services.ModelsDTo;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;

namespace BeerOverflow.Web.Controllers
{
   
    public class BreweryController : Controller
    {
        private readonly IBreweryService brService;
        private readonly ICountryService ctrCountry;        
        IMapper mapper;    
       

        public BreweryController(IBreweryService brService,
            ICountryService ctrCountry, IMapper mapper)
        {
            this.brService = brService;
            this.mapper = mapper;        
            this.ctrCountry = ctrCountry;
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public IActionResult CreateBrewery()
        {
            var countryViewModels = mapper.Map<ICollection<CountryViewModel>>(ctrCountry.GetCountry(mapper));
           
            ViewData["CountryList"] = new SelectList(countryViewModels, "CountryId", "CountryName");

            return View();
        }

        [HttpGet]
        public IActionResult ListBreweries()
        {
            var res = brService.GetBreweries(mapper);
            var x = mapper.Map<ICollection<BreweryViewModel>>(res);
            return View("ListBreweries", x);
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public IActionResult EditBrewery(int id)
        {
            
            var countryViewModels = 
                mapper.Map<ICollection<CountryViewModel>>(ctrCountry.GetCountry(mapper));           
            ViewData["CountryList"] = new SelectList(countryViewModels, "CountryId", "CountryName");

            var res = brService.GetBrewery(id, mapper);

            if (res == null)
            {
                return NotFound();
            }
            return View(mapper.Map<BreweryViewModel>(res));
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public IActionResult EditBrewery(int id,BreweryViewModel brewery)
        {
            brewery.BreweryId = id;
            var res = brService.Update(mapper.Map<BreweryDTO>(brewery), mapper);
            return RedirectToAction("ListBreweries");
            //return RedirectToActionPermanent("BeerDetails", new { id = res.BreweryId });
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public IActionResult CreateBrewery(BreweryViewModel brewery)
        {
            var res = brService.Create(mapper.Map<BreweryDTO>(brewery), mapper);
            return RedirectToAction("ListBreweries");
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public IActionResult DeleteBrewery(int id)
        {
            var res = brService.Delete(id);
            return RedirectToAction("ListBreweries");
        }

        [HttpGet] 
        public IActionResult BreweryDetails(int id)
        {
            ICollection<BeerDTO> beersRaw = brService.GetBeers(id, mapper);
            ICollection<BeerViewModel> beers = new List<BeerViewModel>();
            foreach (var beer in beersRaw)
            {
                beers.Add(mapper.Map<BeerViewModel>(beer));
            }

            return View(beers);
        }
    }
}